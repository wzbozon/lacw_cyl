C************************************************************************************************************

C	 ��������� ������� ���������� � ��������� ��������� ������� ������� �����
C	 � �������������� �������������� ����

C************************************************************************************************************	 
C	 ������� �. �., ��������� �.�.

C	 "�������� �������� ������ �������� ����� ���������� ������, �� ���� ��������� �� �����" 
C	  �� ������������� �������� �� ���������

C      "� ����� ������ ������������� ���������� �������� ����������, ������� ������ ������� �� �����������" 
C       ����� ������, "����������� ������������� �++"

C	 "������ ������ �������� ����������������. ������ �� ����� ����� � ��������� 1:1"
C	  �� ���� �� ����������������

C	 "������� ������ ���������� �� ����������������, � ����� ������ ���� ���������."
C	  �. �. �������

C	 "������ ����� ��������������"
C	  �. �. �������
C
C	 "THE UNIVERSE IS RUN BY THE COMPLEX INTERWEAVING OF THREE	*
C	  ELEMENTS: ENERGY, MATTER, AND ENLIGHTENED SELF-INTEREST.'	*
C	  --G'KAR, `BABYLON 5' EPISODE 1.11: `SURVIVORS'
C************************************************************************************************************
	
C      1. �� ���� ������ ������� �������� �������� ����� ���� "ADDED FOR GFM" 
C	 2. ��� ����������� �������� ������ "�" �����
C	 3. ���������� ����� �������� �������
C	 4. I_ �������� I-�����, � ��������� ������� ����� ��������� 1, �������� L1 - ��� L-�����
C	 5. ��� ���������� ������ ���� ���� ���������
C	 6.	������� ������� ��������� � ����� GFUNCTIONS, ���������� ����� ����� CONTAINS; ��� �� ���� �����
C	 7. LSMALL = L-1
C	 8. ���� ����������� ����� *.FOR ����, �� ���������� �������� ��� � � ������ *.MAK ����� �� ��������
C	 9. ����� ���������, ��� ������ �� INTEGER ������, ������ �������� INTEGER �� DOUBLE PRECISION � �.�.
C	(�� ����� ������ ����� �������� ���������� � ����� � FORTRAN)

C************************************************************************************************************
 
      PROGRAM GREEN

C************************************************************************************************************
C	���������� ����������
C************************************************************************************************************

	USE MSFLIB
	!MS$LIST
	IMPLICIT NONE
100	FORMAT(F23.20)
1	FORMAT(A1\)
C	BEEP PARAMETERS
	INTEGER(4) FREQUENCY, DURATION
      
C      CALL BEEPQQ(FREQUENCY, DURATION)
C	���������� ��� ������������� MSFLIB ���������� ��� �������� ������
	INTEGER(4)  LEN, COUNT
	CHARACTER(80)  FILE
	CHARACTER(1) CH, CHH	
	CHARACTER(1) CHAR
	LOGICAL RESULT
	CHARACTER TATIME*16, TCTIME*19, TMTIME*18
	DOUBLE PRECISION A, B, C, RMT, EPSI, CALCCYFORGFM, CALCCJFORGFM, 
	+KP, K,  E, Z, Z1, RES3, STEP, EPSD, DELTANUM, EMIN, EMAX, ESTEP,
	+DELTAMAX, CB,CB1, KP_SMALL, KP_SMALL1, KPZ, MFI,  
     + TIME_OF_ALL, TIME_OF_LACW, MTIME, TIME, VR(300), VRRB(300),
     + TDELTA, EDELTA,DE, 
	+TIME_LOCAL, LNRAD, RAD(300)
	DOUBLE COMPLEX SS, F, FA, FB, 
	+I1, I2, SIMP_I12,   
	+SUM1, SUM2, BIG, RES14, SSS, BIGRES
C	A - ������� ������ ��������, B - ���������� ������ ��������,
C	C - ����� ������������ ������
C	EPSI - ����������� ���������� ����������
C	������� ������������ �������, ������� ������ �����
C	����� ���������� ����� �������� ������� � ������ ����������
	DOUBLE PRECISION, ALLOCATABLE :: KAPPA(:), CJ(:), CY(:), PSI(:),
	+PSIR(:), PSIE(:), PSIER(:), NLALPHA(:), D(:), RO(:), PHI(:),
     +RSZ(:), EL(:), BIGD(:,:), GREAL(:), GIMAG(:), PSIALL(:,:),  
	+PSIRALL(:,:), PSIEALL(:,:), DSTATES(:,:,:), DTOTAL(:), DL(:)
	
	DOUBLE COMPLEX, ALLOCATABLE:: H(:,:), INTE(:), BIGH(:,:,:), 
	+ASMALL(:,:), BSMALL(:,:), ASMALL1(:,:), BSMALL1(:,:),
	+OECBAR(:,:), IMGLML1M1(:,:,:)
	INTEGER, ALLOCATABLE :: M(:), N(:), P(:)
C	M, N, P - ��������� �������� �������, ��������� �����
C	NPNTS - ����� ����� K, ��� ����� ���� � INLACW � ���� ��������� POINTSTOTAL
	INTEGER MC, COUNTX, I, I_, J, NPNTS, NINTERVALS, NLMAX, NLMAXNEW,
	+MSMALL, L, AL, NA, L1, NA1, AL1, JJ, II, X, X1, 
     +MSMALL1, LAMBDA,T, EPNTS,ITERATIONS, LMAX, 
	+LENH, LEND, LENS, ALPHA, NATOMS, LN, MNUM,
	+CCOUNTER, CNUM, JRIS(5), NPNTS1
C	JRIS(5) - ������ ������� ����� �� R, ��������������� �������� �� ���� 5 ��������� ����� ������
C	IT - ����� ����� ������, ���������� ����� ����� ������ 5
	DOUBLE PRECISION, PARAMETER :: 
	+PI=3.14159265358979323846264338328D0
	DOUBLE PRECISION, PARAMETER:: EPSBES = 1.0D-12
C	�������� ���������� ������� �������
	DOUBLE PRECISION, PARAMETER:: EVS=13.6058D0
C	����������� �������� �� �� � ��������

	DATA TATIME/'FIRST-POINT TIME'/,TCTIME/'CALCULATION TIME '/,
	+ TMTIME/'(L,M) CYCLE TIME: '/
	TIME_OF_ALL=0.D0
	CALL TTIME(TIME_OF_ALL,'CALCULATION TIME ',0)

	WRITE(*,*) '********************************************'
	WRITE(*,*) 'GREEN FUNCTION AND LACW METHOD PROGRAM'
	WRITE(*,*) '********************************************'
	WRITE(*,*)
	CALL TIMESTAMP()
C	TIMESTAMP ������ ����� � ���� ������� ���� � �����
	WRITE(*,*)
	WRITE(*,*) 'SHOULD I RUN ATOM.EXE, STRCY.EXE AND OVER55.EXE'
	WRITE(*,*) '(1=YES, ANYKEY=NO)?'
	CH = GETCHARQQ()
	WRITE(*,*) CH
	WRITE(*,*) 'DO YOU WANT TO RUN SECOND TIME OVER55'
	WRITE(*,*) '(1=YES, ANYKEY = NO)?'
	CHH = GETCHARQQ()
	WRITE(*,*) CH

C	������������� �������� ������� �������� ATOM.EXE-OVER55.EXE
  	TIME_OF_LACW=0.D0
	CALL TTIME(TIME_OF_LACW,'TIME OF LACW',0)

	IF ((CH .EQ. '1')) THEN
	WRITE(*,*) 'TRYING TO EXECUTE ATOM.EXE'
	RESULT = RUNQQ('ATOM.EXE', '-C -R')
	IF (.NOT.RESULT) THEN
	WRITE(*,*)
	WRITE(*,*) 'ATOM.EXE SUCCESSFULLY FINISHED'
	ELSE
	WRITE(*,*) 'ATOM.EXE FAILED TO COMPLETE'
	END IF
C	WRITE(*,*) 'PRESS ENTER TO CONTINUE'
C	READ(*,*)
	WRITE(*,*) 'TRYING TO EXECUTE STRCY.EXE'
	WRITE(*,*)
	RESULT = RUNQQ('STRCY.EXE', '-C -R')
	IF (.NOT.RESULT) THEN 
	WRITE(*,*)
	WRITE(*,*) 'STRCY.EXE SUCCESSFULLY FINISHED'
	ELSE
	WRITE(*,*) 'STRCY.EXE FAILED TO COMPLETE'
	END IF
C	WRITE(*,*) 'PRESS ENTER TO CONTINUE'
C	READ(*,*)
C	WRITE(*,*) 'TRYING TO RUN OVER55.EXE'
C	WRITE(*,*) 'PRESS ENTER TO CONTINUE'
C	READ(*,*)
C	TIME_OF_OVER55=0.D0
C	CALL TTIME(TIME_OF_OVER55,OVER55TIME,0)
	RESULT = RUNQQ('OVER55.EXE', '-C -R')
	IF (.NOT.RESULT) THEN
	WRITE(*,*)
	WRITE(*,*) 'OVER55.EXE SUCCESSFULLY FINISHED'
C	CALL TTIME(TIME_OF_OVER55,OVER55TIME,0)
	ELSE 
	WRITE(*,*) 'OVER55.EXE FAILED TO COMPLETE'
	END IF
	END IF
C	��������� ����������� ������� - ��������� ������ GREEN ��� ���? 
	WRITE(*,*)
C	WRITE(*,*) 'DO YOU WANT TO RUN SECOND TIME OVER55'
C	WRITE(*,*) '(1=YES, ANYKEY = NO)?'
C	CH = GETCHARQQ()
C	WRITE(*,*) CH
	IF ((CHH.EQ.'1')) THEN
C	 WRITE(*,*) 'TRYING TO EXECUTE OVER55.EXE'
C	 WRITE(*,*) ' SECOND TIME AFTER RENAMING OF FILES'
C	 WRITE(*,*) 'PRESS ENTER TO CONTINUE'
C	 READ(*,*)
C	 RESULT = RENAMEFILEQQ('M.TXT', 'MG.TXT')
C	 IF (RESULT) THEN
C	  WRITE(*,*) 'FILE M.TXT SUCCESSFULLY RENAMED'
C	 ELSE 
C	  WRITE(*,*) 'COULDN''T RENAME M.TXT'
C	 END IF
C	 RESULT = RENAMEFILEQQ('N.TXT', 'NG.TXT')
C	 IF (RESULT) THEN
C	  WRITE(*,*) 'FILE N.TXT SUCCESSFULLY RENAMED'
C	 ELSE 
C	  WRITE(*,*) 'COULDN''T RENAME N.TXT'
C	 END IF
C	 RESULT = RENAMEFILEQQ('P.TXT', 'PG.TXT')
	 RESULT = RENAMEFILEQQ('PSI.TXT', 'PSIG.TXT')
	 RESULT = RENAMEFILEQQ('PSIE.TXT', 'PSIEG.TXT')
	 RESULT = RENAMEFILEQQ('PSIER.TXT', 'PSIERG.TXT')
	 RESULT = RENAMEFILEQQ('PSIR.TXT', 'PSIRG.TXT')
	 RESULT = RENAMEFILEQQ('CYLINDRICALCOORDINATES.TXT', 
	+'CYLINDRICALCOORDINATESG.TXT')
	 RESULT = RENAMEFILEQQ('D_ENERGIES.TXT', 'D_ENERGIESG.TXT')
	 RESULT = RENAMEFILEQQ('EPSI_NATOMS.TXT', 'EPSI_NATOMSG.TXT')
	 RESULT = RENAMEFILEQQ('H_AMNP.TXT', 'H_AMNPG.TXT')
	 RESULT = RENAMEFILEQQ('NPNTS_LMAX_LENH_LEND.TXT',
	+ 'NPNTS_LMAX_LENH_LENDG.TXT')
	 RESULT = RUNQQ('OVER55.EXE', '-C -R')
	 IF (.NOT.RESULT) THEN
	  WRITE(*,*)
	  WRITE(*,*) 'OVER55.EXE SUCCESSFULLY FINISHED THE SECOND TIME'
	 ELSE
	  WRITE(*,*)
	  WRITE(*,*) 'OVER55.EXE FAILED TO COMPLETE THE SECOND TIME'
	 END IF
C	��������� ������� - ��������� OVER55 ������ ���? 
	END IF

C	END IF
C	WRITE(*,*) 'PRESS ENTER TO CONTINUE'
C	READ(*,*)
C	WRITE(*,*) 'BEGINNING TO CALCULATE GF ELEMENTS'
C	WRITE(*,*) 'PRESS ENTER TO CONTINUE'
C	READ(*,*)
	
	FREQUENCY = 20000
      DURATION = 10000

	CALL TTIME(TIME_OF_LACW,'TIME OF LACW',0)

C	END IF

	WRITE(*,*) 'BEGINNING TO CALCULATE DENSITY OF STATES BY GF METHOD'

	
	FREQUENCY = 20000
      DURATION = 10000


C************************************************************************************************************
C	���������� ����� �������� ������� COUNTX � ������ ���������� �� ������
C************************************************************************************************************

C	RESET THE TIME COUNTER:
	TIME_OF_ALL=0.D0
	CALL TTIME(TIME_OF_ALL,TCTIME,0)
	TIME = 0.D0
	CALL TTIME(TIME, TATIME,0)
C	OPEN(60, FILE = 'CYCLE_I_TIME.TXT', FORM = 'FORMATTED')    
C	OPEN(61, FILE = 'INFORMATION.TXT', FORM = 'FORMATTED')
C	OPEN(62, FILE = 'FC REAL.TXT', FORM = 'FORMATTED')
C	OPEN(63, FILE = 'FC IMAG.TXT', FORM = 'FORMATTED')
C	OPEN(64, FILE = 'PODINTEGR.TXT', FORM = 'FORMATTED')
	OPEN(65, FILE = 'BIGH.TXT', FORM = 'FORMATTED')
	OPEN(66, FILE = 'BIGD.TXT', FORM = 'FORMATTED')
	OPEN(33, FILE = 'LN.TXT')
	READ(33,*) LN
	WRITE(*,*) 'LN IS EQUAL TO: ', LN
C	READ(*,*)
	OPEN(67, FILE = 'MYJ.TXT', FORM = 'FORMATTED')
 	OPEN(32, FILE = 'VR.TXT', ACCESS = 'DIRECT', RECL = LN+100) 
	OPEN(33, FILE = 'COUNTER.TXT')
	DO WHILE (.NOT. EOF(33))
	 READ(33, *) ITERATIONS
	END DO
	WRITE(*,*) 'THE NUMBER OF ITERATIONS IN ATOM IS READ AS...',
	+ ITERATIONS
C	READ(*,*)
	READ(32, REC = ITERATIONS) VR
	CLOSE(32)
	CLOSE(33)
C	WRITE(*,*) 'VR IS READ AS: '
C	WRITE(*,100) VR
	WRITE(*,*)
	WRITE(*,*) 'VR(295) IS READ AS: '
	WRITE(*,100) VR(295)
	WRITE(*,*) 'VR(296) IS READ AS: '
	WRITE(*,100) VR(296)
	WRITE(*,*)
C	READ(*,*)
      OPEN(1, FILE = 'COUNTX.TXT', FORM = 'FORMATTED')
	READ(1,*) COUNTX
	CLOSE(1)
C	���������� ����, M, N, P � ������ �J(I), CY(I), ��� I - ����� �������� �������
	ALLOCATE (KAPPA(COUNTX), M(COUNTX), N(COUNTX), P(COUNTX),
	+ CJ(COUNTX), CY(COUNTX), H(COUNTX,COUNTX), D(COUNTX),
     + OECBAR(COUNTX,COUNTX))
	OPEN(2, FILE = 'KAPPA.TXT', FORM = 'FORMATTED')
	READ(2,*) KAPPA
C	OPEN(3, FILE = 'VIVODKAPPA.TXT', FORM = 'FORMATTED')
C	WRITE(3,*) KAPPA
	CLOSE(2)
C	CLOSE(3)
	OPEN(4, FILE = 'M.TXT', FORM = 'FORMATTED')
	READ(4,*) M
C	READ(4,*) (M(I),I=1,COUNTX), (N(I),I=1,COUNTX), (P(I),I=1,COUNTX)      
	CLOSE(4)
C	WRITE(*,*) 'M IS READ AS', M
C	WRITE(*,*) '--------------------'
C	WRITE(*,*) 'N IS READ AS', N
C	WRITE(*,*) '--------------------'
C	WRITE(*,*) 'P IS READ AS', P
C	WRITE(*,*) '--------------------'
C	READ(*,*)
	OPEN(5, FILE = 'N.TXT', FORM = 'FORMATTED')
	READ(5,*) N
	CLOSE(5)
	OPEN(6, FILE = 'P.TXT', FORM = 'FORMATTED')
	READ(6,*) P
	CLOSE(6)
C	����� ���������� �������� �, N, P (I) ������� ��, ��� ���������, ����� ���������
C	OPEN(7, FILE = 'VIVODM.TXT', FORM = 'FORMATTED')
C	WRITE(7,*) M
C	OPEN(8, FILE = 'VIVODN.TXT', FORM = 'FORMATTED')
C	WRITE(8,*) N
C	OPEN(9, FILE = 'VIVODP.TXT', FORM = 'FORMATTED')
C	WRITE(9,*) P
	
	
C	READ(*,*)
	WRITE(*,*) 'M, N, P, KAPPA ARE READ AS FOLLOWING ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		WRITE(*, *) M(I), N(I), P(I), KAPPA(I)
	END DO

C	READ(*,*)

	WRITE(*,*)
	WRITE(*,*) 'CYLINDER PARAMETERS A, B AND C ARE READ AS FOLLOWING
	+ ...'
	WRITE(*,*)
	OPEN(10, FILE = 'CYLPARAMS.TXT', FORM = 'FORMATTED')
	READ(10,*) A, B, C
	CLOSE(10)
	WRITE(*,*) A, B, C
C	READ(*,*)
	WRITE(*,*)
	OPEN(11, FILE = 'CJ.TXT', FORM = 'FORMATTED')
	OPEN(12, FILE = 'CY.TXT', FORM = 'FORMATTED')							 
	WRITE(*,*) 'CJ(I) ARE CALCULATED ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		CJ(I) = CALCCJFORGFM(M(I), A, B, KAPPA(I))
C		WRITE(*,*) CJ(I)
		WRITE(11,*) CJ(I)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'CY(I) ARE CALCULATED ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		CY(I) = CALCCYFORGFM(M(I), CJ(I), B, KAPPA(I))
C		WRITE(*,*) CY(I)
		WRITE(12,*) CY(I)
	END DO
	CLOSE(11)
	CLOSE(12)
C	READ(*,*)
	OPEN(13, FILE = 'EPSI_NATOMSG.TXT', FORM = 'FORMATTED')
	READ(13,*) EPSI, NATOMS
	CLOSE(13)
	WRITE(*,*) 'EPS AND NATOMS ARE READ'
	WRITE(*,*)
C	READ(*,*)
C	����� ���������� ����� ������ � ������������ ������ NATOMS ����� ���������� ������� �������������� ���������
	ALLOCATE(RO(NATOMS), PHI(NATOMS), RSZ(NATOMS))
	OPEN(14, FILE = 'RADIUS OF MT SPHERE.TXT', FORM = 'FORMATTED')
	READ(14, *) RMT
	WRITE(*,*) 'RMT IS READ AS ', RMT
	WRITE(*,*)
C	READ(*,*)
	CLOSE(14)
	OPEN(14, FILE = 'JRIS.TXT', FORM = 'FORMATTED')
	READ(14, *) JRIS
	CLOSE(14)
	WRITE(*,*) 'JRIS(1) AND JRIS(2) ARE READ AS: ', JRIS(1), JRIS(2)
C	READ(*,*)

	OPEN(32, FILE = 'VR_AS_READ.TXT', FORM = 'FORMATTED')
C	����� ����������� ��������������� ���������� ���������� ����� ����� (X) � �������(RAD)
	DO X = 1, JRIS(1)
	LNRAD = LOG(0.000000001) + ABS((LOG(RMT)-
	+		LOG(0.000000001)))/DBLE(JRIS(1)-1)*(X-1)
	RAD(X) = EXP(LNRAD)
	VRRB(X) = VR(X)*2
C	VR - V ���������� �� R � ��������-�������
C	VRRB - �� �� ����� ��� VR � ���������
	WRITE(32,*) RAD(X), VR(X), VRRB(X)
C	������� ������ �� ������� ��-�����, �� ���� �� JRIS(1)-�� (221-��) �����
	END DO
	CLOSE(32)

	OPEN(15, FILE = 'NPNTS_LMAX_LENH_LENDG.TXT', FORM = 'FORMATTED')
	READ(15, *) NPNTS, NLMAX, LENS, LENH, LEND
	WRITE(*,*) 'NPNTS,NLMAX, LEN ARE READ'
	NPNTS1 = (NPNTS-1)/2+1
	WRITE(*,*) 'NPNTS1 IS EQUAL TO',NPNTS1
C	NLMAX - ��� ������������ �������� L ���������(��������� �����)
	CLOSE(15)
	WRITE(*,*) 
	WRITE(*,*) 'EPSI, NPNTS AND RMT, LENH, LEND'
C	EPSI - ����������� ���������� ����������, ������� �� �������� ����� INLACW � ���������
C	OVER55.EXE, � �� ����� ��� �� OVER55.EXE, STRCY.EXE � ATOM.EXE ��� ������������� ��������
C	����� INLACW, INATOM 
C	RMT - ������ ������-��� �����
	WRITE(*,*)
	WRITE(*,*) EPSI, NPNTS
	WRITE(*,*) RMT, LENH, LEND
	WRITE(*,*)
	ALLOCATE(BIGH(NPNTS,COUNTX,COUNTX), BIGD(NPNTS,COUNTX),
	+ASMALL(COUNTX, NPNTS),  BSMALL(COUNTX, NPNTS),
     +ASMALL1(COUNTX, NPNTS),  BSMALL1(COUNTX, NPNTS))
C	READ(*,*)
C	����� ����������	
	NINTERVALS = NPNTS - 1

C	������ A, B, ������� ������� �� U, I1, I2, U' � �.�. - ALNAL � BLNAL   
C	A = I2*(U*) - I1*U(*')
C	B = I1*(U') - I2*(U)
C	NLMAX �������, ������ ����� ������� ALLOCATE ������� PSI � ������
C	PSI - ��� ������ ���������� �������� ������� U
C	PSIE - ��� ������ �� ����������� �� �������
C	PSIR - ����������� �� �������
C	PSIER - ����������� �� ������� � �������
	ALLOCATE(PSI(NLMAX), PSIR(NLMAX), PSIE(NLMAX), PSIER(NLMAX),
	+ EL(NLMAX), NLALPHA(NLMAX))			 
C	 ���������� �������� PSI, PSIR, PSIE, PSIER �� ��������������� ������	
      OPEN(16, FILE = 'PSI.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(16,*) PSI(L, IT), A1, A2, A3
C			WRITE(*,*) L, IT, PSI(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(16)

	OPEN(17, FILE = 'PSIR.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(17,*) PSIR(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(17)
	 
	OPEN(18, FILE = 'PSIE.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(18,*) PSIE(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(18)
	 
	OPEN(19, FILE = 'PSIER.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(19,*) PSIER(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(19)

	OPEN(19, FILE = 'PSIER.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(19,*) PSIER(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(19)

	OPEN(27, FILE = 'indefect6.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		DO L = 1, NLMAX 
 			READ(27,*) NLALPHA(L, IT), A1, A2
		END DO
	END DO
	CLOSE(27)

	WRITE(*,*)
	WRITE(*,*) 'PSI(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSI(I)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'PSIR(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSIR(I)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'PSIE(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSIE(I)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'PSIER(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSIER(I)
	END DO 
 
C	OPEN(20, FILE = 'I1.TXT', FORM = 'FORMATTED')
C	OPEN(21, FILE = 'I2.TXT', FORM = 'FORMATTED')
C	OPEN(26, FILE = 'ALNAL.TXT', FORM = 'FORMATTED')
C	OPEN(27, FILE = 'BLNAL.TXT', FORM = 'FORMATTED')
	NLMAXNEW = NLMAX - 1

C	���������� �������������� ���������, RO - RHO, PHI, RSZ - Z; ALPHA - ����� ����� � ������
 	OPEN(24, FILE = 'CYLINDRICALCOORDINATESG.TXT', FORM = 'FORMATTED')
	DO ALPHA=1,NATOMS
		READ(24, '(A1,3F20.10)') CHAR, RO(ALPHA),PHI(ALPHA),RSZ(ALPHA)
	END DO
C	�������� ����������
	WRITE(*,*) 'CYLINDRICAL COORDINATES ARE READ AS FOLLOWING: '
      DO ALPHA=1,NATOMS
		WRITE(*, '(3F20.10)') RO(ALPHA),PHI(ALPHA),RSZ(ALPHA)
	END DO
	CLOSE(24)
C	READ(*,*)

C	���������� ������� EL
	OPEN(25, FILE = 'E_LG.TXT', FORM = 'FORMATTED')
	DO I = 1, 5
		READ(25,*) EL(I)
		EL(I) = EL(I)/EVS
	END DO
	DO I = 6, NLMAX
		EL(I) = EL(5)
		EL(I) = EL(I)/EVS
	END DO
	CLOSE(25)

C	�������� EL
	WRITE(*,*)
	WRITE(*,*) 'EL(I) ARE READ AS FOLLOWING:'
	WRITE(*,*) EL
	WRITE(*,*)
	WRITE(*,*) 'READING OF H AND D ARRAYS STARTED... '	
	
C	���������� ������� ������������� �������� ������� � �������� ��������


 	OPEN(22,FILE='H_AMNPG.TXT', ACCESS = 'DIRECT',
	+RECL = LENH+1000)
	DO J = 1, NPNTS
C	 WRITE(65,*) 'J =', J
	 READ(22, REC=J)((H(II,JJ),JJ=1,COUNTX),II=1,COUNTX)
	 DO JJ = 1, COUNTX
	 DO II = 1, COUNTX
	  
	  BIGH(J,II,JJ) = H(II,JJ)
C	  WRITE(65, *) BIGH(J,II,JJ)
	  WRITE(65, *) J, JJ, II, DBLE(BIGH(J,II,JJ)),
	+   DIMAG(BIGH(J,II,JJ))
C	  BIGH(J,II,JJ) - J=K, II= I, JJ=LAMBDA
	  END DO
	 END DO
C	 WRITE(65, *) '--------------------------------------------------'
	WRITE(*,1) '.'
	END	DO
	CLOSE(65)
	CLOSE(22)
	
	WRITE(*,*)

		

C	���������� ������� �������� ������� �������� �������
	OPEN(23,FILE='D_ENERGIESG.TXT',
	+ACCESS = 'DIRECT', RECL = LEND+1000)
	DO J=1, NPNTS
	 READ(23,REC=J) D 
	 WRITE(66,*) 'J =', J
	 DO II = 1, COUNTX
	  BIGD(J,II) = D(II)
	  WRITE(66,*) BIGD(J,II)
	 END DO
	 WRITE(66,*) '-----------------------------------------------'
C	WRITE(*,1) '.'
	END DO
	WRITE(66,*) 'THE WHOLE BIGD: '
	WRITE(66,*) BIGD
	CLOSE(66)
	CLOSE(23)
	WRITE(*,*)
	WRITE(*,*) 'READING OF H AND D ARRAYS FINISHED.'
	WRITE(*,*)

C	I =1
C 	I_=1
C     X = 1
C	X1 = 1
C	J = 1
C	NA = 1
C	AL = 1
C	NA1 = 1
C	AL1 = 1

	ALPHA = 1
C	ALPHA1 = 1
C	EPSD = 0.0001D0

C	 ����������  ��������� �� K
C	S - ��������������� ����� � ��� �������� �����
C	FA - �������� ��������������� ����� ��� J = 1
C     FB - �������� ��������������� ����� ��� J = NPNTS(����� ����� �� �)
C     ������������ ����� ��������, ��� ����� 1, ��� ��� J=1,2,3,...,NPNTS
C	���������� ������ �� �����

	OPEN(30, FILE='indefect1.dat', FORM = 'FORMATTED')
	
C	READ(*,*)
C	WRITE(*,*) 'EMIN='
C	READ(*,*) EMIN
C	WRITE(*,*) 'EMAX='
C	READ(*,*) EMAX
C	WRITE(*,*) 'EPNTS='
C	READ(*,*) EPNTS
C	WRITE(*,*) 'EPSD='
C	READ(*,*) EPSD
C	WRITE(30,*) EMIN, EMAX, EPNTS, EPSD
	READ(30,*) EMIN, EMAX, EPNTS, EPSD, TDELTA, LMAX
C	EPNTS - ����� ����� �� �������
C	EPSD - ������� � ������ ������� ������
	CLOSE(30)
	WRITE(*,*) 'EMIN IS READ AS ', EMIN
	WRITE(*,*) 'EMAX IS READ AS ', EMAX
	WRITE(*,*) 'EPNTS IS READ AS ', EPNTS
	WRITE(*,*) 'EPSD IS READ AS ', EPSD
	WRITE(*,*) 'TDELTA IS READ AS ', TDELTA
	WRITE(*,*) 'LMAX IS READ AS ', LMAX
C	READ(*,*)
C	WRITE(*,*) 'ALPH IS CALCULATED AS', ALPH(L)
C	WRITE(*,*) 'WHERE L IS EQUAL TO', L
	EDELTA = ABS(SQRT(-4*EPSD*LOG(2*TDELTA*SQRT(PI*EPSD))))
	
	WRITE(*,*) 'EDELTA IS CALCULATED AS', EDELTA
C	WRITE(*,*)
	WRITE(*,*) 'NUMBER OF INTERVALS BY K : ', NINTERVALS
C	READ(*,*)
	ESTEP = (EMAX-EMIN)/DBLE((EPNTS-1))
	ALLOCATE(INTE(EPNTS), GREAL(EPNTS), GIMAG(EPNTS))
	EPNTS = EPNTS
C	EPNTS - ������������ �������� �������� X
C	FJ = (0.D0,0.D0)
C	S = (0.D0,0.D0)

C	���������� PSI ��� ���� L, R
C	WRITE(*,*) 'READING PSI, PSIE...'
C	WRITE(*,*) 'PSIALL, R, L ARE READ AS: ' 
	ALLOCATE(PSIALL(NLMAX, JRIS(1)))
 	OPEN(1, FILE = 'PSIALL.TXT', FORM = 'FORMATTED')
	DO K = 1, NLMAX
	DO X = 1, JRIS(1)
	READ(1, *) PSIALL(K, X)
C	WRITE(*,*) PSIALL(K, X), X, K 
	END DO
	END DO
C	READ(*,*)

C	WRITE(*,*)

C	���������� PSIR ��� ���� L, R
C 	WRITE(*,*) 'PSIRALL, R, L ARE READ AS: ' 
	ALLOCATE(PSIRALL(NLMAX, JRIS(1)))
 	OPEN(1, FILE = 'PSIRALL.TXT', FORM = 'FORMATTED')
	DO K = 1, NLMAX
	DO X = 1, JRIS(1)
	READ(1, *) PSIRALL(K, X)
C	WRITE(*,*) PSIRALL(K, X), X, K 
	END DO
	END DO
C	READ(*,*)

C	WRITE(*,*)
	
C	���������� PSIE ��� ���� L, R
C  	WRITE(*,*) 'PSIEALL, R, L ARE READ AS: ' 
	ALLOCATE(PSIEALL(NLMAX, JRIS(1)))
 	OPEN(1, FILE = 'PSIEALL.TXT', FORM = 'FORMATTED')
	DO K = 1, NLMAX
	DO X = 1, JRIS(1)
	READ(1, *) PSIEALL(K, X)
C	WRITE(*,*) PSIEALL(K, X), X, K 
	END DO
	END DO
C	READ(*,*)							 
C	WRITE(*,*) 'DONE'						 
	
C************************************************************************************************************	
C	������ ��������� ���������
C************************************************************************************************************

	OPEN(15, FILE = 'IMGDIAGGR.TXT', FORM = 'FORMATTED')
C	STEP - ��� �� �, ��������� �������
C	SS - ������������� ����������
C	X,T - ��������
C	F - ������������� ���������� � ������� �������� ������� �������� ���������������� ���������
C	ESTEP - ��� �� �������
C	EMIN - ����������� �������� ������� �� �����
C	FA - �������� ��������������� ������� � ��������� �����, �� ���� ��� ����������� �������
C	FB - �� �� ����� ��� ������������ �������
	DELTAMAX = DELTANUM(EPSD, 0.D0)

	WRITE(*,*) 'DELTAMAX IS EQUAL TO', DELTAMAX
C	WRITE(*,*) 'TEST IS EQUAL TO', DELTAMAX*SQRT(EPSD)
	WRITE(*,*) 'RO(ALPHA) IS EQUAL TO', RO(ALPHA)

C	������ ����� �� L
 	
C	LMAX = 3
C	LMAX - ������������ ����� L � ���������� ��������� ��������� �� L �����
	MNUM = (LMAX-1)*2 + 1
	CNUM = ((1+MNUM)/2)*((MNUM-1)/2+1)
	WRITE(*,*) 'NUMBER OF CYCLES BY E = ', CNUM
	ALLOCATE(IMGLML1M1(LMAX,MNUM,EPNTS))
	CCOUNTER = 0
C	CNUM - ����� ������ ����� �� L � M(CNUM = ����� M) CNUM = 2*(((1+((LMAX-1)*2))/2)*((((LMAX-1)*2)-1)/2+1)
C	MNUM - ������������ ���������� MSMALL �� ������ ��� ������ �������� L
C	��� ���� LSMALL = L-1, MSMALL = M-1
C	L ������ ��� ����, ����� ������������ ��� � �������� ������� � ��������, ������� �� �� ����� ���� ����� 0, ��� LSMALL
	ALLOCATE(DSTATES(LMAX,MNUM,EPNTS), DTOTAL(EPNTS), DL(EPNTS))
C	OPEN(60, FILE = 'G_FOR_ALL_L_M.TXT', FORM = 'FORMATTED')
      DO L = 1, LMAX
      L1 = L
	WRITE(*,*)
	MC = 0
C	������ ����� �� M
	DO MSMALL = -(L-1), (L-1)
	MC = MC + 1
C	��������� �������� ������� ������ ����� �� (L, M)
	MTIME=0.D0
	CALL TTIME(MTIME, TMTIME,0)
      MSMALL1 = MSMALL
	WRITE(*,*)
	CCOUNTER = CCOUNTER + 1
	WRITE(*,*) CCOUNTER,')'
	WRITE(*,*) 'LSMALL = ', L-1
	WRITE(*,*) 'M = ', MSMALL
	WRITE(*,*) 'LSMALL1 = ', L1-1
	WRITE(*,*) 'M1 = ', MSMALL1
     	I =1
  	I_=1
      X = 1
	X1 = 1
	J = 1
	NA = 1
	AL = 1
	NA1 = 1
	AL1 = 1
	ALPHA = 1
C	TIME_OF_ALL=0.D0
C	CALL TTIME(TIME_OF_ALL,TCTIME,0)
C	TIME = 0.D0
C	CALL TTIME(TIME, TATIME,0)

C	CALL RESULT4(I, CB, MSMALL, RO(ALPHA))
C	WRITE(*,*) 'CB FOR I =',I,' IS EQUAL TO', CB
C	CALL RESULT4(I_, CB1, MSMALL1, RO(ALPHA))
C	WRITE(*,*) 'CB1 FOR I_ =',I_,' IS EQUAL TO', CB1
C	CLOSE(70)
	
C	CB �������� ������������ ������������ CJ �� BESSEL, CB=CJ*J+CY*Y
C	CB1=CJ1*J1*CY1*Y1
C	1 - ��� �����
C	������� �������� ��������� ����� ������������� �� M,N,P � ������� ������� BIG(RMT1,RMT2,C,L,L1,M,M1), 
C	������� ���������� � ����� GFUNCTION.FOR
C	WRITE(*,*) 'BIG IS EQUAL TO', BIGRES
C	WRITE(*,*)
C	WRITE(*,*) 'LONG PART OF CALCULATION STARTED ON...' 
C	WRITE(*,*)
C	CALL TIMESTAMP()
C	WRITE(*,*)
C	WRITE(*,*) 'PLEASE WAIT...'
C	WRITE(*,*)
C	WRITE(*,*)
C	WRITE(*,*) 'TOTAL NUMBER OF POINTS IS: ',EPNTS
C	WRITE(*,*)

C	������������ ������� ������� ������� ����
C	OPEN(68,FILE='BESSJ.TXT', FORM = 'FORMATTED')
C104	FORMAT(I3, 3F10.2)
C	DO X = 1, 25
C		XBES = DBLE(X)
C		CALL BESSYPROC(0,XBES,EpsBES,Y1,Y11)
C		CALL BESSYPROC(1,XBES,EpsBES,Y2,Y21)
C	    CALL BESSYPROC(2,XBES,EpsBES,Y3,Y31)
C		WRITE(68,104) X, Y1, Y2, Y3
C	END DO
C
C	CLOSE(68)

C	������ ���������, ������� ����������� ��� ��������� � : A(L,N, ALPHA), B(L,N, ALPHA), ��� �������� ��������� � 
C	������� ASMALL(I,J), BSMALL(I,J), ASMALL1(I,J), BSMALL1(I,J) 
C	WRITE(*,*)
C	WRITE(*,*) 'CALCULATION OF ASMALL(I,J), BSMALL(I,J) STARTED.'
 
C	OPEN(68,FILE='ASMALL.TXT',FORM = 'FORMATTED')
C	OPEN(69,FILE='BSMALL.TXT',FORM = 'FORMATTED')
C	OPEN(70,FILE='ASMALL1.TXT',FORM = 'FORMATTED')
C	OPEN(71,FILE='BSMALL1.TXT',FORM = 'FORMATTED')
	DO I = 1, COUNTX
		DO J = 1, NPNTS
			CALL ALBL(L, MSMALL, J, I, ASMALL(I, J), BSMALL(I, J))
			CALL ALBL(L1, MSMALL1, J, I, ASMALL1(I, J), BSMALL1(I, J))
C			WRITE(68,*) ASMALL(I,J)
C			WRITE(69,*) BSMALL(I,J)
C			WRITE(70,*) ASMALL1(I,J)
C			WRITE(71,*) BSMALL1(I,J)
		END DO
	END DO
C	WRITE(*,*) 'CALCULATION OF ASMALL(I,J), BSMALL(I,J) FINISHED '
C	CLOSE(68)
C	CLOSE(69)
C	CLOSE(70)
C	CLOSE(71)
C	WRITE(*,*)

C	������ ���������, ������� ����������� ��� ��������� � : -1**(�+�1)*EXP*CB*CB1, ��� �������� ��������� � 
C	������ OECBAR(I,I_)
C	WRITE(*,*) 'CALCULATION OF OECBAR(I,I_) STARTED' 
  101	FORMAT(F10.2,I4,I4)
	DO I = 1, COUNTX 
		DO I_ = 1, COUNTX
			CALL RESULT4(I, CB,MSMALL, RO(ALPHA))
			CALL RESULT4(I_, CB1, MSMALL1, RO(ALPHA))
			KP_SMALL=2*PI/C * P(I)
			KP_SMALL1 = 2*PI/C * P(I_)
			Z = RSZ(AL)
			Z1 = RSZ(AL1)
			KPZ = KP_SMALL * Z - KP_SMALL1 * Z1
			MFI = M(I)*PHI(AL) - M(I_)*PHI(AL1)
			RES14 = DCMPLX(DCOS(KPZ+MFI),DSIN(KPZ+MFI),8)    
			OECBAR(I,I_) = DCMPLX(CB * CB1 *
	+		 (-1)**DBLE((M(I) + M(I_))))*RES14
		END DO		
	END DO
C	WRITE(*,*) 'CALCULATION OF OECBAR(I,I_) FINISHED'
C	WRITE(*,*) 
C	WRITE(*,*) 'CALCULATION OF FIRST POINT BY E STARTED'
C	WRITE(*,*)
	BIGRES = BIG(RMT,RMT, L-1,L1-1,MSMALL,MSMALL1)

C**********************************************************************************************************
C	������� ����� �������� �� ������� - ���� �� �������� E
C**********************************************************************************************************

	DO X = 1, EPNTS
		E = EMIN+(DBLE(X-1))*ESTEP
		CALL RESULT5(DE,L,L1,MSMALL,MSMALL1, E)
		SUM1 = (0.D0, 0.D0)
		TIME_LOCAL=0.D0
	    SS = (0.D0, 0.D0)
	    SSS = (0.D0, 0.D0)
			DO I = 1, COUNTX 
				SUM2 = (0.D0, 0.D0)
				SS = (0.D0, 0.D0)			
				DO I_ = 1, COUNTX
					SS = (0.D0, 0.D0)	
					CALL RESULT3(NPNTS1, L, L1, FA, E, I, I_)
					CALL RESULT3(NPNTS, L, L1, FB, E, I, I_)
					STEP = (2*PI/C)/NINTERVALS
					DO T = NPNTS1, (NPNTS-2)
						CALL RESULT3((1+T), L, L1, F, E, I, I_)
						SS = SS + F
					END DO
					SS = DCMPLX(2.D0 * DBLE(STEP),0.D0)*
	+				(SS + (FA + FB)/2)
					SSS = SS*OECBAR(I,I_)
					SUM2 = SUM2 + SSS
				END DO
				SUM1 = SUM1 + SUM2
			END DO
		IMGLML1M1(L,MC,X) = (1/(ALPH(L,E)*ALPH(L1,E)))*
	+	(DCMPLX(DE,0.D0)-PI*BIGRES*SUM1)
		WRITE(15,*) IMGLML1M1(L,MC,X),L,MC
		DSTATES(L, MC, X) = ALPH(L,E)*(SQRT(E)-
	+	DBLE(IMGLML1M1(L,MC,X)))/PI

	WRITE(*,1) '.'
	END DO
	
	WRITE(*,*)
C	WRITE(*,*) '!!! Calculation for this l, m finished !!!'
C	��������� ����� �� �
	WRITE(*,*)
C	����� ������������ ������� �� 1 ���� �� (L, M)
	CALL TTIME(MTIME, TMTIME,0)
C	WRITE(*,*)
C	RTIME = MTIME * (CNUM-CCOUNTER)
	WRITE(*,*) 'TIME REMAINING: ', MTIME,' * ',(CNUM-CCOUNTER)
	END DO


C	��������� ����� �� M
	END DO
	CLOSE(15)
C	��������� ����� �� L

C**********************************************************************************************************
C	������������ � ����� ������ ��������� ��������� ��� ���� L, M
C**********************************************************************************************************

C	��������� ��������� ��� ������� � � ������ ��������� � ���� 'OUT_GREEN'
	OPEN(31, FILE='OUT_GREEN', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DTOTAL(X) = 0.D0
C	����� ������ ����� �� ������� ������������� �������� DTOTAL
	DO L = 1, LMAX
	 DO MC = 1, MNUM
		DTOTAL(X) = DTOTAL(X) + DSTATES(L, MC, X)	
	 END DO
	END DO
	E = EMIN+(DBLE(X-1))*ESTEP
	WRITE(31,*) E, DTOTAL(X)
	END DO
	CLOSE(31)
C**********************************************************************************************************
C	����� ��������� ��������� ��� ��������� L
C**********************************************************************************************************

C	LSMALL=0
	OPEN(40, FILE = 'OUT_GREEN_S', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + DSTATES(1, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(40,*) E, DL(X)
	END DO
	CLOSE(40)

C	LSMALL=1
	OPEN(40, FILE = 'OUT_GREEN_P', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + DSTATES(2, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(40,*) E, DL(X)
	END DO
	CLOSE(40)

C	LSMALL=2
	OPEN(40, FILE = 'OUT_GREEN_D', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + DSTATES(3, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(40,*) E, DL(X)
	END DO
	CLOSE(40)

C**********************************************************************************************************

	WRITE(*,*)
	WRITE(*,*)
	WRITE(*,*) '!!! FULL CALCULATION FINISHED !!!'
	WRITE(*,*) 'Look for results in files Out_green'
	WRITE(*,*)


C**********************************************************************************************************
C	����������� �����
C**********************************************************************************************************
	
      WRITE(*,*)	 
C	CALL BEEPQQ(FREQUENCY, DURATION)
	CALL TIMESTAMP()
	CALL TTIME(TIME_OF_ALL,TCTIME,0)
C	���������� ������, �������� ��������
C	CLOSE(60)
C	CLOSE(61)
C	CLOSE(64)
	CLOSE(65)
	CLOSE(66)
C	������ G � ����
	
	
	WRITE(*,*) "ENTER NAMES OF FILES TO DELETE: "
	LEN = GETSTRQQ(FILE)
	IF (FILE(1:LEN) .EQ. '*.*') THEN
	WRITE(*,*) "ARE YOU SURE (Y/N)?"
	CH = GETCHARQQ()
	IF ((CH .NE. 'Y') .AND. (CH .NE. 'Y')) STOP
	END IF
	COUNT = DELFILESQQ(FILE)
	WRITE(*,*) "DELETED ", COUNT, " FILES."

	WRITE(*,*) 'PRESS ENTER TO EXIT' 
      READ(*,*)

C************************************************************************************************************
C	������� � ��������� 
C************************************************************************************************************

	CONTAINS

C************************************************************************************************************

	SUBROUTINE ALBL(L, MSMALL, J, I, ALNAL, BLNAL)
	INTEGER I, J, L, MSMALL
	DOUBLE COMPLEX ALNAL, BLNAL
C	������� ������� ����� ��� ������� ���� A(L,N,ALPHA), B(L,N,ALPHA) ��� ���� K, MNP
	 K=(-PI/C)+(DBLE(2*(J-1))*PI)/(DBLE(NINTERVALS)*C)
	 KP=K+(2*PI/C)*DBLE(P(I))
	 I1=SIMP_I12(EPSI, RMT, KP, KAPPA(I), IABS(MSMALL), L-1, 1) 
	 I2=SIMP_I12(EPSI, RMT, KP, KAPPA(I), IABS(MSMALL), L-1, 2)
       ALNAL = I2*DCMPLX(PSIE(L))-I1*DCMPLX(PSIER(L))
	 BLNAL = I1*DCMPLX(PSIR(L))-I2*DCMPLX(PSI(L))
	END SUBROUTINE
C************************************************************************************************************

	DOUBLE COMPLEX FUNCTION ANBE(L, AS, BS, E)
	INTEGER L
	DOUBLE PRECISION E
	DOUBLE COMPLEX AS, BS
C	NA - ����� ������������ ������
C	X - ����� ������ � ������� PEXT
C	PEXT - ������, ���������� ������ �� �������� P, ������� ���������� ���� �� ����� 
	ANBE = AS+DCMPLX(NLALPHA(L))
	+*BS*DCMPLX((E-EL(L)))
C	�������� �� ������: ANBE
C	����� ��������� ����� ��������� ��� ����. ��� ��� ��� L', MSMALL', N', ALPHA'
	END FUNCTION ANBE
C************************************************************************************************************

	SUBROUTINE RESULT3(J, L, L1, RES13, E, I, I_)
C	EXTERNAL RESULT1
C	��������� RESULT3 ����� ��������� �������� �������������� ���������������� ��������� - ����� ��� ������
C	�� ������ ������� ���������� �� AMNP, AMNP* � �� ��� ��������� � RESULT1.
C	��������� ��������� � ���������� ���������� RES10
	DOUBLE COMPLEX RES10, RES13, RES6, RES, RES5
	DOUBLE PRECISION RES11, E
	INTEGER J, L, L1, I, I_
C1	FORMAT(A1\)
C2	FORMAT(I3\)
	RES10 = (0.D0,0.D0)
	DO LAMBDA = 1, COUNTX
	 RES11 = E - BIGD(J, LAMBDA)
C	 COUNTING = 0
	 IF (ABS(RES11) <= EDELTA) THEN
	  RES10 = RES10 + DCMPLX(DELTANUM(EPSD, RES11))*BIGH(J, I, LAMBDA)
	+*DCONJG(BIGH(J, I_, LAMBDA))
	 END IF 
	END DO
C	CALL RESULT1(J, L, L1,I, I_, RES12, E)
      RES=ANBE(L, ASMALL(I,J), BSMALL(I,J), E)*
	+DCONJG(ANBE(L1, ASMALL1(I_,J), BSMALL1(I_,J), E))
C	������� RES3, I_ �������� I-�����, � ��������� ������� ����� ��������� 1, �������� L1 - ��� L-�����	 
	K=(-PI/C)+(DBLE(2*(J-1))*PI)/(DBLE(NINTERVALS)*C)
	Z = RSZ(AL)
	Z1 = RSZ(AL1)
	RES3 = K*(Z-Z1)
C	������� RES5 ����������� ������, ��� �������������� ����� ����� COS(RES3), � ������ SIN(RES3)
	RES5 = DCMPLX(DCOS(RES3),DSIN(RES3),8)
	RES6 = RES5*RES
C	RES13 = RES10*RES12
	RES13 = RES10*RES6
	END SUBROUTINE RESULT3

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION ALPH(L, E)
	INTEGER L
	DOUBLE PRECISION E
	ALPH = 1 + ((E-EL(L))**2)*NLALPHA(L)
	END FUNCTION

C************************************************************************************************************

C	��� SUBROUTINE ��������� �������� CJ*J+CY*Y
	SUBROUTINE RESULT4(I, RES4, MSMALL, RNA)
	INTEGER I,MSMALL, NBES
	
	DOUBLE PRECISION BESJ, BESJ1, BESY, BESY1,RES4, RNA, XBES
	EXTERNAL BESS, BESSYPROC
  103	FORMAT(I3, 3F15.3)
	XBES = KAPPA(I)*RNA
	NBES = MSMALL-M(I)
	CALL BESS(NBES, XBES, BESJ, BESJ1)
	CALL BESSYPROC(NBES, XBES, EPSBES,BESY, BESY1)
	RES4 = CJ(I)*BESJ+CY(I)*BESY
	WRITE(67,103) NBES, XBES, BESJ, BESY
	
      END SUBROUTINE RESULT4 

C************************************************************************************************************ 	
C	 ��� SUBROUTINE ��������� �������� ������� ���������� ������ ����� � �������� ��������� ���������
    	SUBROUTINE RESULT5(DDDDE,L,L1,MSMALL,MSMALL1,E)
	EXTERNAL DELTA
	INTEGER DELTA,L, L1, MSMALL, MSMALL1
	DOUBLE PRECISION DDDDE, E
	DDDDE = DBLE(DELTA(NA,NA1)*DELTA(AL,AL1)*DELTA(L,L1)*
	+DELTA(MSMALL,MSMALL1))*SQRT(E)*ALPH(L,E)*ALPH(L,E)
C	WRITE(*,*) 'DELTA ARE EQUAL TO', DELTA(NA,NA1), DELTA(AL, AL1),
C	+DELTA(L,L1), DELTA(MSMALL, MSMALL1)
	END SUBROUTINE


C************************************************************************************************************

	END PROGRAM GREEN

