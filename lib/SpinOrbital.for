
C	 ��������� ���������� ����-������������ �������������� � ����������� ������� ����
C	 ������ 3.14159

C************************************************************************************************************

C	 ������:
C	 ������������� �����: ������� �.�.
C	 ���������: ��������� �.�.
C

C************************************************************************************************************

C	 ���� ������ ����������: ����, 2011 �.

C************************************************************************************************************

C	�������� ���������� � ���������: 

C	* ����������� ��������� ����� ��� �������� ���� ����� FORTRAN (��� ������� ��� ������ ������ ���� *.FOR)
C	* ��� ����� ������ *.F90 ��������� ����� ��� ���������� �� ����������� (��. ���������)
C	* � ���� ��������� ��� ���������� ������ ���� ���� ��������� � �����(������� ���������)
C	* ����� �� ����� ���������� ������������ ������ ���������
C	* � ��������� ����������� ���������� ����������� ��������� ���������

C	�������� ������:

C	* ���� ���-�� � ��� ���� ����, ��������� ��� �����, ��� �� ���������	����������
C	* ����������� ������ ��������� WRITE(*,*), READ(*,*), �������� � MS FPS 4.0 �������
C	* �������� ������������� ���������� �� �����, ����� ������, ��� �� ���
C	* ��� ������������� DCMPLX(a, b, 8) ����� ��������� � ����� 8, ����� � �� �����������, ���
C		��������� ���� � ��� ��.


C************************************************************************************************************

C	��������� ���������: 
C	(����� ���������� ��������� ��������� - �����, ���� � ���� ���������) 

C************************************************************************************************************
	
	PROGRAM SpinOrbital
	
	IMPLICIT NONE
	
C************************************************************************************************************
C	������� ��� ����� � ������, ��������� �� ������������� ��������� ��� �������� ��������
C************************************************************************************************************

100	FORMAT(F23.20)
1	FORMAT(A1\)
17	FORMAT(F5.3,1X,F9.3)

C************************************************************************************************************
C	���������
C************************************************************************************************************

C	����� ������� � ����� �� R
	INTEGER, Parameter:: NGRID = 300

C	����� ����� ������
C	INTEGER, Parameter:: NT = 1

C	����� ��
	DOUBLE PRECISION, Parameter:: 
	+PI = 3.14159265358979323846264338328D0

C	�������� ����� � ���������
	DOUBLE PRECISION, Parameter:: 
C	+LightSpeed = 299792458.D0
	+LightSpeed = 274.0746D0

C	������� ����� �������� R, ������� �������� � ������ R, �� ������� �������������(�� ������������)
	DOUBLE PRECISION, Parameter:: deltaRho = 1.0D-5

C	������������ l, ��� ���� L = l+1, ��� ��� l=0 ����� ����, � ������� � Fortran �� ����� ������ 0	
C	INTEGER, Parameter:: NLMAX = 8

C	����������� �������� �� � ��������	
      DOUBLE PRECISION, Parameter:: evs = 13.6058D0

C	������ ������� � ����������� �������
	DOUBLE COMPLEX, PARAMETER :: IM=(0.D0, 1.D0)
	DOUBLE COMPLEX, PARAMETER :: RE=(1.D0, 0.D0)

	DOUBLE PRECISION, PARAMETER:: EPSBES = 1.0D-12
C	�������� ���������� ������� �������

	CHARACTER, PARAMETER :: JOB*1 = 'V'
C	Constraint: JOB = 'N' or 'V'.

	CHARACTER, PARAMETER :: UPLO*1 = 'L'

C************************************************************************************************************
C	���������� ����������, � ��� ����� ��������
C************************************************************************************************************

	DOUBLE COMPLEX SIMP_I12, ASMALL, BSMALL, Res2, Res3, Ress, SUM,
	+Res, ResAdd, Element1, Element2 

	DOUBLE COMPLEX, ALLOCATABLE :: AHA(:,:), BHA(:,:), AHB(:,:),
	+BHB(:,:), BHA2(:,:), AHB2(:,:), HamAHA(:,:), HamBHA(:,:),
     +HamAHB(:,:), HamBHB(:,:), HamBIG(:,:), 
	+H(:,:), BIGH(:,:,:), 
	+OECBAR(:,:), SP(:,:), BIGSP(:,:,:), ASM(:,:,:,:), 
	+BSM(:,:,:,:), ASM_2(:,:,:,:), BSM_2(:,:,:,:),
     +ASM_3(:,:,:,:), BSM_3(:,:,:,:), WORK(:), HamTest(:,:)

	DOUBLE PRECISION A, B, C, RMT, TIME,
	+EPSI, A5, CALCCYFORGFM, CALCCJFORGFM,
	+EFERMI, ResDiv,
	+VurduIntValue, Vu2drIntValue, ZetaInt1Value,
	+ZetaInt2Value, ZetaInt3Value,
	+VdurdduIntValue, Vdu2drIntValue, SubInt3Value, dy,
	+Res1, Res41, Res42, Res5, K, kStart, kStop

	DOUBLE PRECISION, ALLOCATABLE :: KAPPA(:), CJ(:), CY(:), PSI(:,:),
	+PSIR(:,:), PSIE(:,:), PSIER(:,:), NLALPHA(:,:), DE(:), RO(:),
     + PHI(:), RAD(:,:),V(:,:), VR(:,:), EBig(:,:),
     +RSZC(:), EL(:,:), BIGD(:,:), BIGD0(:),   
	+PSIRALL(:,:,:), PSIALL(:,:,:), PSIEALL(:,:,:), PSIERALL(:,:,:),
     +Dzeta1(:,:), Dzeta2(:,:), Dzeta3(:,:), RWORK(:), D(:) 

	INTEGER NT, IT, COUNTX, I, I_, N1, N2, J, NPNTS, NLMAX,
	+NINTERVALS, LMAX, MNUM, JJ, II, 
	+MSMALL, L, X, MC, ALPHA,
	+LENH, LEND, LENS, NATOMS,
	+A1, A2, A3,
	+JRIS, LWORK, INFO, LASTI, LASTJ

	INTEGER, ALLOCATABLE :: M(:), N(:), P(:)
C	M, N, P - ��������� �������� �������, ��������� �����

	CHARACTER(1) CHAR

	LOGICAL WriteToFile
C************************************************************************************************************
C	�����
C************************************************************************************************************

	WRITE(*,*) '********************************************'
	WRITE(*,*) 'Spin-Orbital Interaction in Nanotubes'
	WRITE(*,*)
	WRITE(*,*) 'Theory by P.N.D''yachkov'
	WRITE(*,*)
	WRITE(*,*) 'Programmed by D.Z.Kutlubaev'
	WRITE(*,*)
	WRITE(*,*) 'Kurnakov Institute of General and Inorganic Chemistry'
	WRITE(*,*) 'Of the russian academy of sciences'
	WRITE(*,*)
	WRITE(*,*) 'Moscow, 2011'
	WRITE(*,*) '********************************************'
	WRITE(*,*)
	WRITE(*,*) 'Today:'
	CALL TIMESTAMP()
	WRITE(*,*)

C************************************************************************************************************
C	HADRCODE - ���������, ��������� � ���������, ������� ������ ���� �������� ����� � ���� ������� ������
C************************************************************************************************************
     
C	����� ����� ������
      NT = 1
C	l ������������ + 1, �� ���� ���� LMAX = 2, �� l ������������ ����� 1
	LMAX = 2
C	���� WriteToFile = .TRUE., �� �������� BHA, AHB, HamAHB � �.�. ������������ � ����, ��� ���� CycleJ ���� ��������� �������
	WriteToFile = .TRUE.

C	Test
C	WRITE(*,*) 'TEST'
C	WRITE(*,*) 'POINT1' 
C	CountX = 2
C	INFO=0
C	LWORK = 64 * CountX
C	ALLOCATE(RWORK(7*CountX), WORK(LWORK), D(CountX),
C	+ HamTest(2,2))
C	WRITE(*,*) 'POINT2'
C	HamTest(1,1) = (5.d0,0.d0)
C	HamTest(2,1) = (2.d0,-1.d0)
C	HamTest(1,2) = (2.d0,1.d0)
C	HamTest(2,2) = (10.d0,0.d0)
C	WRITE(*,*) 'POINT3'
	 
c --- Here we call NAG/LAPACK routine for eigenvalues and eigenvectors ---
C      CALL F02HAF(JOB,UPLO,COUNTX,HamTest,COUNTX,D,
C	+RWORK,WORK,LWORK,INFO)
c     -- after this routine, D contain eigenvalues,         --
c     -- H - corresponding eigenvectors in orthogonal basis set --
C	WRITE(*,*) 'POINT4'  
C	WRITE(*,*) 'EigenValues ='
C	WRITE(*,*) D

C************************************************************************************************************
C	����������� �������� RAD, V, VR
C************************************************************************************************************

C	Rad - ������ ����� �� r � ��������������� �����
C	NGRID - ����� ����� � �����
C	V - ������ �������� ����������
C	VR = V*R
	ALLOCATE(RAD(NGRID,NT), V(NGRID,NT), VR(NGRID,NT)) 

C************************************************************************************************************
C	���������� COUNTX, EFERMI, KAPPA, M, N, P, A, B, C, EPSI, NATOMS, JRIS, RMT, RAD
C************************************************************************************************************

C	���������� ������ �� EF.DAT - ������� �����
	OPEN(1, FILE = 'EF.DAT', FORM = 'FORMATTED')
	READ(1,17) A5, EFERMI
	CLOSE(1)
	WRITE(*,*) 'EFERMI IS READ AS = ', EFERMI
	EFERMI = EFERMI/EVS
	WRITE(*,*) 'EFERMI IN RIDBERGS = ', EFERMI

	WRITE(*,*) 'JRIS, NT = ', JRIS, NT 

	OPEN(65, FILE = 'BIGH.TXT', FORM = 'FORMATTED')
	OPEN(66, FILE = 'BIGD.TXT', FORM = 'FORMATTED')
	CLOSE(32)

	OPEN(1, FILE = 'indefect9.dat', FORM = 'FORMATTED')
	READ(1,*) COUNTX
	ALLOCATE (KAPPA(COUNTX), M(COUNTX), N(COUNTX), P(COUNTX),
	+ CJ(COUNTX), CY(COUNTX), H(COUNTX,COUNTX), D(COUNTX),
     + OECBAR(COUNTX,COUNTX), SP(COUNTX,COUNTX))
	READ(1,*) KAPPA
	READ(1,*) A, B, C
	DO I = 1, COUNTX
	READ(1,*) M(I), N(I), P(I)
	END DO
	CLOSE(1)

	WRITE(*,*) 'M, N, P, KAPPA ARE READ AS FOLLOWING ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		WRITE(*, *) M(I), N(I), P(I), KAPPA(I)
	END DO

	WRITE(*,*)
	WRITE(*,*) 'CYLINDER PARAMETERS A, B AND C ARE READ AS FOLLOWING
	+ ...'
	WRITE(*,*)
	WRITE(*,*) A, B, C

      
	WRITE(*,*)
	OPEN(11, FILE = 'CJ2.TXT', FORM = 'FORMATTED')
	OPEN(12, FILE = 'CY2.TXT', FORM = 'FORMATTED')							 
	WRITE(*,*) 'CJ(I) ARE CALCULATED ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		CJ(I) = CALCCJFORGFM(M(I), A, B, KAPPA(I))
C		WRITE(*,*) CJ(I)
		WRITE(11,*) I, CJ(I)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'CY(I) ARE CALCULATED ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		CY(I) = CALCCYFORGFM(M(I), CJ(I), B, KAPPA(I))
C		WRITE(*,*) CY(I)
		WRITE(12,*) I, CY(I)
	END DO
	CLOSE(11)
	CLOSE(12)

	OPEN(13, FILE = 'EPSI_NATOMSG.TXT', FORM = 'FORMATTED')
	READ(13,*) EPSI, NATOMS
	CLOSE(13)
	WRITE(*,*) 'EPSI AND NATOMS ARE READ AS'
	WRITE(*,*) EPSI, NATOMS

C	����� ���������� ����� ������ � ������������ ������ NATOMS ����� ���������� ������� �������������� ���������

	ALLOCATE(RO(NATOMS), PHI(NATOMS), RSZC(NATOMS))

C************************************************************************************************************
C	 ���������� �������������� ���������, RO - RHO, PHI, RSZC - Z; ALPHA - ����� ����� � ������
C************************************************************************************************************
	
     	WRITE(*,*) 'CYLINDRICAL COORDINATES ARE READ AS FOLLOWING: '   
   	OPEN(24, FILE = 'CYLINDRICALCOORDINATESG.TXT', FORM = 'FORMATTED')
	DO ALPHA=1,NATOMS
		READ(24, '(A1,3F20.10)') CHAR, RO(ALPHA),PHI(ALPHA),RSZC(ALPHA)
		WRITE(*, '(3F20.10)') RO(ALPHA),PHI(ALPHA),RSZC(ALPHA)
	END DO

C************************************************************************************************************
C	 ���������� JRIS, RMT, RAD, NPNTS, NLMAX, LENH, LEND, EPSI
C************************************************************************************************************

	OPEN(1, FILE = 'indefect2.dat', FORM = 'FORMATTED')
	READ(1,*) JRIS, RMT
	READ(1,*) RAD
	CLOSE(1)

	OPEN(15, FILE = 'NPNTS_LMAX_LENH_LENDG.TXT', FORM = 'FORMATTED')
	READ(15, *) NPNTS, NLMAX, LENS, LENH, LEND
	WRITE(*,*) 'NPNTS,NLMAX, LEN ARE READ'
C	NLMAX - ��� ������������ �������� L ���������(��������� �����)
	CLOSE(15)
	WRITE(*,*) 
	WRITE(*,*) 'EPSI, NPNTS AND RMT, LENH, LEND'

	OPEN(1, FILE = 'kStart_kStop.txt', FORM = 'FORMATTED')
	READ(1, *) kStart, kStop
	CLOSE(1)
	WRITE(*,*) 'kStart = ', kStart
	WRITE(*,*) 'kStop = ', kStop

C	EPSI - ����������� ���������� ����������, ������� �� �������� ����� INLACW � ���������
C	OVER55D.EXE, � �� ����� ��� �� OVER55D.EXE, STRCYD.EXE � ATOMD.EXE ��� ������������� ��������
C	����� INLACW, INATOM 
C	RMT - ������ ������-��� �����

	WRITE(*,*)
	WRITE(*,*) EPSI, NPNTS
	WRITE(*,*) RMT, LENH, LEND
	WRITE(*,*)

C************************************************************************************************************
C	���������� Psi, PsiR, PsiE, PsiER
C************************************************************************************************************

	WRITE(*,*) 'Reading Psi, PsiR, PsiE, PsiER...'
C	PSI - ��� ������ ���������� �������� ������� U
C	PSIE - ��� ������ �� ����������� �� �������
C	PSIR - ����������� �� �������
C	PSIER - ����������� �� ������� � �������

	ALLOCATE(PSI(NLMAX,NT), PSIR(NLMAX,NT), PSIE(NLMAX,NT),
	+ PSIER(NLMAX,NT),
	+ EL(NLMAX,NT), NLALPHA(NLMAX,NT))			 
      OPEN(16, FILE = 'PSI.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(16,*) PSI(L, IT), A1, A2, A3
C			WRITE(*,*) L, IT, PSI(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(16)

	OPEN(17, FILE = 'PSIR.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(17,*) PSIR(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(17)
	 
	OPEN(18, FILE = 'PSIE.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(18,*) PSIE(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(18)
	 
	OPEN(19, FILE = 'PSIER.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(19,*) PSIER(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(19)

C************************************************************************************************************

	MNUM = (LMAX-1)*2 + 1

C	����� ���������� �� k
	NINTERVALS = NPNTS - 1

C	����� ����, ��� ������ ������� ��������, �������� ��� ��� ������

	ALLOCATE(BIGH(NPNTS,COUNTX,COUNTX), BIGD(NPNTS,COUNTX),
	+BIGD0(COUNTX),
     +BIGSP(NPNTS,COUNTX,COUNTX), ASM(LMAX, MNUM, COUNTX, NPNTS),
	+BSM(LMAX, MNUM, COUNTX, NPNTS), ASM_2(LMAX, MNUM, COUNTX, NPNTS),
	+BSM_2(LMAX, MNUM, COUNTX, NPNTS),
     + ASM_3(LMAX, MNUM, COUNTX, NPNTS),
	+BSM_3(LMAX, MNUM, COUNTX, NPNTS), EBig(NPNTS,CountX*2))

C	���������� ��� ��������������
	INFO=0
	LWORK = 64 * CountX * 2
	ALLOCATE(RWORK(7*CountX*2), WORK(LWORK))

	ALLOCATE(AHA(CountX, CountX),
	+ BHA(CountX, CountX), AHB(CountX, CountX),
     + BHB(CountX, CountX), BHA2(CountX, CountX),
	+ AHB2(CountX, CountX),
     + HamAHA(CountX, CountX),
	+ HamBHA(CountX, CountX),
     + HamAHB(CountX, CountX),
     + HamBHB(CountX, CountX),
     + HamBIG(CountX*2, CountX*2), DE(CountX*2))

C	* ����� HamBIG - ������������, ����������� � ��� ���� � ������ ����� ��� ������ ����� �� k - J

C************************************************************************************************************
C  	���������� JRIS, RMT, RAD(X,IT)
C************************************************************************************************************

	    
	OPEN(1, FILE = 'indefect2.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		READ(1,*) JRIS, RMT
		READ(1,*) RAD(:,IT)
	END DO
	CLOSE(1)

	ALLOCATE(PSIALL(NT, NLMAX, JRIS),PSIEALL(NT, NLMAX, JRIS),
	+ PSIRALL(NT, NLMAX, JRIS),PSIERALL(NT, NLMAX, JRIS))


C************************************************************************************************************
C	���������� V(X,IT), VR(X,IT)
C************************************************************************************************************
	
	WRITE(*,*) 'NT = ', NT

	OPEN(1, FILE = 'indefect3.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		READ(1,*) V(:,IT)
	END DO
	CLOSE(1)

	WRITE(*,*) 'V is read successfully...'
	WRITE(*,*) 

	DO IT = 1, NT
		DO X = 1, JRIS
			VR(X,IT) = V(X,IT)*RAD(X,IT)
		END DO
	END DO	 
	

C************************************************************************************************************
C	���������� PSIALL(NT, NLMAX, EPNTS), PSIEALL(NT, NLMAX, EPNTS), PSIRALL, PSIERALL
C************************************************************************************************************

	WRITE(*,*) 'NT, NLMAX, JRIS	= ', NT, NLMAX, JRIS

	WRITE(*,*)
      WRITE(*,*) 'PSIALL Reading started...'
	WRITE(*,*)
  	OPEN(1, FILE = 'indefect4.dat', FORM = 'FORMATTED')

	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS

	READ(1, *) PSIALL(IT, L, X), A1, A2, A3
	PSIALL(IT,L,X) = PSIALL(IT, L, X)/RAD(X,1)

	END DO
	END DO
	END DO
	CLOSE(1)
	WRITE(*,*) 'U(:,:,:) is read successfully.'
	WRITE(*,*)

 	OPEN(1, FILE = 'indefect5.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIEALL(IT, L, X), A1, A2, A3
	PSIEALL(IT, L, X)=PSIEALL(IT, L, X)/RAD(X,1)
	END DO
	END DO
	END DO
	CLOSE(1)
	WRITE(*,*) 'Ue(:,:,:) is read successfully.'
	WRITE(*,*) 


	OPEN(1, FILE = 'PSIRALL.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIRALL(IT, L, X), A1, A2, A3

	PSIRALL(IT, L, X)=PSIRALL(IT, L, X)/RAD(X,1)

	END DO
	END DO
	END DO
	CLOSE(1)

	WRITE(*,*) 'Ur(:,:,:) is read successfully.'
	WRITE(*,*) 

	OPEN(1, FILE = 'PSIERALL.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIERALL(IT, L, X), A1, A2, A3

	PSIERALL(IT, L, X)=PSIERALL(IT, L, X)/RAD(X,1)

	END DO
	END DO
	END DO
	CLOSE(1)

	WRITE(*,*) 'Uer(:,:,:) is read successfully.'
	WRITE(*,*)
	   
C************************************************************************************************************
C	 ���������� ������� ������������� �������� ������� a � ������� �������� ������� � BIGH � BIGD
C************************************************************************************************************
	   
 	WRITE(*,*)
	WRITE(*,*) 'Reading of H and D arrays started ... '
	WRITE(*,*) 'Check IF H_AMNP is renamed... '

 	OPEN(22,FILE='H_AMNPG.TXT', ACCESS = 'DIRECT',
	+RECL = LENH+1000)
	DO J = 1, NPNTS
	 READ(22, REC=J)((H(II,JJ),JJ=1,COUNTX),II=1,COUNTX)
	 DO JJ = 1, COUNTX
	 DO II = 1, COUNTX
	  BIGH(J,II,JJ) = H(II,JJ)
	  END DO
	 END DO
	WRITE(*,1) '.'
	END	DO
	CLOSE(22)
	
	WRITE(*,*)
	WRITE(*,*) 'Reading H array finished...'

	OPEN(23,FILE='D_ENERGIESG.TXT',
	+ACCESS = 'DIRECT', RECL = LEND+1000)
	DO J=1, NPNTS
	 READ(23,REC=J) D 
	 DO II = 1, COUNTX
	  BIGD(J,II) = D(II)
	 END DO
	END DO
	CLOSE(23)
	WRITE(*,*)
	WRITE(*,*) 'Reading of H and D arrays finished ... '
	WRITE(*,*) 

C************************************************************************************************************
C	���������� �������� Dzeta1, Dzeta2, Dzeta3
C************************************************************************************************************

	ALLOCATE(Dzeta1(LMAX, NT), Dzeta2(LMAX, NT),
	+ Dzeta3(LMAX, NT))
	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)
	WRITE(*,*) 'Calculation of Dzeta1, Dzeta2, Dzeta3 arrays ... '
	WRITE(*,*) 'LMAX = ', LMAX
	WRITE(*,*)
	WRITE(*,*) 'NT=', NT

	DO IT = 1, NT
	DO L = 1, LMAX
	WRITE(*,*) '------------------------'
	WRITE(*,*) 'L = ', L
		CALL ZetaInt1(L,IT)
		CALL ZetaInt2(L,IT)
		CALL ZetaInt3(L,IT)
		Dzeta1(L, IT) = ZetaInt1Value
		Dzeta2(L, IT) = ZetaInt2Value
		Dzeta3(L, IT) = ZetaInt3Value
		WRITE(*,*) 'Dzeta1(L,IT), L, IT = ', Dzeta1(L,IT), L, IT
		WRITE(*,*) 'Dzeta2(L,IT), L, IT = ', Dzeta2(L,IT), L, IT
		WRITE(*,*) 'Dzeta3(L,IT), L, IT = ', Dzeta3(L,IT), L, IT
	END DO
	END DO

	WRITE(*,*)
	WRITE(*,*) 'Calculation of Dzeta arrays finished successfully.'
	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*) 
C************************************************************************************************************
C	������ �������� ASM, BSM - a, b(� ����������� I1, I2)
C************************************************************************************************************

C	��� ���� �������� k, ��� ���� �������� l, m, I ������� ����� �������� asmall, bsmall
C	������� �������� � �������� �������: 
C 	L = LSMALL + 1
C  	MC = MSMALL + LSMALL + 1 = MSMALL + L

	OPEN(1, File = 'ASM.txt', Form = 'FORMATTED')
	WRITE(1,*) 'ASM,L,MC,I,J' 
	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)
	WRITE(*,*) 'Calculation of ASM,BSM arrays ... '
	IT = 1
	WRITE(*,*) 'NT = ', NT
	WRITE(*,*) 'IT = ', IT
	WRITE(*,*) 'LMAX = ', LMAX
	WRITE(*,*) 'MNUM = ', MNUM
	WRITE(*,*) 'COUNTX = ', COUNTX
	WRITE(*,*) 'NPNTS = ', NPNTS
	WRITE(*,*)						  
	DO L = 1, LMAX
	WRITE(*,*) '------------------------'
	WRITE(*,*) 'L = ', L
		
	DO MSMALL = -(L-1), (L-1)
	WRITE(*,*) 'MSMALL = ', MSMALL
	MC = MSMALL + L
	WRITE(*,*) 'MC = ', MC

	DO I = 1, COUNTX
		DO J = 1, NPNTS
									
						IF (L==1) Then
							ASM(L, MC, I, J) = (0.d0, 0.d0)
							BSM(L, MC, I, J) = (0.d0, 0.d0)
							Cycle
						ENDIF 
	
			CALL ALBL_1(L, MSMALL, J, I, ASMALL, BSMALL)
			ASM(L, MC, I, J) = ASMALL
			BSM(L, MC, I, J) = BSMALL
		END DO
	END DO
	WRITE(*,*)
	END DO
	END DO
	CLOSE(1)
	WRITE(*,*) 'Calculation of ASM,BSM arrays finished successfully.'
	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)

C************************************************************************************************************
C	������ �������� ASM_2, BSM_2 - a, b(� ����������� I1, I2)
C************************************************************************************************************

C	��� ���� �������� k, ��� ���� �������� l, m, I ������� ����� �������� asmall, bsmall
C	������� �������� � �������� �������: 
C 	L = LSMALL + 1
C  	MC = MSMALL + LSMALL + 1 = MSMALL + L

	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)
	WRITE(*,*) 'Calculation of ASM_2,BSM_2 arrays ... '
	WRITE(*,*) 'LMAX = ', LMAX
	WRITE(*,*) 'MNUM = ', MNUM
	WRITE(*,*) 'COUNTX = ', COUNTX
	WRITE(*,*) 'NPNTS = ', NPNTS
	WRITE(*,*) 'IT = ', IT
	WRITE(*,*)
	DO L = 1, LMAX
	WRITE(*,*) '------------------------'
	WRITE(*,*) 'L = ', L
	DO MSMALL = -(L-1), (L-1)
	WRITE(*,*) 'MSMALL = ', MSMALL
	MC = MSMALL + L
	WRITE(*,*) 'MC = ', MC

	DO I = 1, COUNTX
		DO J = 1, NPNTS
						IF (MSMALL==(L-1)) Then
							ASM_2(L, MC, I, J) = (0.d0, 0.d0)
							BSM_2(L, MC, I, J) = (0.d0, 0.d0)
							Cycle
						ENDIF 
			CALL ALBL_2(L, MSMALL, J, I, ASMALL, BSMALL)
			ASM_2(L, MC, I, J) = ASMALL
			BSM_2(L, MC, I, J) = BSMALL
		END DO
	END DO
	WRITE(*,*)
	END DO
	END DO

	WRITE(*,*) 'Calculation of ASM_2,BSM_2 arrays finished...'
	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)

C************************************************************************************************************
C	������ �������� ASM_3, BSM_3 - a, b(� ����������� I1, I2)
C************************************************************************************************************

C	��� ���� �������� k, ��� ���� �������� l, m, I ������� ����� �������� asmall, bsmall
C	������� �������� � �������� �������: 
C 	L = LSMALL + 1
C  	MC = MSMALL + LSMALL + 1 = MSMALL + L

	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)
	WRITE(*,*) 'Calculation of ASM_3,BSM_3 arrays ... '
	WRITE(*,*) 'LMAX = ', LMAX
	WRITE(*,*) 'MNUM = ', MNUM
	WRITE(*,*) 'COUNTX = ', COUNTX
	WRITE(*,*) 'NPNTS = ', NPNTS
	WRITE(*,*) 'IT = ', IT
	WRITE(*,*)
	DO L = 1, LMAX
	WRITE(*,*) '------------------------'
	WRITE(*,*) 'L = ', L
	DO MSMALL = -(L-1), (L-1)
	WRITE(*,*) 'MSMALL = ', MSMALL
	MC = MSMALL + L
	WRITE(*,*) 'MC = ', MC

	DO I = 1, COUNTX
		DO J = 1, NPNTS
						IF (-MSMALL==(L-1)) Then
							ASM_3(L, MC, I, J) = (0.d0, 0.d0)
							BSM_3(L, MC, I, J) = (0.d0, 0.d0)
							Cycle
						ENDIF 
			CALL ALBL_3(L, MSMALL, J, I, ASMALL, BSMALL)
			ASM_3(L, MC, I, J) = ASMALL
			BSM_3(L, MC, I, J) = BSMALL 
		END DO
	END DO
	WRITE(*,*)
	END DO
	END DO

	WRITE(*,*) 'Calculation of ASM_3,BSM_3 arrays finished...'
	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
	WRITE(*,*)
	      
C************************************************************************************************************
C	�������� �����
C************************************************************************************************************

C	��� ������� �������� k ������� ������� H �������� �������, ����� ������� H �������������, ����� ������������� ��
	
	OPEN(100, FILE='SO_EnergiesAll.txt', FORM = 'FORMATTED')
	
      WRITE(*,*) 'Reading existing SO_EnergiesAll.txt'

	LASTJ = 0 
	LASTI = 0 
	EXT: DO J = 1, NPNTS
			LASTI = 0
			LASTJ = LASTJ + 1
			IF(.NOT.EOF(100)) THEN
			DO I = 1, CountX*2
				LASTI = LASTI + 1
				IF(.NOT.EOF(100)) THEN
					READ(100,*) K, EBig(J,I)
					WRITE(*,*) 'Read for K, I, J = ', K, I, J
				ELSE
					WRITE(*,*) 'File SO_EnergiesAll.txt is corrupt.'
					WRITE(*,*) 'Please delete it and restart'
					READ(*,*)
					GOTO 9999
				ENDIF	
	  		ENDDO
			ELSE 
				EXIT EXT
			END IF
	ENDDO EXT

	CLOSE(100)
	WRITE(*,*) 'LASTJ = ', LASTJ
	WRITE(*,*) 'LASTI = ', LASTI


	IF ((LASTI.NE.CountX*2).AND.(LASTI.NE.0)) THEN
		WRITE(*,*) 'File SO_EnergiesAll.txt is corrupt.'
		WRITE(*,*) 'Please delete it and restart'
		READ(*,*)
		GOTO 9999
	ELSE
		WRITE(*,*) 'File SO_EnergiesAll.txt is read successfully'
		WRITE(*,*) 'Starting from J = ', LASTJ
	ENDIF
 
	CycleJ: DO J=LASTJ, NPNTS
	
	Write(*,*) 'J = ', J
	TIME = 0.D0
	CALL TTIME(TIME,'',0)
	OPEN(100,FILE = 'SO_EnergiesAll.txt', STATUS = 'OLD',
	+FORM = 'FORMATTED', POSITION = 'APPEND')

C************************************************************************************************************
C	������ ��������� ��������� BHA
C************************************************************************************************************

	WRITE(*,*)
      WRITE(*,*) '-----------------------------------------------------'
 	WRITE(*,*)
 	WRITE(*,*) 'Calculation of BHA matrix elements ... '
	WRITE(*,*)
	OPEN(1, FILE='BHA.txt', FORM = 'FORMATTED')
	DO I_=1, CountX
		DO I=1, CountX
			CALL Result1(I_,I)
			Ress = (0.d0, 0.d0)
			BHA(I_,I) = (0.0d0, 0.d0)
				DO L = 2, LMAX							
						Res = (0.d0, 0.d0)
						M1:DO MSMALL = -(L-1), (L-1)
							IF ((MSMALL==(L-1)).OR.((L-1)==0)) THEN
								CYCLE M1
							ENDIF
							CALL Result3_2(L, MSMALL, I_, I, J, ALPHA)
C							CALL Result3_1(L, MSMALL, I_, I, J, ALPHA)
							CALL Result2_2(L, MSMALL, I_, I, J, ALPHA)
							Res41 = Result4_2(I_, MSMALL, ALPHA)
C							Res41 = Result4_1(I_, MSMALL, ALPHA)
							Res42 =	Result4_1(I, MSMALL, ALPHA)
							Res5 = Result5_2(L, MSMALL)
							Res = Res + Res5*Res3*Res2*Res41*Res42 	
						END DO M1
						Ress = Ress + DBLE(2*(L-1)+1)*Res
				END DO
			BHA(I_,I) = Res1*Ress
			IF (WriteToFile) THEN
				WRITE(1,*) I_, I, BHA(I_,I)
			ENDIF
		END DO
	END DO
	WRITE(*,*)
	WRITE(*,*) 'Calculation of BHA matrix elements finished ... '
	CLOSE(1)
	
C************************************************************************************************************
C	������ ��������� ��������� AHB
C************************************************************************************************************

 	WRITE(*,*)
	WRITE(*,*) 'Calculation of AHB matrix elements ... '
	WRITE(*,*)
	OPEN(1, FILE='AHB.txt', FORM = 'FORMATTED')

	DO I_=1, CountX
		DO I=1, CountX

			CALL Result1(I_,I)
			Ress = (0.d0, 0.d0)
			AHB(I_,I) = (0.d0, 0.d0)
			
				DO L = 2, LMAX 
						
						Res = (0.d0, 0.d0)
						M2:DO MSMALL = -(L-1), (L-1)
							IF ((MSMALL==-(L-1)).OR.((L-1)==0)) THEN
								CYCLE M2
							ENDIF
							CALL Result3_3(L, MSMALL, I_, I, J, ALPHA)
C							CALL Result3_1(L, MSMALL, I_, I, J, ALPHA)
							CALL Result2_3(L, MSMALL, I_, I, J, ALPHA)
							Res41 = Result4_3(I_, MSMALL, ALPHA)
C							Res41 = Result4_1(I_, MSMALL, ALPHA)
							Res42 =	Result4_1(I, MSMALL, ALPHA)
							Res5 = Result5_3(L, MSMALL)
							Res = Res + Res5*Res3*Res2*Res41*Res42 	
						END DO M2
						Ress = Ress + DBLE(2*(L-1)+1)*Res
				END DO
			AHB(I_,I) = Res1*Ress

			IF (WriteToFile) THEN		
				WRITE(1,*) I_, I, AHB(I_,I)
			ENDIF
		END DO
	END DO

	WRITE(*,*)
	WRITE(*,*) 'Calculation of AHB matrix elements finished ... '
	CLOSE(1)

C************************************************************************************************************
C	������ ��������� ��������� BHA2 ����� ���������� AHB ��� ��������
C************************************************************************************************************

 	WRITE(*,*)
 	WRITE(*,*) 'Calculation of BHA2 matrix elements for checking... '
	WRITE(*,*)
	OPEN(1, File = 'BHA and BHA2.txt', Form = 'FORMATTED')

	DO I=1, CountX
		DO I_=1, CountX
			BHA2(I,I_) = DCONJG(AHB(I_,I))
			IF (WriteToFile) THEN
				WRITE(1,*) I_, I, BHA(I,I_), BHA2(I,I_)
			ENDIF
C		IF ((ABS(DIMAG(BHA2(I,I_)) - DIMAG(BHA(I,I_)))>1.D-12).OR.
C	+	(ABS(DBLE(BHA2(I,I_)) - DBLE(BHA(I,I_)))>1.D-12))	 THEN
C			WRITE(*,*) '!!!BHA2 <> BHA!!!'
C			WRITE(*,*) 'BHA, BHA2, I, I_ = ', BHA(I,I_),
C	+		 BHA2(I,I_), I, I_
C			READ(*,*)
C			ENDIF
		END DO
	END DO
	WRITE(*,*) 
      WRITE(*,*) 'Calculation of BHA2 matrix elements finished ... '
	CLOSE(1)

C************************************************************************************************************
C	������ ��������� ��������� AHA
C************************************************************************************************************

C	AHA - ��������� ������� Psi(ALPHA)HPsi(ALPHA) ��� �������� �������
C	BHA - Psi(beta)HPsi(ALPHA) � �.�.
C	HamAHA - ��������� ������� ������������� ����� ����� ������������� �������� �������

 	WRITE(*,*)
 	WRITE(*,*) 'Calculation of AHA matrix elements ... '
	WRITE(*,*)
	OPEN(1, FILE='AHA.txt', FORM = 'FORMATTED')
	
	DO I_=1, CountX
		DO I=1, CountX
			CALL Result1(I_,I)
			Ress = (0.d0, 0.d0)
			AHA(I_,I) = (0.d0, 0.d0)	  
				DO L = 2, LMAX
						
						IF (L==1) THEN
							CYCLE
						ENDIF 
						
						Res = (0.d0, 0.d0)
						M3:DO MSMALL = -(L-1), (L-1)
							CALL Result3_1(L, MSMALL, I_, I, J, ALPHA)
							CALL Result2_1(L, MSMALL, I_, I, J, ALPHA)
							Res41 = Result4_1(I_, MSMALL, ALPHA)
							Res42 =	Result4_1(I, MSMALL, ALPHA)
							Res5 = Result5_1(L, MSMALL)
							ResAdd = Res5*Res3*Res2*Res41*Res42
							Res = Res + ResAdd
						END DO  M3
						Ress = Ress + (2*(L-1)+1)*Res
	
				END DO	
			AHA(I_,I) = Res1*Ress
			IF (WriteToFile) THEN
				WRITE(1,*) I_, I, AHA(I_,I)
			ENDIF 
	    END DO
	END DO
	WRITE(*,*)
	WRITE(*,*) 'Calculation of AHA matrix elements finished ... '
	CLOSE(1)


C************************************************************************************************************
C	������ ��������� ��������� BHB
C************************************************************************************************************

 	WRITE(*,*)
 	WRITE(*,*) 'Calculation of BHB matrix elements ... '
	WRITE(*,*)
	OPEN(1, FILE='BHB.txt', FORM = 'FORMATTED')
	DO I=1, CountX
		DO I_=1, CountX
			BHB(I,I_) = AHA(I,I_)*(-1.D0)
			IF (WriteToFile) THEN
				WRITE(1,*) I, I_, BHB(I,I_)
			ENDIF
		END DO
	END DO

	WRITE(*,*) 
      WRITE(*,*) 'Calculation of BHB matrix elements finished ... '
	CLOSE(1)

C************************************************************************************************************
C	������ ��������� ��������� AHB2 ����� ���������� BHA ��� ��������
C************************************************************************************************************

C	WRITE(*,*)
C 	WRITE(*,*) 'Calculation of AHB2 matrix elements for checking... '
C	WRITE(*,*)
C
C	DO I=1, CountX
C		DO I_=1, CountX
C			AHB2(I,I_) = DCONJG(BHA(I,I_))
C		END DO
C	END DO
C	WRITE(*,*) 
C     WRITE(*,*) 'Calculation of AHB2 matrix elements finished ... '


C************************************************************************************************************
C	������ ��������� ��������� ������������� ����� ����.�������� �������
C************************************************************************************************************

 	WRITE(*,*)
 	WRITE(*,*) 'Calculation of hamiltonian matrix elements... '
	WRITE(*,*)

C	HamAHA
 	WRITE(*,*) 'Calculation of HamAHA hamiltonian matrix elements...'
	WRITE(*,*)
	OPEN(1, FILE='HamAHA.txt', FORM = 'FORMATTED')
	DO N2=1, CountX
		DO N1=1, CountX
			SUM = (0.D0, 0.D0)
			DO I_=1, CountX
			DO I=1, CountX
			
			SUM = SUM + DCONJG(BIGH(J,I_,N2))*BIGH(J,I,N1)*AHA(I_,I)
C			SUM = SUM + DCONJG(BIGH(J,N2,I_))*BIGH(J,N1,I)*AHA(I_,I)

			END DO
			END DO
			HamAHA(N2,N1) = SUM
			IF (WriteToFile) THEN
				WRITE(1,*) N2, N1, HamAHA(N2,N1)
			ENDIF					
		END DO
	END DO		
	WRITE(*,*)
	WRITE(*,*) 'Calculation of HamAHA finished...'
	CLOSE(1)

C	HamBHB
 	WRITE(*,*) 'Calculation of HamBHB hamiltonian matrix elements...'
	WRITE(*,*)
	OPEN(1, FILE='HamBHB.txt', FORM = 'FORMATTED')
	DO N2=1, CountX
		DO N1=1, CountX
			SUM = (0.D0, 0.D0)
			DO I_=1, CountX
			DO I=1, CountX
			
			SUM = SUM + DCONJG(BIGH(J,I_,N2))*BIGH(J,I,N1)*BHB(I_,I) 
C			SUM = SUM + DCONJG(BIGH(J,N2,I_))*BIGH(J,N1,I)*BHB(I_,I)

			END DO
			END DO
			HamBHB(N2,N1) = SUM
			IF (WriteToFile) THEN
				WRITE(1,*) N2, N1, HamBHB(N2,N1)
			ENDIF		
		END DO
	END DO		
	WRITE(*,*)
	WRITE(*,*) 'Calculation of HamBHB finished...'
	CLOSE(1)

	
C	HamAHB
 	WRITE(*,*) 'Calculation of HamAHB hamiltonian matrix elements...'
	WRITE(*,*)
	OPEN(1, FILE='HamAHB.txt', FORM = 'FORMATTED')
	DO N2=1, CountX
		DO N1=1, CountX
			SUM = (0.D0, 0.D0)
			DO I_=1, CountX
			DO I=1, CountX
			
			SUM = SUM + DCONJG(BIGH(J,I_,N2))*BIGH(J,I,N1)*AHB(I_,I) 
C			SUM = SUM + DCONJG(BIGH(J,N2,I_))*BIGH(J,N1,I)*AHB(I_,I)

			END DO
			END DO
			HamAHB(N2,N1) = SUM
C			HamAHB(N2,N1) = (0.d0, 0.d0)
			IF (WriteToFile) THEN
				WRITE(1,*) N2, N1, HamAHB(N2,N1)
			ENDIF		
		END DO
	END DO		
	WRITE(*,*)
	WRITE(*,*) 'Calculation of HamAHB finished...'
	CLOSE(1) 

C	HamBHA
 	WRITE(*,*) 'Calculation of HamBHA hamiltonian matrix elements...'
	WRITE(*,*)
	OPEN(1, FILE='HamBHA.txt', FORM = 'FORMATTED') 
	DO N2=1, CountX
		DO N1=1, CountX
			SUM = (0.D0, 0.D0)
			DO I_=1, CountX
			DO I=1, CountX
			
			SUM = SUM + DCONJG(BIGH(J,I_,N2))*BIGH(J,I,N1)*BHA(I_,I) 
C			SUM = SUM + DCONJG(BIGH(J,N2,I_))*BIGH(J,N1,I)*BHA(I_,I)

			END DO
			END DO
			HamBHA(N2,N1) = SUM
C			HamBHA(N2,N1) = (0.d0, 0.d0)
			IF (WriteToFile) THEN
				WRITE(1,*) N2, N1, HamBHA(N2,N1)
			ENDIF		
		END DO
	END DO		
	WRITE(*,*)
	WRITE(*,*) 'Calculation of HamBHA finished...'
	CLOSE(1)
	                
      WRITE(*,*) 'Calculation of hamiltonian matrix elements finished.'


C************************************************************************************************************
C	 ����������� ������� ������� H � ������� ������ - HamAHA, HamAHB, HamBHA, HamBHB
C************************************************************************************************************

	WRITE(*,*) 'Calculation of Big Hamiltonian Matrix...'
	OPEN(1, FILE='HamBIG.txt', FORM = 'FORMATTED')
	DO I=1, CountX*2
		DO I_=1, CountX*2
			IF ((I.LE.CountX).AND.(I_.LE.CountX)) THEN
				HamBIG(I,I_) = HamAHA(I,I_)
			ENDIF
			IF ((I.LE.CountX).AND.(I_>CountX)) THEN
			 	HamBIG(I,I_) = HamAHB(I,ABS(CountX-I_))
			ENDIF
			IF ((I>CountX).AND.(I_.LE.CountX)) THEN
				HamBIG(I,I_) = HamBHA(ABS(CountX-I),I_)
			ENDIF
			IF ((I>CountX).AND.(I_>CountX)) THEN
				HamBIG(I,I_) = HamBHB(ABS(CountX-I),ABS(CountX-I_))
			ENDIF
			IF (WriteToFile) THEN
				WRITE(1,*) I, I_, HamBIG(I,I_)
			ENDIF
C			IF (I_==I) THEN
C				WRITE(1,*) I, I_, DBLE(HamBIG(I,I_)), DIMAG(HamBIG(I,I_))
C			ENDIF
		ENDDO
	ENDDO
	CLOSE(1)
	WRITE(*,*) 'Calculation of Big Hamiltonian Matrix finished.'

C	�������� ����������� (��������������)
	WRITE(*,*) 'Checking if matrix HamBIG is Hermitian...'
	HermitCycle: DO I = 1, CountX*2
	DO I_ = 1, CountX*2
		Element1 = HamBIG(I, I_)
		Element2 = HamBIG(I_, I)
		IF ((ABS(DBLE(Element1)-DBLE(Element2))>1.0D-12).OR.
	+	(ABS(DIMAG(Element1)+DIMAG(Element2))>1.0D-12)) THEN
			WRITE(*,*) '!!!HamBIG is NOT Hermitian!!!'
			Exit HermitCycle
		ENDIF
	ENDDO
	ENDDO HermitCycle                          
	
C	WRITE(*,*) 'HamBIG is probably Hermitian.'
C	�������
C	I = 1
C	DO WHILE (I.NE.0)
C	WRITE(*,*) 'Type I, I_ to check if HamBIG is Hermitian'
C	READ(*,*) I
C	READ(*,*) I_
C	Element1 = HamBIG(I, I_)
C	Element2 = HamBIG(I_, I)
C	WRITE(*,*) 'Element1 = ', Element1
C	WRITE(*,*) 'Element2 = ', Element2
C	WRITE(*,*) '========================='
C	ENDDO


C************************************************************************************************************
C	 ���������� �������� ������������� ����� ���������� ������ ������� ������ ����
C************************************************************************************************************

	DO I=1, CountX*2
		IF (I.LE.CountX) THEN
			HamBIG(I,I) = HamBIG(I,I) + BIGD(J,I)
		ENDIF
	    IF (I>CountX) THEN
		 	HamBIG(I,I) = HamBIG(I,I) + BIGD(J,ABS(CountX-I))
		ENDIF
      ENDDO 

C	WRITE(*,*) 'Preparation of Big Hamiltonian Matrix finished.'

C************************************************************************************************************
C	 �������������� �������� �������������
C************************************************************************************************************

	WRITE(*,*) 'Diagonalisation of Big Hamiltonian Matrix...' 

c --- Here we call NAG/LAPACK routine for eigenvalues and eigenvectors ---
      CALL F02HAF(JOB,UPLO,COUNTX*2,HamBIG,COUNTX*2,DE,
	+RWORK,WORK,LWORK,INFO)
c     -- after this routine, D contain eigenvalues,         --
c     -- H - corresponding eigenvectors in orthogonal basis set --
*	  

	EBig(J,:) = DE
		
	WRITE(*,*) 'Diagonalisation of Big Hamiltonian Matrix finished.'
	WRITE(*,*) 'Energies saved to EBig Array.'

C************************************************************************************************************
C	���������� ������� � ����
C************************************************************************************************************
	   
    	WRITE(*,*) 'Energies saved to file SO_EnergiesAll.txt...'
	 
	DO I = 1, 2*CountX

	EBig(J,I) = EBig(J,I)*evs
	K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))
	WRITE(100,*) K, EBig(J,I)
	 
	ENDDO
			
	CLOSE(100)
	WRITE(*,*) 'Saving finished.'
	WRITE(*,*)

	CALL TTIME(TIME,'Time on this point',0)

	ENDDO CycleJ
C	��������� ����� �� k


9999	WRITE(*,*) 'Program finished.'
      READ(*,*)

C************************************************************************************************************
C	������� � ���������
C************************************************************************************************************

	Contains
	
C************************************************************************************************************

	FUNCTION FCT(N)
C	������� ���������� ����������
	INTEGER FCT
	INTEGER I, N
	FCT = 1
	DO I=1, N
		FCT = FCT*I
	END DO
	END FUNCTION FCT

C************************************************************************************************************

c	������������ ���������
      
	SUBROUTINE polint(xa,ya,n,x,y,dy)
      INTEGER n,NMAX
      DOUBLE PRECISION dy,x,y,xa(n),ya(n)
      PARAMETER (NMAX=10)
      INTEGER i,m,ns
      DOUBLE PRECISION den,dif,dift,ho,hp,w,c(NMAX),d(NMAX)
      ns=1
      dif=ABS(x-xa(1))
      DO 11 i=1,n
        dift=ABS(x-xa(i))
        IF (dift.lt.dif) THEN
          ns=i
          dif=dift
        ENDIF
        c(i)=ya(i)
        d(i)=ya(i)
11    CONTINUE
      y=ya(ns)
      ns=ns-1
      DO 13 m=1,n-1
        DO 12 i=1,n-m
          ho=xa(i)-x
          hp=xa(i+m)-x
          w=c(i+1)-d(i)

          den=ho-hp
          IF(den.eq.0.) THEN
			WRITE(*,*) xa
			WRITE(*,*)
			WRITE(*,*) ya
			WRITE(*,*) n
			WRITE(*,*)
			WRITE(*,*) x
			WRITE(*,*)
		    WRITE(*,*) y
			WRITE(*,*) 
			WRITE(*,*) dy
			PAUSE 'failure in polint'
c     This error can occur only if two input xa's are (to within roundoff)
c     identical.
		ENDIF
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
12      CONTINUE
        IF (2*ns.lt.n-m) THEN
          dy=c(ns+1)
        ELSE
          dy=d(ns)
          ns=ns-1
        ENDIF
        y=y+dy
13    CONTINUE
      RETURN
      END SUBROUTINE polint

C************************************************************************************************************
	
	DOUBLE PRECISION FUNCTION Vi(r,IT)
	INTEGER X, X1, X2, t, n, j, IT
	DOUBLE PRECISION Va(5), Ra(5), r
C	������� ����������������� ������ V ��� ����� r
C    	����: V(X,IT)
C    	�� �����: r, IT

C
	n = 5

C	1. ������� ��������� ����� RAD(X,IT) � ����� r, �� ���� ������� X
C	���� ����� ��������� �� ������� RAD(1,IT), �� X=1
C	���� ����� ��������� ����� ���������� RAD(JRIS,IT), �� X=JRIS
	
	X = 0
	Loop1:DO t=1, JRIS-1
	X1 = t
	X2 = t+1
 	 
	IF ((RAD(X2,IT)-r)>0.AND.(r-RAD(X1,IT))>=0) Then
C	������ ����� RAD(X2) � RAD(X1)
	X = X2
	EXIT Loop1
	ENDIF

	IF ((RAD(X1,IT)-r)>0) Then
	X = 1
	EXIT Loop1
	ENDIF

	ENDDO Loop1

	IF (X==0) Then
C	������ �� ���� RAD, �� �� ����� ���� r � ��������� � ���� RAD(X,IT), ������, �������� r>Max(RAD)
	X=JRIS
	ENDIF

C	2. ���������� ���������������� ������� Va(n), Ra(n)
C	����� n ����� ������ � n ����� �����
C	���������� X2, ���� �� ������, ���� ����� X2, ������� ������� �� X �� 5 ��� ������ �����
C	��������� X1, ���� �� ������, ���� ����� X1, ������� ������� �� X �� 5 ��� ������ �����
 
	IF (X>(JRIS-2)) Then
C	��������� ��� ������������� �����
 
		t = X-n-1
		DO j=1, n
			Va(j) = V(t,IT)
			Ra(j) = RAD(t,IT)
			t = t+1
		ENDDO

      ELSE IF ((X==1).OR.(X==2)) Then
C	������ ��� ������ �����	

		t = X
		DO j=1, n	
			Va(j) = V(t,IT)
			Ra(j) = RAD(t,IT)
			t = t+1
		ENDDO

	ELSE IF (((X-2)>0).AND.(X<(NGRID-1))) Then

C	��� �����, ����� 2 ������ � 2 ���������
		Va(1) = V(X-2,IT)
		Ra(1) = RAD(X-2,IT)
		Va(2) = V(X-1,IT)
		Ra(2) = RAD(X-1,IT)
		Va(3) = V(X,IT)
		Ra(3) = RAD(X,IT)  
		Va(4) = V(X+1,IT)
		Ra(4) = RAD(X+1,IT)
		Va(5) = V(X+2,IT)
		Ra(5) = RAD(X+2,IT)
	ENDIF
	
C 	3. ������� ������� �������� V ��� ������ r � ������� ��������� �������� � ��������� polint
	 
	CALL Polint(Ra,Va,n,r,Vi,dy)

	END FUNCTION Vi

C************************************************************************************************************
	  
	DOUBLE PRECISION FUNCTION Vurdu(L,IT,i)
	INTEGER IT, L, i

C	������� ��������� �������� ���������������� ��������� Vurdu(L,IT,i)

	Vurdu = V(i,IT)*PSIALL(IT, L, i)*PSIRALL(IT, L, i)*RAD(i,IT)

	END FUNCTION Vurdu

C************************************************************************************************************

	SUBROUTINE VurduInt(L,IT)
	INTEGER IT, L, i

C	��������� ��������� �������� ��������� VuduIntValue(l, IT) ������� ��������������� �� ��������

	VurduIntValue = 0.d0

	DO i=1, JRIS-1
		VurduIntValue = VurduIntValue + (Vurdu(L,IT,i) + 
	+Vurdu(L,IT,i+1))/2.D0 * (RAD(i+1,IT)-RAD(i,IT))
      ENDDO

	END SUBROUTINE VurduInt	

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Vu2dr(L,IT,i)
	INTEGER IT, L, i

C	������� ��������� �������� ���������������� ��������� Vu2dr(L,IT,i)

	Vu2dr = V(i,IT)*(PSIALL(IT, L, i)**2)

	END FUNCTION Vu2dr

C************************************************************************************************************

	SUBROUTINE Vu2drInt(L,IT)
	INTEGER IT, L, i

C	��������� ��������� �������� ��������� Vu2drIntValue(l, IT) ������� ��������������� �� ��������

	Vu2drIntValue = 0.d0

	DO i=1, JRIS-1
		Vu2drIntValue = Vu2drIntValue + (Vu2dr(L,IT,i) + 
	+Vu2dr(L,IT,i+1))/2.D0 * (RAD(i+1,IT)-RAD(i,IT))
      ENDDO

	END SUBROUTINE Vu2drInt

C************************************************************************************************************


	SUBROUTINE ZetaInt1(L,IT)
	INTEGER IT, L
	DOUBLE PRECISION firstPart, secondPart

C	��������� ��������� �������� ��������� Zeta1(l, IT) ��� ������������� ����������� �� V

	ZetaInt1Value = 0.d0

	CALL VurduInt(L,IT)

	CALL Vu2drInt(L,IT)

 	firstPart = (PSIALL(IT, L, JRIS)**2)*RAD(JRIS,IT)* V(JRIS,IT)
	secondPart = (PSIALL(IT, L, 1)**2)*RAD(1,IT)* V(1,IT)

	ZetaInt1Value = firstPart - secondPart - 2*VurduIntValue -
	+ Vu2drIntValue

	END SUBROUTINE ZetaInt1

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Vdurddu(L,IT,i)
	INTEGER IT, L, i

C	������� ��������� �������� ���������������� ��������� Vdurddu(L,IT,i)

	Vdurddu = V(i,IT)*PSIEALL(IT, L, i)*PSIERALL(IT, L, i)*RAD(i,IT)

	END FUNCTION Vdurddu

C************************************************************************************************************

	SUBROUTINE VdurdduInt(L,IT)
	INTEGER IT, L, i

C	��������� ��������� �������� ��������� VdurdduIntValue(l, IT) ������� ��������������� �� ��������

	VdurdduIntValue = 0.d0

	DO i=1, JRIS-1
		VdurdduIntValue = VdurdduIntValue + (Vdurddu(L,IT,i) + 
	+Vdurddu(L,IT,i+1))/2.D0 * (RAD(i+1,IT)-RAD(i,IT))
      ENDDO

	END SUBROUTINE VdurdduInt	

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Vdu2dr(L,IT,i)
	INTEGER IT, L, i

C	������� ��������� �������� ���������������� ��������� Vdu2dr(L,IT,i)

	Vdu2dr = V(i,IT)*(PSIEALL(IT, L, i)**2)

	END FUNCTION Vdu2dr

C************************************************************************************************************

	SUBROUTINE Vdu2drInt(L,IT)
	INTEGER IT, L, i

C	��������� ��������� �������� ��������� Vdu2drIntValue(l, IT) ������� ��������������� �� ��������

	Vdu2drIntValue = 0.d0

	DO i=1, JRIS-1
		Vdu2drIntValue = Vdu2drIntValue + (Vdu2dr(L,IT,i) + 
	+Vdu2dr(L,IT,i+1))/2.D0 * (RAD(i+1,IT)-RAD(i,IT))
      ENDDO

	END SUBROUTINE Vdu2drInt

C************************************************************************************************************

	SUBROUTINE ZetaInt2(L,IT)
	INTEGER IT, L
	DOUBLE PRECISION firstPart, secondPart

C	��������� ��������� �������� ��������� Zeta1(l, IT) ��� ������������� ����������� �� V

	ZetaInt2Value = 0.d0

	CALL VdurdduInt(L,IT)

	CALL Vdu2drInt(L,IT)

	firstPart = (PSIEALL(IT, L, JRIS)**2)*RAD(JRIS,IT)* V(JRIS,IT)
	secondPart = (PSIEALL(IT, L, 1)**2)*RAD(1,IT)* V(1,IT)

	ZetaInt2Value = firstPart - secondPart - 2*VdurdduIntValue -
	+ Vdu2drIntValue

	END SUBROUTINE ZetaInt2

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION UnderInt3(L,IT,i)
	INTEGER IT, L, i

C	������� ��������� �������� ���������������� ��������� UnderInt3(L,IT,i)	��� ���� 3 ��� ������������� ����������� �� V
	   
	UnderInt3 = V(i,IT)*(PSIALL(IT, L, i)*RAD(i,IT)*PSIERALL(IT,L,i)+
	+PSIEALL(IT,L,i)*RAD(i,IT)*PSIRALL(IT,L,i)+
     +PSIALL(IT, L, i)*PSIEALL(IT,L,i))

	END FUNCTION UnderInt3

C************************************************************************************************************

	SUBROUTINE SubInt3(L,IT)
	INTEGER IT, L, i

C	��������� ��������� �������� ��������� SubInt3(l, IT) ������� ��������������� �� ��������,
C	 ������� ������ � �������� ����3

	SubInt3Value = 0.d0

	DO i=1, JRIS-1
		SubInt3Value = SubInt3Value + (UnderInt3(L,IT,i) + 
	+UnderInt3(L,IT,i+1))/2.D0 * (RAD(i+1,IT)-RAD(i,IT))
      ENDDO

	END SUBROUTINE SubInt3

C************************************************************************************************************


	SUBROUTINE ZetaInt3(L,IT)
	INTEGER IT, L
	DOUBLE PRECISION firstPart, secondPart

C	��������� ��������� �������� ��������� Zeta1(l, IT) ��� ������������� ����������� �� V

	ZetaInt3Value = 0.d0

	CALL SubInt3(L,IT)

	firstPart = PSIEALL(IT, L, JRIS)*PSIALL(IT, L, JRIS)*
	+RAD(JRIS,IT)* V(JRIS,IT)
	secondPart = PSIEALL(IT, L, 1)*PSIALL(IT, L, 1)*
	+RAD(1,IT)* V(1,IT)

	ZetaInt3Value = firstPart - secondPart - SubInt3Value

	END SUBROUTINE ZetaInt3	

C************************************************************************************************************

C	������� ���������� ���������������� ��������� � ������ ��������� � ��������� ��� ������� ��������� ����

	DOUBLE PRECISION FUNCTION Zeta11(r,L,IT)
	INTEGER IT,	L
C	������ i �������� interpolated ����� ����������. 
	DOUBLE PRECISION r
	Zeta11 = Vi(r,IT)*PSIALL(IT, L, r)*PSIRALL(IT, L, i)*RAD(i,IT)
	END FUNCTION Zeta11

C************************************************************************************************************

	SUBROUTINE Result1(I_,I)
C	
	DOUBLE PRECISION r1, r2, r3, r4
	INTEGER I, I_

      r1 = RMT**4
	r2 = 2*(LightSpeed**2)*C
	r3 = (-1.D0)**(M(I)+M(I_))
	r4 = r1/r2
	Res1 = r4 * r3

	END SUBROUTINE Result1

C************************************************************************************************************

	SUBROUTINE ALBL_1(L, MSMALL, J, I, ALNAL, BLNAL)
 	INTEGER I, J, L, MSMALL
	DOUBLE COMPLEX ALNAL, BLNAL
	DOUBLE PRECISION K, KP
	DOUBLE COMPLEX I1, I2
C	������� ������� ������� A(L,N,ALPHA), B(L,N,ALPHA)
	 K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))
	 KP=K+(2*PI/C)*DBLE(P(I))
	 I1 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL, L-1, 1)
	 I2 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL, L-1, 2)
       ALNAL = I2*DCMPLX(PSIE(L,IT))-I1*DCMPLX(PSIER(L,IT))
	 BLNAL = I1*DCMPLX(PSIR(L,IT))-I2*DCMPLX(PSI(L,IT))
	END SUBROUTINE ALBL_1

C************************************************************************************************************

	SUBROUTINE ALBL_2(L, MSMALL, J, I, ALNAL, BLNAL)
 	INTEGER I, J, L, MSMALL
	DOUBLE COMPLEX ALNAL, BLNAL
	DOUBLE PRECISION K, KP
	DOUBLE COMPLEX I1, I2
	 K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))
	 KP=K+(2*PI/C)*DBLE(P(I))
	 I1 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL+1, L-1, 1)
	 I2 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL+1, L-1, 2)
       ALNAL = I2*DCMPLX(PSIE(L,IT))-I1*DCMPLX(PSIER(L,IT))
	 BLNAL = I1*DCMPLX(PSIR(L,IT))-I2*DCMPLX(PSI(L,IT))
	END SUBROUTINE ALBL_2

C************************************************************************************************************

	SUBROUTINE ALBL_3(L, MSMALL, J, I, ALNAL, BLNAL)
 	INTEGER I, J, L, MSMALL
	DOUBLE COMPLEX ALNAL, BLNAL
	DOUBLE PRECISION K, KP
	DOUBLE COMPLEX I1, I2
	 K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))
	 KP=K+(2*PI/C)*DBLE(P(I))
	 I1 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL-1, L-1, 1)
	 I2 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL-1, L-1, 2)
       ALNAL = I2*DCMPLX(PSIE(L,IT))-I1*DCMPLX(PSIER(L,IT))
	 BLNAL = I1*DCMPLX(PSIR(L,IT))-I2*DCMPLX(PSI(L,IT))
	END SUBROUTINE ALBL_3


C************************************************************************************************************

	SUBROUTINE Result2_1(L, MSMALL, I_ ,I, J, ALPHA)
C     ��������� ��������� �������� � ����������� ����� ������� � CJ, CY
C	ALPHA - ����� �����
	INTEGER L, MSMALL, I, I_, J, ALPHA
	DOUBLE PRECISION r6, r7, K, KP1, KP2
	DOUBLE COMPLEX r8
	r6 = DBLE(FCT((L-1) - ABS(MSMALL)))/DBLE(FCT((L-1) + ABS(MSMALL)))
	r8 = (0.d0, 0.d0)
      K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))
	KP1=K+(2*PI/C)*DBLE(P(I))
	KP2=K+(2*PI/C)*DBLE(P(I_))
 	 
	DO ALPHA = 1, NATOMS
 		r7 = (KP1-KP2)*RSZC(ALPHA) + (M(I)-M(I_))*PHI(ALPHA)
		r8 = r8 + DCMPLX(Dcos(r7),Dsin(r7),8)			
	ENDDO

	Res2 = r6*r8

	END SUBROUTINE Result2_1

C************************************************************************************************************

	SUBROUTINE Result2_2(L, MSMALL, I_, I, J, ALPHA)
C     ��������� ��������� �������� � ����������� ����� ������� � CJ, CY
C	ALPHA - ����� �����
	INTEGER L, MSMALL, I, I_, J, ALPHA
	DOUBLE PRECISION r14, r15, r6, r7, K, KP1, KP2
	DOUBLE COMPLEX r8

	r6 = DBLE(FCT((L-1) - ABS(MSMALL)))/DBLE(FCT((L-1) + ABS(MSMALL)))
	r8 = (0.d0, 0.d0)

	r14 = DBLE(FCT((L-1) - ABS(MSMALL+1)))/DBLE(FCT((L-1) +
	+ ABS(MSMALL+1)))

	r15 = (r6*r14)**(0.5d0)
	
      K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))						
	KP1=K+(2*PI/C)*DBLE(P(I))
	KP2=K+(2*PI/C)*DBLE(P(I_))

	DO ALPHA = 1, NATOMS
 		r7 = (KP1-KP2)*RSZC(ALPHA) + (M(I)-M(I_))*PHI(ALPHA)
		r8 = r8 + DCMPLX(Dcos(r7),Dsin(r7),8)			
	ENDDO
	
	Res2 = r15*r8

C	Res2 = (1.d0, 0.d0)

	END SUBROUTINE Result2_2

C************************************************************************************************************

	SUBROUTINE Result2_3(L, MSMALL, I_, I, J, ALPHA)
C     ��������� ��������� �������� � ����������� ����� ������� � CJ, CY
C	ALPHA - ����� �����
	INTEGER L, MSMALL, I, I_, J, ALPHA
	DOUBLE PRECISION r14, r15, r6, r7, K, KP1, KP2
	DOUBLE COMPLEX r8

	r6 = DBLE(FCT((L-1) - ABS(MSMALL)))/DBLE(FCT((L-1) + ABS(MSMALL)))
	r8 = (0.d0, 0.d0)

	r14 = DBLE(FCT((L-1) - ABS(MSMALL-1)))/DBLE(FCT((L-1) +
	+ ABS(MSMALL-1)))

	r15 = (r6*r14)**(0.5d0)
	
      K= kStart + (kStop - kStart) * (DBLE(J-1)/DBLE(NINTERVALS))						
	KP1=K+(2*PI/C)*DBLE(P(I))
	KP2=K+(2*PI/C)*DBLE(P(I_))

	DO ALPHA = 1, NATOMS
 		r7 = (KP1-KP2)*RSZC(ALPHA) + (M(I)-M(I_))*PHI(ALPHA)
		r8 = r8 + DCMPLX(Dcos(r7),Dsin(r7),8)			
	ENDDO
	
	Res2 = r15*r8

	END SUBROUTINE Result2_3

C************************************************************************************************************

	SUBROUTINE Result3_1(L, MSMALL, I_, I, J, ALPHA)
C    	���������� ������� ������ � �������
	INTEGER L, MSMALL, I, I_, J, ALPHA, MC
 	DOUBLE COMPLEX r1, r2, r3
C	���������� ��� ����� �� ������ �����
C	� ��� ���� 1 ��� ������
	IT = 1

	MC = MSMALL + L 

	r1 = DCMPLX(Dzeta1(L, IT), 0.d0)*DCONJG(ASM(L, MC, I_, J))*
	+ASM(L, MC, I, J)
	r2 = DCMPLX(Dzeta2(L, IT), 0.d0)*DCONJG(BSM(L, MC, I_, J))*
	+BSM(L, MC, I, J)
	r3 = DCMPLX(Dzeta3(L, IT), 0.d0)*(DCONJG(ASM(L, MC, I_, J))*
	+BSM(L, MC, I, J)+DCONJG(BSM(L, MC, I_, J))*ASM(L, MC, I, J))
	Res3 = r1 + r2 + r3

	END SUBROUTINE Result3_1

C************************************************************************************************************
	 	  
	SUBROUTINE Result3_2(L, MSMALL, I_, I, J, ALPHA)
C 	 ���������� ������� ������ � �������
	INTEGER L, MSMALL, I, I_, J, ALPHA, MC
	DOUBLE COMPLEX r1, r2, r3
C	���������� ��� ����� �� ������ �����
C	� ��� ���� 1 ��� ������
	IT = 1

	MC = MSMALL + L

	r1 = DCMPLX(Dzeta1(L, IT), 0.d0)*DCONJG(ASM_2(L, MC, I_, J))*
	+ASM(L, MC, I, J)
	r2 = DCMPLX(Dzeta2(L, IT), 0.d0)*DCONJG(BSM_2(L, MC, I_, J))*
	+BSM(L, MC, I, J)
	r3 = DCMPLX(Dzeta3(L, IT), 0.d0)*(DCONJG(ASM_2(L, MC, I_, J))*
	+BSM(L, MC, I, J)+DCONJG(BSM_2(L, MC, I_, J))*ASM(L, MC, I, J))
	Res3 = r1 + r2 + r3

	END SUBROUTINE Result3_2

C************************************************************************************************************

	SUBROUTINE Result3_3(L, MSMALL, I_, I, J, ALPHA)
C	���������� ������� ������ � �������
	INTEGER L, MSMALL, I, I_, J, ALPHA, MC
	DOUBLE COMPLEX r1, r2, r3
C	���������� ��� ����� �� ������ �����
C	� ��� ���� 1 ��� ������
	IT = 1

	MC = MSMALL + L 

	r1 = DCMPLX(Dzeta1(L, IT), 0.d0)*DCONJG(ASM_3(L, MC, I_, J))*
	+ASM(L, MC, I, J)
	r2 = DCMPLX(Dzeta2(L, IT), 0.d0)*DCONJG(BSM_3(L, MC, I_, J))*
	+BSM(L, MC, I, J)
	r3 = DCMPLX(Dzeta3(L, IT), 0.d0)*(DCONJG(ASM_3(L, MC, I_, J))*
	+BSM(L, MC, I, J)+DCONJG(BSM_3(L, MC, I_, J))*ASM(L, MC, I, J))
	Res3 = r1 + r2 + r3
C	Res3 = (1.d0,0.d0)

	END SUBROUTINE Result3_3

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Result4_1(I, MSMALL, ALPHA)

C	���������� CJ*J+CY*Y
	INTEGER I,MSMALL, NBES, ALPHA
	DOUBLE PRECISION BESJ, BESJ1, BESY, BESY1, XBES
	EXTERNAL BESS, BESSYPROC
	ALPHA = 1
	XBES = KAPPA(I)*RO(ALPHA)
	NBES = MSMALL-M(I)
	CALL BESS(NBES, XBES, BESJ, BESJ1)
	CALL BESSYPROC(NBES, XBES, EPSBES, BESY, BESY1)
	Result4_1 = CJ(I)*BESJ+CY(I)*BESY
	
	END FUNCTION Result4_1

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Result4_2(I, MSMALL, ALPHA)

C	���������� CJ*J+CY*Y
	INTEGER I,MSMALL, NBES, ALPHA
	DOUBLE PRECISION BESJ, BESJ1, BESY, BESY1, XBES
	EXTERNAL BESS, BESSYPROC
	ALPHA = 1
	XBES = KAPPA(I)*RO(ALPHA)
	NBES = MSMALL-M(I)+1
	CALL BESS(NBES, XBES, BESJ, BESJ1)
	CALL BESSYPROC(NBES, XBES, EPSBES,BESY, BESY1)
	Result4_2 = CJ(I)*BESJ+CY(I)*BESY
	
	END FUNCTION Result4_2

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Result4_3(I, MSMALL, ALPHA)
 	 	
C	���������� CJ*J+CY*Y
	INTEGER I,MSMALL, NBES, ALPHA
	DOUBLE PRECISION BESJ, BESJ1, BESY, BESY1, XBES
	EXTERNAL BESS, BESSYPROC
	ALPHA = 1
	XBES = KAPPA(I)*RO(ALPHA)
	NBES = MSMALL-M(I)-1
	CALL BESS(NBES, XBES, BESJ, BESJ1)
	CALL BESSYPROC(NBES, XBES, EPSBES,BESY, BESY1)
	Result4_3 = CJ(I)*BESJ+CY(I)*BESY
	
	END FUNCTION Result4_3

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Result5_1(L,MSMALL)
    
    	INTEGER L, MSMALL, LSMALL
	DOUBLE PRECISION r1, r2
	LSMALL = L-1
	r1 = DBLE(MSMALL)
	r2 = DBLE(MSMALL+ABS(MSMALL) + 2*LSMALL)
	Result5_1 = r1 * ((-1.D0)**r2)

	END FUNCTION Result5_1

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Result5_2(L,MSMALL)
    
    	INTEGER L, MSMALL, LSMALL, r1
	DOUBLE PRECISION r2, r3
	LSMALL = L-1
      r1 = LSMALL*(LSMALL+1)-MSMALL*(MSMALL+1)
	r2 = DBLE(r1)**(0.5d0)
	r3 = DBLE(MSMALL+ABS(MSMALL))/2.d0 + 
	+DBLE(MSMALL+1+ABS(MSMALL+1))/2.d0 + 2.D0*LSMALL

	Result5_2 = r2 * ((-1.D0)**r3)

	END FUNCTION Result5_2

C************************************************************************************************************

	DOUBLE PRECISION FUNCTION Result5_3(L,MSMALL)

	INTEGER L, MSMALL, LSMALL, r1
	DOUBLE PRECISION r2, r3
	LSMALL = L-1
      r1 = LSMALL*(LSMALL+1)-MSMALL*(MSMALL-1)
	r2 = DBLE(r1)**(0.5d0)
	r3 = DBLE(MSMALL+ABS(MSMALL))/2.d0 + 
	+DBLE(MSMALL-1+ABS(MSMALL-1))/2.d0 + 2.D0*LSMALL
	Result5_3 = r2 * ((-1.D0)**r3)

	END FUNCTION Result5_3

C************************************************************************************************************

	END PROGRAM SpinOrbital
