C	������� ��� ������ ����� INDEFECT1.DAT
C	���������� �����. 1 - ���� ��� ���������, 0 - ���� �������� ������
	INTEGER FUNCTION IREADINDEFECT1(FILENAME)
	CHARACTER*(*) FILENAME
	CHARACTER BUF*256, STR*256
	INTEGER IFS,M, LMAX, EPNTS, NEARFERMI, MATRIXSIZE
	COMMON /INDEFECT1/ EMIN, EMAX, EPNTS, EPSD, TDELTA, LMAX,
	+ NEARFERMI, DELTAFERMI, MATRIXSIZE
	DOUBLE PRECISION EMIN, EMAX, EPSD, TDELTA, DELTAFERMI
	IFS = IINIFILEOPENSECTION(FILENAME, 'DATA FOR CALCULATION', 1, 0)
C	IFS - ����� ������ ��������� �����, ������ ��������� 1, 0 - ������������ ����� � ������ ������, 'ENERGIES' - 
C	�������� ������� � INI �����
	DO WHILE (IINIFILEREADENTRYSTRING(IFS,BUF).GT.0)
C	��������� ��������� ������ � ������ ������� INI ����� � ������� �� � ������ BUF, IFS - ����� ������ ������� �����
	M = INDEX(BUF, '=')
C	WRITE(*,*) 'BUF = ', BUF
C	����� ��������� ����� ����� � ������ BUF
	IF (M.LE.1) CONTINUE
C	���� ��� ������ 1, �� ����������
	STR = BUF(1:M-1)
C	WRITE(*,*) 'STR=',STR
C	������� � ������ STR ����� ������ �� ����� �����
	BUF = BUF(M+1:)
C	������� � ������ BUF ������ ��, ��� �� ������ �����
	CALL BTRIMEX(BUF,256)
C	WRITE(*,*) 'NEW BUF AFTER BTRIMEX = ',BUF
C	������� ���������, ������� ������� � ������ BUF ������������ ����� 256 ��� �������
	IF (STR.EQ.'EMIN') READ(BUF,*) EMIN
	IF (STR.EQ.'EMAX') READ(BUF,*) EMAX
	IF (STR.EQ.'EPNTS') READ(BUF,*) EPNTS
	IF (STR.EQ.'EPSD') READ(BUF, *) EPSD
	IF (STR.EQ.'TDELTA') READ(BUF, *) TDELTA
	IF (STR.EQ.'LMAX') READ(BUF,*) LMAX
	IF (STR.EQ.'NEARFERMI') READ(BUF,*) NEARFERMI
	IF (STR.EQ.'DELTAFERMI') READ(BUF,*) DELTAFERMI
	IF (STR.EQ.'MATRIXSIZE') READ(BUF,*) MATRIXSIZE
	END DO
	IREADINDEFECT1 = 1
	RETURN
	END FUNCTION IREADINDEFECT1