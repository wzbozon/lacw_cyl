*-MODULE------------------------------------------------------------------*       bess_sph.for    Spherical Bessel Functions      (i) 2001 by Mith
*
*-THEORY------------------------------------------------------------------
* The equation  z*z*w'' + 2zw' + [z*z - n(n+1)]w = 0,   n=0,1,-1,2,-2,...
* has two sets of linear-independent solutions, j_n(z) and y_n(z)
* (or h1_n = j_n+i*y_n and h2_n = j_n-i*y_n).
*
* j_n(z) and y_n(z) are called spherical Bessel functions.
*
* [See more of their properties in the code of BessSphEx().]
*
*-CONTENTS----------------------------------------------------------------
* BessSphEx(x,bj,by,N) calculates values of spherical (half-integer order)
* Bessel functions j_l(x) and y_l(x) for orders l=0..N-1.
*
* BessSph(x,N,ret_j,ret_y) is a shortcut to BessSphEx().
* A large number of j's and y's are formed by the BessSphEx() call,
* but BessSph returns only those requested---j_N(x) and y_N(x).
* WARNING! Unoptimised calls to BessSph() may eat up a hell of time.
*
*-------------------------------------------------------------------------
        recursive subroutine BessSph(x,N,ret_j,ret_y)
        implicit double precision(a-h,o-z)
        parameter (NMAX=2048)
        double precision x,bj(NMAX),by(NMAX)
        parameter (PMIN=1.0d-12,PZERO=1.0d-35)

        integer NM

        if (N<0) then
          call BessSph(x,-N-1,ret_j,ret_y)
          ret_j=ret_j*(-1)**(N+1)
          ret_y=ret_y*(-1)**(N+1)
          return
        else
          NM=max0(N,idnint(x),10)*2
          if (NM.gt.NMAX) stop 'BessSph: N>NMAX'
          call BessSphEx(x,bj,by,NM)

          ret_j=bj(N+1)
          ret_y=by(N+1)
!       call AddValue1(x,ret_j,N)
          return
        endif
        end
*-------------------------------------------------------------------------
        subroutine BessSphEx(x,bj,by,N)
        implicit double precision(a-h,o-z)

        double precision x,bj(*),by(*)
        integer j,y
        integer N
        parameter (PMIN=1.0d-6,PZERO=1.0d-35)


*       j_l(x->0) = (x**l)/(2l+1)!!
*       y_l(x->0) = (x**(-l-1))/(2l+1)!! * ((-1)**(l+1))

        if (x<PZERO) then
          bj(1)=1.0d0
          bj(2:N+1)=PZERO
          by(1:N+1)=-1.0d35
          !x=PZERO
          return
        endif
        if (dabs(x).lt.PMIN) then
          j=1
          y=1
          do i=0,N
            j=j*(2*i+1)
            if (i.ne.1) y=y*(2*i-1)
            bj(i+1)=x**i/j
            by(i+1)=1.0d0/x**(i+1)*y
          enddo
          return
        endif

* Use Miller's technique: assume j(n)=0, j(n-1)=1 and unwind the array
* to its origin using the recurrent formula; for "f" being "j" or "y",
*       f(i-1) = (2i+1)/x * f(i) - f(i+1), for ALL i=0,1,-1,2,-2,....
* Having done that, apply a re-normalisation to all the j's.
* It may be done so that j_0(x)=sin(x)/x,
* or remembering that  SUM{ (2k+1)*(j_k**2), k=0..+INF } == 1

* NOTE: There's one thing important when you work with recurrent formulae:
* If you don't believe there are really coeffs like "(-2i+3)"
* ---derive all the formulae manually, again and again. In the end,
* it will be more convincing and less erroneous than tracing the code.

        fp=0.0d0        ! "f(i+1)"
        fo=1.0d0        ! "f(i)"
        bj(N-1+1)=fo    ! = j(N-1)
        do i=N-1,1,-1   ! bj(i) := j(i-1)
          fm=(dble(2*i+1)/x)*fo-fp      ! "f(i-1)"
          fp=fo
          fo=fm
          bj(i)=fo      ! store (de-normalised) j(i-1)
        enddo
        s=(dsin(x)/x)/bj(1)  ! normalisation factor
        sv=s    ! will need to restore
* The recurrent formula shown is also true for negative i;
* the unwinding procedure walks into negative i's and
* gives j(-1), j(-2), j(-3), ..., which are exactly -y(0),y(1),-y(2), ....
        do i=1,N        ! by(i) := j(-i)
          fm=dble(-2*i+3)/x*fo-fp
          fp=fo
          fo=fm
          s=-s          ! apply the (-1) factor: y(l) = j(-l-1) * (-1)**(l+1)
          by(i)=fo*s    ! normalise it right now
        enddo
        s=sv
        do i=1,N
          bj(i)=bj(i)*s
        enddo

        return
        end

        subroutine BessSphDer(x,N,ret_j,ret_y)
          real*8,intent(in) :: x
          integer,intent(in) :: N
          real*8,intent(out) :: ret_j,ret_y
          real*8 ret_j1,ret_y1,ret_j_1,ret_y_1

          call BessSph(x,N+1,ret_j1,ret_y1)
          call BessSph(x,N-1,ret_j_1,ret_y_1)

          ret_j=(N*ret_j_1-(N+1)*ret_j1)/(2*N+1)
          ret_y=(N*ret_y_1-(N+1)*ret_y1)/(2*N+1)

        end subroutine
        





