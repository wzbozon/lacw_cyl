C************************************************************************************************************

C	 ��������� ������� ���������� � �������� ������� ������� ����� / ������ ������� ����� ��������� ������

C************************************************************************************************************	 

C	 ������� �. �., ��������� �.�.

C	 "�������� �������� ������ �������� ����� ���������� ������, �� ���� ��������� �� �����" 
C	  �� ������������� �������� �� ���������

C      "� ����� ������ ������������� ���������� �������� ����������, ������� ������ ������� �� �����������" 
C       ����� ������, "����������� ������������� �++"

C	 "������ ������ �������� ����������������. ������ �� ����� ����� � ��������� 1:1"
C	  �� ���� �� ����������������

C	 "������� ������ ���������� �� ����������������, � ����� ������ ���� ���������."
C	  �. �. �������

C	 "������ ����� ��������������"
C	  �. �. �������
C
C	 "THE UNIVERSE IS RUN BY THE COMPLEX INTERWEAVING OF THREE	*
C	  ELEMENTS: ENERGY, MATTER, AND ENLIGHTENED SELF-INTEREST.'	*
C	  --G'KAR, `BABYLON 5' EPISODE 1.11: `SURVIVORS'

C	 "����� ��� ������, ����� ���� ���������"
C	  ���, �����-������, ��� ���� �� 1 ������

C	 "��� ������� �������� ����� ����������� ������� � ������� ������������� ������������ � ����� ������������ ���
C	 ������� � �������� ������ �������, ��� ��� ���� ������� � ���� DOOM"
C	 "����������� ������� �������� ����� ����, ��� ������ ������������. ������� ��� ����� �� � �� �����, �����
C	 ��������� ������ �����������"	
C	 "������ ����� �� ������ ����������� ������ �������, ���� ��� ����� ��������� ����� ���!"
C	 "����� ��������� ��������� ��������, ��� ������������ ����"
C	 "����� ��������� ��������� ����� ������� ���������� � ���������"
C	 "����� ��������� ������������ �������� ������ �������� ��������� � �������"
C	 "��� ��� ����� � ���������� ���� ����� ��������� ������������ ��� ��� �����������"
C	 "���������� ��������� ����� �������� � ��������� �������"
C	 "����� ������������ �������� ������ ���������������� �������� ���������� � ���������"
C		���������������� �� �++ ������� ������


C************************************************************************************************************
	
C      1. �� ���� ������ ������� �������� �������� ����� ���� "ADDED FOR GFM" 
C	 2. ��� ����������� �������� ������ "�" �����
C	 3. ���������� ����� �������� �������
C	 4. I_ �������� I-�����, � ��������� ������� ����� ��������� 1, �������� L1 - ��� L-�����
C	 5. ��� ���������� ������ ���� ���� ���������
C	 6.	������� ������� ��������� � ����� GFUNCTIONS, ���������� ����� ����� CONTAINS; ��� �� ���� �����
C	 7. LSMALL = L-1, L �� ����� ���� ����� ����!!! � MC �� ����� ���� ����� ����
C	 8. ���� ����������� ����� *.FOR ����, �� ���������� �������� ��� � � ������ *.MAK ����� �� ��������
C	 9. ����� ��������� ����
C	(�� ����� ������ ����� �������� ���������� � ����� � FORTRAN)
C	10.  ������� �������� � �������� �������: 
C		L = LSMALL + 1
C		MC = MSMALL + LSMALL + 1


C************************************************************************************************************
 
      PROGRAM GFIdeal

C************************************************************************************************************
C	���������� ����������
C************************************************************************************************************

	USE MSFLIB
	IMPLICIT NONE
100	FORMAT(F23.20)
1	FORMAT(A1\)
17	FORMAT(F5.3,1X,F9.3)
C	���������� ��� ������������� MSFLIB ���������� ��� �������� ������
	INTEGER(4)  LEN, COUNT, TOTALNUMPOINTS,TOTALNUMPOINTS1,
	+ COUNTER, COUNTER1,	INTCOUNTER,	NCLCPNTS,
	+ CURCOUNTER,
	+ MINS, MINS1, HOURS
	CHARACTER(80)  FILE
	CHARACTER(1) CH,CHH	
	CHARACTER(1) CHAR
	LOGICAL RESULT

	DOUBLE PRECISION RAD(300,1), A, B, C, RMT, EPSI,
	+ CALCCYFORGFM, CALCCJFORGFM, TIME, DEN, EFERMI,
	+KP, K,  E, Z, Z1, INTSTEP, EPSD, DELTANUM, EMIN, EMAX,
     +ESTEP, KZ, DELTAFERMI,
	+CB,CB1, KP_SMALL, KP_SMALL1, KPZ, MFI,  
     +A5, TDELTA, EDELTA, DE, INTDOS, TIMERATIO

	COMMON /INDEFECT1/ EMIN, EMAX, EPNTS, EPSD, TDELTA, LMAX, 
	+NEARFERMI, DELTAFERMI, MATRIXSIZE

	DOUBLE COMPLEX SS,  
	+I1, I2, SIMP_I12, 
	+SUM1, SUM2, BIG, RES14, SSS, BIGRES,
	+PSUM1, PSUM2, F, FA, FB, 
	+PSS, PSSS, PF, PFA, PFB

C	A - ������� ������ ��������, B - ���������� ������ ��������,
C	C - ����� ������������ ������
C	EPSI - ����������� ���������� ����������
C	������� ������������ �������, ������� ������ �����
C	����� ���������� ����� �������� ������� � ������ ����������

	DOUBLE COMPLEX, PARAMETER :: IM=(0.D0, 1.D0)
	DOUBLE COMPLEX, PARAMETER :: RE=(1.D0, 0.D0)

	DOUBLE PRECISION, ALLOCATABLE :: KAPPA(:), CJ(:), CY(:), PSI(:,:),
	+PSIR(:,:), PSIE(:,:), PSIER(:,:), NLALPHA(:,:), D(:), RO(:),
     + PHI(:),
     +RSZC(:), EL(:,:), BIGD(:,:), BIGD0(:),   
	+PSIRALL(:,:,:), PSIALL(:,:,:), PSIEALL(:,:,:), PSIERALL(:,:,:), 
     + DSTATES(:,:,:),
     + DTOTAL(:), DL(:),
	+PDTOTAL(:), PDSTATES(:,:,:)  
	
	DOUBLE COMPLEX, ALLOCATABLE:: H(:,:), BIGH(:,:,:), 
	+ASMALL(:,:), BSMALL(:,:), ASMALL1(:,:), BSMALL1(:,:),
	+OECBAR(:,:), SP(:,:), BIGSP(:,:,:), GLML1M1(:,:,:,:,:),
	+DEFGLML1M1(:,:,:,:,:), RES5(:),
	+IMGLML1M1(:,:,:,:,:), REGLML1M1(:,:,:,:,:), G1(:,:,:,:,:),
	+G2(:,:,:,:,:)
      
	INTEGER, ALLOCATABLE :: M(:), N(:), P(:)
C	M, N, P - ��������� �������� �������, ��������� �����
C	NPNTS - ����� ����� K, ��� ����� ���� � INLACW � ���� ��������� POINTSTOTAL

	INTEGER NT, IT, COUNTX, I, I_, J, NPNTS, NINTERVALS, NLMAX,
	+NLMAXNEW, IREADINDEFECT1, RES, XFERMI,
	+MSMALL, L, AL, NA, L1, NA1, AL1, JJ, II, X, X1, 
     +MSMALL1, LAMBDA, T, EPNTS,LMAX, NEARFERMI, 
	+LENH, LEND, LENS, ALPHA, NATOMS, MNUM,
	+CCOUNTER, CNUM, NPNTS1, MC, MC1, CYCLESNUM, A1, A2, A3,
	+A4, JRIS, RES30, WH, MATRIXSIZE, NTM1, NTM, ATM, ATM1

C	JRIS - ������ ������� ����� �� R, ��������������� �������� �� ���� 5 ��������� ����� ������
C	IT - ����� ����� ������, ���������� ����� ����� ������ 5
	DOUBLE PRECISION, PARAMETER :: 
	+PI=3.14159265358979323846264338328D0
	DOUBLE PRECISION, PARAMETER:: EPSBES = 1.0D-12
C	�������� ���������� ������� �������
	DOUBLE PRECISION, PARAMETER:: EVS=13.6058D0
C	����������� �������� �� �� � ��������
C	���������������� ��� POINT, ������� ������������ �� ���� ����� ����� ��� ������ ����� � �������
	TYPE POINT
		INTEGER(4) PNUM, LS, MS, LS1, MS1, EX
		INTEGER CALCLTD
	END TYPE POINT
C	PNUM - ����� �����, LS - L �����, MS - M �����, EX - ����� ����� �� E
C	CALCULATED - ����������, ���������� �� ������ ����� ���. ���� �� - �� TRUE.

C	������� ������, ���������� ��������� ���������� ����� ��������� ���� POINT
	TYPE (POINT), ALLOCATABLE :: PT(:)

	WRITE(*,*) '********************************************'
	WRITE(*,*) 'GREEN FUNCTION AND LACW METHOD PROGRAM'
	WRITE(*,*) '********************************************'
	WRITE(*,*)

	CALL TIMESTAMP()
C	TIMESTAMP ������ ����� � ���� ������� ���� � �����
	WRITE(*,*)
	WRITE(*,*) 'SHOULD I RUN ATOMD.EXE, STRCYD.EXE AND OVER55D.EXE'
	WRITE(*,*) '(1=YES, ANYKEY=NO)?'
	CH = GETCHARQQ()
	WRITE(*,*) CH
	WRITE(*,*) 'DO YOU WANT TO RUN SECOND TIME OVER55D'
	WRITE(*,*) '(1=YES, ANYKEY = NO)?'
	CHH = GETCHARQQ()
	WRITE(*,*) CH

	IF ((CH .EQ. '1')) THEN
	WRITE(*,*) 'TRYING TO EXECUTE ATOMD.EXE'
	RESULT = RUNQQ('ATOMD.EXE', '-C -R')
	IF (.NOT.RESULT) THEN
	WRITE(*,*)
	WRITE(*,*) 'ATOMD.EXE SUCCESSFULLY FINISHED'
	ELSE
	WRITE(*,*) 'ATOMD.EXE FAILED TO COMPLETE'
	END IF

	WRITE(*,*) 'TRYING TO EXECUTE STRCYD.EXE'
	WRITE(*,*)
	RESULT = RUNQQ('STRCYD.EXE', '-C -R')
	IF (.NOT.RESULT) THEN 
	WRITE(*,*)
	WRITE(*,*) 'STRCYD.EXE SUCCESSFULLY FINISHED'
	ELSE
	WRITE(*,*) 'STRCYD.EXE FAILED TO COMPLETE'
	END IF

C	WRITE(*,*) 'TRYING TO RUN OVER55D.EXE'

	RESULT = RUNQQ('OVER55D.EXE', '-C -R')
	IF (.NOT.RESULT) THEN
	WRITE(*,*)
	WRITE(*,*) 'OVER55.EXE SUCCESSFULLY FINISHED'
	ELSE 
	WRITE(*,*) 'OVER55D.EXE FAILED TO COMPLETE'
	END IF
	END IF
C	��������� ����������� ������� - ��������� ������ GREEN ��� ���? 
	WRITE(*,*)

	IF ((CHH.EQ.'1')) THEN

	 RESULT = RENAMEFILEQQ('PSI.TXT', 'PSIG.TXT')
	 RESULT = RENAMEFILEQQ('PSIE.TXT', 'PSIEG.TXT')
	 RESULT = RENAMEFILEQQ('PSIER.TXT', 'PSIERG.TXT')
	 RESULT = RENAMEFILEQQ('PSIR.TXT', 'PSIRG.TXT')
	 RESULT = RENAMEFILEQQ('CYLINDRICALCOORDINATES.TXT', 
	+'CYLINDRICALCOORDINATESG.TXT')
	 RESULT = RENAMEFILEQQ('D_ENERGIES.TXT', 'D_ENERGIESG.TXT')
	 RESULT = RENAMEFILEQQ('EPSI_NATOMS.TXT', 'EPSI_NATOMSG.TXT')
	 RESULT = RENAMEFILEQQ('H_AMNP.TXT', 'H_AMNPG.TXT')
	 RESULT = RENAMEFILEQQ('NPNTS_LMAX_LENH_LEND.TXT',
	+ 'NPNTS_LMAX_LENH_LENDG.TXT')
	 RESULT = RUNQQ('OVER55D.EXE', '-C -R')
	 IF (.NOT.RESULT) THEN
	  WRITE(*,*)
	  WRITE(*,*) 'OVER55D.EXE SUCCESSFULLY FINISHED THE SECOND TIME'
	 ELSE
	  WRITE(*,*)
	  WRITE(*,*) 'OVER55D.EXE FAILED TO COMPLETE THE SECOND TIME'
	 END IF
C	��������� ������� - ��������� OVER55D ������ ���? 
	END IF

C****************************************************************************
C	���������� ����� �������� ������� COUNTX � ������ ���������� �� ������
C****************************************************************************

	IT = 1
 	NT = 1

C	���������� ������ �� EF.DAT - ������� �����
	OPEN(1, FILE = 'EF.DAT', FORM = 'FORMATTED')
	READ(1,17) A5, EFERMI
	CLOSE(1)
	WRITE(*,*) 'EFERMI IS READ AS = ', EFERMI
	EFERMI = EFERMI/EVS
	WRITE(*,*) 'EFERMI IN RIDBERGS = ', EFERMI

	WRITE(*,*) 'JRIS, NT = ', JRIS, NT

	OPEN(65, FILE = 'BIGH.TXT', FORM = 'FORMATTED')
	OPEN(66, FILE = 'BIGD.TXT', FORM = 'FORMATTED')
	CLOSE(32)

	OPEN(1, FILE = 'indefect9.dat', FORM = 'FORMATTED')
	READ(1,*) COUNTX
	ALLOCATE (KAPPA(COUNTX), M(COUNTX), N(COUNTX), P(COUNTX),
	+ CJ(COUNTX), CY(COUNTX), H(COUNTX,COUNTX), D(COUNTX),
     + OECBAR(COUNTX,COUNTX), SP(COUNTX,COUNTX))
	READ(1,*) KAPPA
	READ(1,*) A, B, C
	DO I = 1, COUNTX
	READ(1,*) M(I), N(I), P(I)
	END DO
	CLOSE(1)

	WRITE(*,*) 'M, N, P, KAPPA ARE READ AS FOLLOWING ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		WRITE(*, *) M(I), N(I), P(I), KAPPA(I)
	END DO

	WRITE(*,*)
	WRITE(*,*) 'CYLINDER PARAMETERS A, B AND C ARE READ AS FOLLOWING
	+ ...'
	WRITE(*,*)
	WRITE(*,*) A, B, C

	WRITE(*,*)
	OPEN(11, FILE = 'CJ.TXT', FORM = 'FORMATTED')
	OPEN(12, FILE = 'CY.TXT', FORM = 'FORMATTED')							 
	WRITE(*,*) 'CJ(I) ARE CALCULATED ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		CJ(I) = CALCCJFORGFM(M(I), A, B, KAPPA(I))
C		WRITE(*,*) CJ(I)
		WRITE(11,*) CJ(I)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'CY(I) ARE CALCULATED ... '
	WRITE(*,*)
	DO I = 1, COUNTX
		CY(I) = CALCCYFORGFM(M(I), CJ(I), B, KAPPA(I))
C		WRITE(*,*) CY(I)
		WRITE(12,*) CY(I)
	END DO
	CLOSE(11)
	CLOSE(12)

	OPEN(13, FILE = 'EPSI_NATOMSG.TXT', FORM = 'FORMATTED')
	READ(13,*) EPSI, NATOMS
	CLOSE(13)
	WRITE(*,*) 'EPSI AND NATOMS ARE READ AS'
	WRITE(*,*) EPSI, NATOMS

C	����� ���������� ����� ������ � ������������ ������ NATOMS ����� ���������� ������� �������������� ���������

	ALLOCATE(RO(NATOMS), PHI(NATOMS), RSZC(NATOMS))
	OPEN(1, FILE = 'indefect2.dat', FORM = 'FORMATTED')
	READ(1,*) JRIS, RMT
	READ(1,*) RAD
	CLOSE(1)

	OPEN(15, FILE = 'NPNTS_LMAX_LENH_LENDG.TXT', FORM = 'FORMATTED')
	READ(15, *) NPNTS, NLMAX, LENS, LENH, LEND
	WRITE(*,*) 'NPNTS,NLMAX, LEN ARE READ'
	NPNTS1 = (NPNTS-1)/2+1
	WRITE(*,*) 'NPNTS1 IS EQUAL TO',NPNTS1

C	NLMAX - ��� ������������ �������� L ���������(��������� �����)

	CLOSE(15)
	WRITE(*,*) 
	WRITE(*,*) 'EPSI, NPNTS AND RMT, LENH, LEND'

C	EPSI - ����������� ���������� ����������, ������� �� �������� ����� INLACW � ���������
C	OVER55D.EXE, � �� ����� ��� �� OVER55D.EXE, STRCYD.EXE � ATOMD.EXE ��� ������������� ��������
C	����� INLACW, INATOM 
C	RMT - ������ ������-��� �����

	WRITE(*,*)
	WRITE(*,*) EPSI, NPNTS
	WRITE(*,*) RMT, LENH, LEND
	WRITE(*,*)
	ALLOCATE(BIGH(NPNTS,COUNTX,COUNTX), BIGD(NPNTS,COUNTX),
	+ BIGD0(COUNTX),
	+ASMALL(COUNTX, NPNTS),  BSMALL(COUNTX, NPNTS),
     +ASMALL1(COUNTX, NPNTS),  BSMALL1(COUNTX, NPNTS),
     + BIGSP(NPNTS,COUNTX,COUNTX))

C	����� ���������� � ��� �� �
	
	NINTERVALS = NPNTS - 1
	INTSTEP = (PI/C)/NINTERVALS

C	������ A, B, ������� ������� �� U, I1, I2, U' � �.�. - ALNAL � BLNAL   
C	A = I2*(U*) - I1*U(*')
C	B = I1*(U') - I2*(U)
C	NLMAX �������, ������ ����� ������� ALLOCATE ������� PSI � ������
C	PSI - ��� ������ ���������� �������� ������� U
C	PSIE - ��� ������ �� ����������� �� �������
C	PSIR - ����������� �� �������
C	PSIER - ����������� �� ������� � �������

	ALLOCATE(PSI(NLMAX,NT), PSIR(NLMAX,NT), PSIE(NLMAX,NT),
	+ PSIER(NLMAX,NT),
	+ EL(NLMAX,NT), NLALPHA(NLMAX,NT))			 
      OPEN(16, FILE = 'PSI.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(16,*) PSI(L, IT), A1, A2, A3
			WRITE(*,*) L, IT, PSI(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(16)

	OPEN(17, FILE = 'PSIR.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(17,*) PSIR(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(17)
	 
	OPEN(18, FILE = 'PSIE.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(18,*) PSIE(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(18)
	 
	OPEN(19, FILE = 'PSIER.TXT', FORM = 'FORMATTED')  
	DO IT = 1, NT
		DO L = 1, NLMAX
			READ(19,*) PSIER(L, IT), A1, A2, A3
		END DO
	END DO
	CLOSE(19)

	OPEN(27, FILE = 'indefect6.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		DO L = 1, NLMAX 
 			READ(27,*) NLALPHA(L, IT), A1, A2
		END DO
	END DO
	CLOSE(27)

	DO IT = 1, NT
	WRITE(*,*)
	WRITE(*,*) 'PSI(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSI(I,IT)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'PSIR(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSIR(I,IT)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'PSIE(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSIE(I,IT)
	END DO
	WRITE(*,*)
	WRITE(*,*) 'PSIER(I) ARE READ AS FOLLOWING...'
	DO I=1, NLMAX
	WRITE(*,*) PSIER(I,IT)
	END DO 
	END DO

C	�������� ������ ATM, NTM, ATM1, NTM1
	NTM = 1
	ATM = 1
	NTM1 = 1
	ATM1 = 2
		
	WRITE(*,*) 'ATM, RHOTM(ATM) ARE EQUAL TO', ATM, RHOTM(ATM)
	WRITE(*,*) 'ATM1, RHOTM(ATM1) ARE EQUAL TO', ATM1, RHOTM(ATM1)

	WRITE(*,*) 'ATM, PHITM(ATM) ARE EQUAL TO', ATM, PHITM(ATM)
	WRITE(*,*) 'ATM1, PHITM(ATM1) ARE EQUAL TO', ATM1, PHITM(ATM1)

	WRITE(*,*) 'ATM, ZTM(ATM) ARE EQUAL TO', ATM, ZTM(ATM)
	WRITE(*,*) 'ATM1, ZTM(ATM1) ARE EQUAL TO', ATM1, ZTM(ATM1)

 	  
C	OPEN(20, FILE = 'I1.TXT', FORM = 'FORMATTED')
C	OPEN(21, FILE = 'I2.TXT', FORM = 'FORMATTED')
C	OPEN(26, FILE = 'ALNAL.TXT', FORM = 'FORMATTED')
C	OPEN(27, FILE = 'BLNAL.TXT', FORM = 'FORMATTED')

	NLMAXNEW = NLMAX - 1

C	���������� �������������� ���������, RO - RHO, PHI, RSZC - Z; ALPHA - ����� ����� � ������

 	OPEN(24, FILE = 'CYLINDRICALCOORDINATESG.TXT', FORM = 'FORMATTED')
	DO ALPHA=1,NATOMS
		READ(24, '(A1,3F20.10)') CHAR, RO(ALPHA),PHI(ALPHA),RSZC(ALPHA)
	END DO

C	�������� ����������

	WRITE(*,*) 'CYLINDRICAL COORDINATES ARE READ AS FOLLOWING: '
	OPEN(26, FILE = 'CYL_COORD.TXT', FORM = 'FORMATTED')
      DO ALPHA=1,NATOMS
		WRITE(*, '(3F20.10)') RO(ALPHA),PHI(ALPHA),RSZC(ALPHA)
		WRITE(26, '(3F20.10)') RO(ALPHA),PHI(ALPHA),RSZC(ALPHA)
	END DO
	CLOSE(24)
	CLOSE(26)

C	���������� � ���������� EL(NLMAX,NT)

	OPEN(1, FILE = 'indefect7.dat', FORM = 'FORMATTED')
 	DO IT = 1, NT
	 DO I = 1, 5
		READ(1,*) EL(I,IT), A1, A2
	END DO
	DO I = 6, NLMAX
		EL(I, IT) = EL(5, IT)
	END DO
	END DO
	CLOSE(1)

	WRITE(*,*)
	WRITE(*,*) 'READING INDEFECT1.DAT...'
	RES = IREADINDEFECT1('INDEFECT1.DAT')
	WRITE(*,*) 'RES = ', RES

C	OPEN(30, FILE='indefect1.dat', FORM = 'FORMATTED')
C	READ(30,*) EMIN, EMAX, EPNTS, EPSD, TDELTA, LMAX
C 	EPNTS - ����� ����� �� �������
C	EPSD - ������� � ������ ������� ������
C	CLOSE(30)

	WRITE(*,*) 'EMIN IS READ AS ', EMIN
	WRITE(*,*) 'EMAX IS READ AS ', EMAX
	WRITE(*,*) 'EPNTS IS READ AS ', EPNTS
	WRITE(*,*) 'EPSD IS READ AS ', EPSD
	WRITE(*,*) 'TDELTA IS READ AS ', TDELTA
	WRITE(*,*) 'LMAX IS READ AS ', LMAX
	WRITE(*,*) 'CALCULATE NEAR FERMI ENERGY?(1=YES,0=NO)', NEARFERMI
	WRITE(*,*) 'DELTAFERMI IS READ AS ', DELTAFERMI
	WRITE(*,*) 'RECALCULATION TO RYDBERGS...'
	EMIN = EMIN/EVS
	EMAX = EMAX/EVS
	WRITE(*,*) 'EMIN = ', EMIN
	WRITE(*,*) 'EMAX = ', EMAX
	IF (NEARFERMI==1) THEN
		EMIN = EFERMI - DELTAFERMI/EVS
		EMAX = EFERMI + DELTAFERMI/EVS
		WRITE(*,*) 'NEW EMIN,EMAX = ', EMIN, EMAX
	END IF

C	�������� EL

	WRITE(*,*)
	WRITE(*,*) 'EL(I) ARE READ AS FOLLOWING:'
	WRITE(*,*) EL
	WRITE(*,*)
	WRITE(*,*) 'READING OF H AND D ARRAYS STARTED... '	
	
C	���������� ������� ������������� �������� ������� � �������� ��������


 	OPEN(22,FILE='H_AMNPG.TXT', ACCESS = 'DIRECT',
	+RECL = LENH+1000)
	DO J = 1, NPNTS
C	 WRITE(65,*) 'J =', J
	 READ(22, REC=J)((H(II,JJ),JJ=1,COUNTX),II=1,COUNTX)
	 DO JJ = 1, COUNTX
	 DO II = 1, COUNTX
	  
	  BIGH(J,II,JJ) = H(II,JJ)
C	  WRITE(65, *) BIGH(J,II,JJ)
	  WRITE(65, *) J, JJ, II, DBLE(BIGH(J,II,JJ)),
	+   DIMAG(BIGH(J,II,JJ))
C	  BIGH(J,II,JJ) - J=K, II= I, JJ=LAMBDA
	  END DO
	 END DO
C	 WRITE(65, *) '--------------------------------------------------'
	WRITE(*,1) '.'
	END	DO
	CLOSE(65)
	CLOSE(22)
	
	WRITE(*,*)
	WRITE(*,*) 'READING H ARRAY FINISHED'

C	���������� ������� �������� ������� �������� �������
	OPEN(23,FILE='D_ENERGIESG.TXT',
	+ACCESS = 'DIRECT', RECL = LEND+1000)
	DO J=1, NPNTS
	 READ(23,REC=J) D 
	 WRITE(66,*) 'J =', J
	 DO II = 1, COUNTX
	  BIGD(J,II) = D(II)
	  WRITE(66,*) BIGD(J,II)
	 END DO
	 WRITE(66,*) '-----------------------------------------------'
C	WRITE(*,1) '.'
	END DO
	WRITE(66,*) 'THE WHOLE BIGD: '
	WRITE(66,*) BIGD
	CLOSE(66)
	CLOSE(23)
	WRITE(*,*)
	WRITE(*,*) 'READING OF H AND D ARRAYS FINISHED.'
	WRITE(*,*) 
	 
	ALPHA = 1

C	ALPHA1 = 1
C	EPSD = 0.0001D0
	 
C	����������  ��������� �� K
C	S - ��������������� ����� � ��� �������� �����
C	FA - �������� ��������������� ����� ��� J = 1
C     FB - �������� ��������������� ����� ��� J = NPNTS(����� ����� �� �)
C     ������������ ����� ��������, ��� ����� 1, ��� ��� J=1,2,3,...,NPNTS
C	 ���������� ������ �� �����

	EDELTA = DABS(DSQRT(-4*EPSD*LOG(2*TDELTA*DSQRT(PI*EPSD))))

	WRITE(*,*) 'EDELTA IS CALCULATED AS', EDELTA
	WRITE(*,*)
	WRITE(*,*) 'NUMBER OF INTERVALS BY K : ', NINTERVALS														
	ESTEP = (EMAX-EMIN)/DBLE((EPNTS-1))
	OPEN(1, FILE='EDeltaEStep.txt', FORM='FORMATTED')
	WRITE(1,*) 'EDELTA = ', EDELTA
	WRITE(1,*) 'ESTEP = ', ESTEP
	CLOSE(1)

C***************************************
C     ����������� ����� � ������� �����
C***************************************

	DO X = 1, EPNTS
		E = EMIN+(DBLE(X-1))*ESTEP
		DEN = E - EFERMI
		IF (DEN > 0) THEN
			XFERMI = X
			EXIT
		END IF
	END DO
	WRITE(*,*) 'XFERMI IS CALCULATED AS ', XFERMI

C*************************************************************************************
C	������������ ������� E^(IK(Z-Z1))=RES5 ������� ������������ � ��������� RESULT3
C*************************************************************************************

	WRITE(*,*) 'CALCULATION OF RES5 ARRAY...'
	WRITE(*,*) 'NINTERVALS,C = ', NINTERVALS,C
	ALLOCATE(RES5(NPNTS))
	AL = 1
	AL1 = 1
	DO J = 1, NPNTS
		K=(DBLE((J-1))*PI)/(DBLE(NINTERVALS)*C)
		Z = RSZC(AL)
		Z1 = RSZC(AL1)
		KZ = K*(Z-Z1)
		RES5(J) = DCMPLX(DCOS(KZ),DSIN(KZ),8)
	END DO
	WRITE(*,*) 'FINISHED...'
	WRITE(*,*)

C*************************************************************************************

	OPEN(2, FILE='U(r).TXT', FORM='FORMATTED')
	ALLOCATE(PSIALL(NT, NLMAX, JRIS))
 	OPEN(1, FILE = 'PSIALL.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIALL(IT, L, X), A1, A2, A3
	PSIALL(IT,L,X) = PSIALL(IT, L, X)/RAD(X,1)
	WRITE(2,*) RAD(X,1), PSIALL(IT,L,X)
	END DO
	END DO
	END DO
	CLOSE(1)
	CLOSE(2)

	ALLOCATE(PSIRALL(NT, NLMAX, JRIS))
 	OPEN(1, FILE = 'PSIRALL.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIRALL(IT, L, X), A1, A2, A3
	PSIRALL(IT, L, X)=PSIRALL(IT, L, X)/RAD(X,1)
	END DO
	END DO
	END DO
	CLOSE(1)


	ALLOCATE(PSIEALL(NT, NLMAX, JRIS))
 	OPEN(1, FILE = 'PSIEALL.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIEALL(IT, L, X), A1, A2, A3
	PSIEALL(IT, L, X)=PSIEALL(IT, L, X)/RAD(X,1)
	END DO
	END DO
	END DO
	CLOSE(1)

	ALLOCATE(PSIERALL(NT, NLMAX, JRIS))
 	OPEN(1, FILE = 'PSIERALL.TXT', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIERALL(IT, L, X), A1, A2, A3
	PSIERALL(IT, L, X)=PSIERALL(IT, L, X)/RAD(X,1)
	END DO
	END DO
	END DO
	CLOSE(1)
	
	I =1
  	I_=1
      X = 1
	X1 = 1
	NA = 1
	AL = 1
	NA1 = 1
	AL1 = 1
	ALPHA = 1 
	  

	OPEN(14, FILE = 'IMGALL.TXT', FORM = 'FORMATTED')
	WRITE(*,*) 'RO(ALPHA) IS EQUAL TO', RO(ALPHA)

	MNUM = (LMAX-1)*2 + 1
	WRITE(*,*) 'MNUM IS EQUAL TO = ', MNUM
	CNUM = ((1+MNUM)/2)*((MNUM-1)/2+1)
	ALLOCATE(GLML1M1(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+DEFGLML1M1(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+G1(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+G2(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+IMGLML1M1(LMAX,MNUM,LMAX, MNUM, EPNTS),
	+REGLML1M1(LMAX, MNUM, LMAX, MNUM, EPNTS))	
	CCOUNTER = 0
	ALLOCATE(DSTATES(LMAX,MNUM,EPNTS), DTOTAL(EPNTS), DL(EPNTS),
	+PDTOTAL(EPNTS), PDSTATES(LMAX,MNUM,EPNTS))
	MC = 0
	MC1 = 0
		MC = 0
			MC = MC + 1
				MC1 = 0
					CCOUNTER = CCOUNTER + 1
					MC1 = MC1 + 1
     	I =1
  	I_=1
      X = 1
	X1 = 1
	J = 1
	NA = 1
	AL = 1
	NA1 = 1
	AL1 = 1
	ALPHA = 1

  101	FORMAT(F10.2,I4,I4)

	IT = 1

C**********************************************************************************************************
C	������ ������ ������� �����
C**********************************************************************************************************

	WRITE(*,*) 'STARTING CALCULATION OF ALL GLML1M1...'
	OPEN(1,FILE = 'indefect8.dat', FORM = 'FORMATTED')
     	MNUM = (LMAX-1)*2 + 1
	CNUM = ((1+MNUM)/2)*((MNUM-1)/2+1)
	CYCLESNUM = CNUM*CNUM

C	CYCLESNUM - ����� ��������� ���������� ����� L, M, L1, M1
C	�������� ������� PT() ����������� ���������� � ���������� ������ ����� �������

	TOTALNUMPOINTS = CYCLESNUM * EPNTS
	NCLCPNTS = (CNUM/2)*(CNUM+1) * EPNTS
	WRITE(*,*) 'CYCLESNUM, EPNTS, TOTALNUMPOINTS, NCLCPNTS:'
	WRITE(*,*) CYCLESNUM, EPNTS, TOTALNUMPOINTS, NCLCPNTS
	TIMERATIO = DBLE(TOTALNUMPOINTS)/DBLE(NCLCPNTS)
	WRITE(*,*) 'TIME OPTIMIZING RATIO = ', TIMERATIO
	WRITE(*,*) 
	ALLOCATE(PT(TOTALNUMPOINTS))
	COUNTER = 0
	DO L = 1, LMAX
		DO MSMALL = -(L-1), (L-1)
			DO L1 = 1, LMAX
				DO MSMALL1 = -(L1-1), (L1-1)
					DO X = 1, EPNTS
						COUNTER = COUNTER + 1
						PT(COUNTER).PNUM = COUNTER
						PT(COUNTER).LS = L-1
						PT(COUNTER).MS = MSMALL
						PT(COUNTER).LS1 = L1-1
						PT(COUNTER).MS1 = MSMALL1
						PT(COUNTER).EX = X
						PT(COUNTER).CALCLTD = 0  
					END DO
				END DO
			END DO
		END DO
	END DO
	WRITE(*,*) 'PT ARRAY IS FORMED'

	TOTALNUMPOINTS1 = TOTALNUMPOINTS
	INTCOUNTER = 0

C	���������� ��������� ����������� ������� �����

	WRITE(*,*) 'READING EXISTING G ELEMENTS' 
	EXT: DO COUNTER = 1, TOTALNUMPOINTS1
	IF(.NOT.EOF(1)) THEN
		L = PT(COUNTER).LS + 1
		MSMALL = PT(COUNTER).MS
		L1 = PT(COUNTER).LS1 + 1
		MSMALL1 = PT(COUNTER).MS1
		X = PT(COUNTER).EX
		MC = MSMALL + L
		MC1 = MSMALL1+ L1
		READ(1,*)
		READ(1,*) GLML1M1(L,MC,L1,MC1,X), A1, A2, A3, A4, A5
		PT(COUNTER).CALCLTD = 1

	ELSE 
		EXIT EXT
	END IF

	END DO EXT

	CLOSE(1)
	CURCOUNTER = COUNTER
	WRITE(*,*)
	WRITE(*,*) 'CURRENT COUNTER = ', CURCOUNTER
	WRITE(*,*) 'NUMBER OF CYCLES = ', CYCLESNUM
	WRITE(*,*)

C	������� �������� � �������� �������: 
C 	L = LSMALL + 1
C  	MC = MSMALL + LSMALL + 1 = MSMALL + L

	WRITE(*,*) 'CURCOUNTER = ', CURCOUNTER
	IF (CURCOUNTER.NE.1) THEN

	WRITE(*,*) 'CALCULATION OF REPEATING ELEMENTS STARTED'

	BIGRES = BIG(RMT,RMT, L-1,L1-1,MSMALL,MSMALL1)
	DO I = 1, COUNTX
		DO J = 1, NPNTS
			CALL ALBL(L, MSMALL, J, I, ASMALL(I, J), BSMALL(I, J))
			CALL ALBL(L1, MSMALL1, J, I, ASMALL1(I, J), BSMALL1(I, J))
		END DO
	END DO

C	������ ���������, ������� ����������� ��� ��������� � : -1**(�+�1)*EXP*CB*CB1, ��� �������� ��������� � 
C	������ OECBAR(I,I_)

	DO I = 1, COUNTX 
		DO I_ = 1, COUNTX
			CALL RESULT4(I, CB,MSMALL, RO(ALPHA))
			CALL RESULT4(I_, CB1, MSMALL1, RO(ALPHA))
			KP_SMALL=2*PI/C * P(I)
			KP_SMALL1 = 2*PI/C * P(I_)
			Z = RSZC(AL)
			Z1 = RSZC(AL1)
			KPZ = KP_SMALL * Z - KP_SMALL1 * Z1
			MFI = M(I)*PHI(AL) - M(I_)*PHI(AL1)
			RES14 = DCMPLX(DCOS(KPZ+MFI),DSIN(KPZ+MFI),8)    
			OECBAR(I,I_) = DCMPLX(CB * CB1 *
	+		 (-1)**DBLE((M(I) + M(I_))))*RES14
		END DO		
	END DO

	WRITE(*,*) 'CALCULATION OF REPEATING ELEMENTS FINISHED'

	END IF

	MAINCL: DO COUNTER = CURCOUNTER, TOTALNUMPOINTS

		TIME = 0.D0
		CALL TTIME(TIME,'',0)
		OPEN(1,FILE = 'indefect8.dat', STATUS = 'OLD',
	+	 FORM = 'FORMATTED', POSITION = 'APPEND')
		L = PT(COUNTER).LS + 1
		MSMALL = PT(COUNTER).MS
		L1 = PT(COUNTER).LS1 + 1
		MSMALL1 = PT(COUNTER).MS1
		X = PT(COUNTER).EX
		MC = MSMALL + L
		MC1 = MSMALL1+ L1
		WRITE(*,*)
		WRITE(*,*) 'POINT# ', COUNTER, ' of ', TOTALNUMPOINTS				
		WRITE(*,*) 'l = ', L-1
		WRITE(*,*) 'm = ', MSMALL
		WRITE(*,*) 'l1 = ', L1-1
		WRITE(*,*) 'm1 = ', MSMALL1
		WRITE(*,*) 'X = ', X

C1000	IF (PT(COUNTER).CALCLTD==0) THEN

	I =1
  	I_=1
	X1 = 1
	J = 1
	NA = 1
	AL = 1
	NA1 = 1
	AL1 = 1
	ALPHA = 1


	IF (X == 1) THEN

	WRITE(*,*) 'CALCULATION OF REPEATING ELEMENTS STARTED'

	BIGRES = BIG(RMT, RMT, L-1, L1-1, MSMALL, MSMALL1)
	DO I = 1, COUNTX
		DO J = 1, NPNTS
			CALL ALBL(L, MSMALL, J, I, ASMALL(I, J), BSMALL(I, J))
			CALL ALBL(L1, MSMALL1, J, I, ASMALL1(I, J), BSMALL1(I, J))
		END DO
	END DO

C	������ ���������, ������� ����������� ��� ��������� � : -1**(�+�1)*EXP*CB*CB1, ��� �������� ��������� � 
C	������ OECBAR(I,I_)

	DO I = 1, COUNTX 
		DO I_ = 1, COUNTX
			CALL RESULT4(I, CB, MSMALL, RO(ALPHA))
			CALL RESULT4(I_, CB1, MSMALL1, RO(ALPHA))
			KP_SMALL=2*PI/C * P(I)
			KP_SMALL1 = 2*PI/C * P(I_)
			Z = RSZC(AL)
			Z1 = RSZC(AL1)
			KPZ = KP_SMALL * Z - KP_SMALL1 * Z1
			MFI = M(I)*PHI(AL) - M(I_)*PHI(AL1)
			RES14 = DCMPLX(DCOS(KPZ+MFI),DSIN(KPZ+MFI),8)    
			OECBAR(I,I_) = DCMPLX(CB * CB1 *
	+		 (-1)**DBLE((M(I) + M(I_))))*RES14
		END DO		
	END DO

	WRITE(*,*) 'CALCULATION OF REPEATING ELEMENTS FINISHED'

	END IF

C**********************************************************************************************************
C	������� ����� �������� �� ������� - ���� �� �������� E
C**********************************************************************************************************
		 
		E = EMIN+(DBLE(X-1))*ESTEP
		CALL RESULT5(DE)
		SUM1 = (0.D0, 0.D0)
		PSUM1 = (0.D0, 0.D0)
	    SS = (0.D0, 0.D0)
		PSS = (0.D0, 0.D0)
	    SSS = (0.D0, 0.D0)
	 	PSSS = (0.D0, 0.D0)
			 DO I = 1, COUNTX 
			 	SUM2 = (0.D0, 0.D0)
				PSUM2 = (0.D0, 0.D0)
				SS = (0.D0, 0.D0)	
				PSS = (0.D0, 0.D0)		
				 DO I_ = 1, COUNTX
				 	SS = (0.D0, 0.D0)
					PSS = (0.D0, 0.D0)	
					CALL RESULT3(1, L, L1, FA, E, I, I_)
					CALL RESULT3(NPNTS, L, L1, FB, E, I, I_)
					CALL RESULT7(1, L, L1, PFA, E, I, I_)
					CALL RESULT7(NPNTS, L, L1, PFB, E, I, I_)
					DO T = 1, (NPNTS-1)
						CALL RESULT3((1+T), L, L1, F, E, I, I_)
						SS = SS + F
						CALL RESULT7((1+T), L, L1, PF, E, I, I_)
					 	PSS = PSS + PF
					END DO
			SS = DCMPLX(2.D0 * INTSTEP,0.D0)*(SS + (FA + FB)/2)
			SSS = SS*OECBAR(I,I_)
			SUM2 = SUM2 + SSS
			PSS = DCMPLX(2.D0 * INTSTEP,0.D0)*(PSS + (PFA + PFB)/2)
					PSSS = PSS*OECBAR(I,I_)
					PSUM2 = PSUM2 + PSSS
				END DO
				SUM1 = SUM1 + SUM2
				PSUM1 = PSUM1 + PSUM2
			END DO

		GLML1M1(L,MC,L1,MC1,X) = (1/(ALPH(L, IT)*ALPH(L1, IT)))*
	+	(DCMPLX(0.D0,DE)+BIGRES*(-IM*PI*SUM1+PSUM1))

		PT(COUNTER).CALCLTD = 1

		WRITE(1,*)
	 	WRITE(1,*) GLML1M1(L,MC,L1,MC1,X), 
	+	L-1, MSMALL, L1-1, MSMALL1, X

			  
	WRITE(*,*) 'DONE'
	CLOSE(1)
	CALL TTIME(TIME,'Time on this point',0)
	MINS = IDNINT((TIME*(TOTALNUMPOINTS-COUNTER)/60.D0)/
	+TIMERATIO)
	MINS1 = IDNINT(TIME*(TOTALNUMPOINTS-COUNTER)/60.D0)
	WRITE(*,*) 'Remaining time = ', MINS1, ' mins'
	WRITE(*,*) 'Remaining time considering symmetry = ', MINS, ' mins'
	HOURS = IDNINT(MINS/60.D0)

	WRITE(*,*) 'Or ', HOURS, ' hours'

	END DO MAINCL

	WRITE(*,*)

	WRITE(*,*) 'ALL GLML1M1 CALCULATION FINISHED'
	CLOSE(2)
	CLOSE(3)
	CLOSE(10)

	WRITE(*,*) 'IT, TOTALNUMPOINTS = ', IT, TOTALNUMPOINTS

C**********************************************************************************************************
C	 ������ ������� PDSTATES(L, MC, X)
C**********************************************************************************************************
	 
	DO COUNTER = 1, TOTALNUMPOINTS
		L = PT(COUNTER).LS + 1
		MSMALL = PT(COUNTER).MS
	 	L1 = PT(COUNTER).LS1 + 1
		MSMALL1 = PT(COUNTER).MS1
		X = PT(COUNTER).EX
		MC = MSMALL + L
		MC1 = MSMALL1+ L1
		IF (L==L1) THEN
		IF (MSMALL==MSMALL1) THEN
			E = EMIN+(DBLE(X-1))*ESTEP
			PDSTATES(L, MC, X) = ALPH(L, IT)*(DSQRT(E)-
	+		DIMAG(GLML1M1(L,MC,L1,MC1,X)))/PI
		END IF
		END IF
	END DO

C**********************************************************************************************************
C	������ ��������� ��������� ��������� ����������
C**********************************************************************************************************

	WRITE(*,*) 'CALCULATION OF DOS OF IDEAL TUBE...'

	WRITE(*,*) 'EPNTS, LMAX, MNUM =', EPNTS, LMAX, MNUM

C	��� FULL DOS

	OPEN(51, FILE='POUT_GREEN1', FORM = 'FORMATTED')
	  DO X = 1, EPNTS
	PDTOTAL(X) = 0.D0
	DO L = 1, LMAX
	MNUM=(L-1)*2+1
	 DO MC = 1, MNUM
		PDTOTAL(X) = PDTOTAL(X) + PDSTATES(L, MC, X)
	 END DO
	END DO
	E = EMIN+(DBLE(X-1))*ESTEP
	WRITE(51,*) E*EVS, PDTOTAL(X)
	END DO
	CLOSE(51)

C	��� LSMALL = 0

	L = 1
	MNUM=(L-1)*2+1
      OPEN(50, FILE = 'POUT_GREEN1_S', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + PDSTATES(1, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	 WRITE(50,*) E*EVS, DL(X)
	END DO
	CLOSE(50)

C	��� LSMALL=1

	L = 2
	MNUM=(L-1)*2+1
	OPEN(50, FILE = 'POUT_GREEN1_P', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + PDSTATES(2, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(50,*) E*EVS, DL(X)
	END DO
	CLOSE(50)

	WRITE(*,*) 'LMAX = ', LMAX

C	��� LSMALL=2

	IF (LMAX==3) THEN
	L = 3
	MNUM=(L-1)*2+1
	OPEN(50, FILE = 'POUT_GREEN1_D', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + PDSTATES(3, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(50,*) E*EVS, DL(X)
	END DO
	CLOSE(50)
	END IF

C**********************************************************************************************************
C	���������� ��������� �� ��������� ���������
C**********************************************************************************************************

	OPEN(1, FILE = 'INT_OF_IDEAL_DOS.TXT', FORM = 'FORMATTED')
	WRITE(1,*) 'IDEAL INTEGRAL OF DOS = '
	INTDOS = 0.D0
	DO X = 1, XFERMI
	 INTDOS = INTDOS + PDTOTAL(X) * ESTEP
	END DO
	WRITE(*,*) 'IDEAL DOS INTEGRAL IS EQUAL TO =', INTDOS
	WRITE(1,*) 
	WRITE(1,*) INTDOS

C	���������� ���� �� ������� ��������

	INTDOS = 0.D0
	DO X = 1, (XFERMI-2)
		INTDOS = INTDOS + PDTOTAL(1 + X)
	END DO
	INTDOS = ESTEP*(((PDTOTAL(1) + PDTOTAL(XFERMI))/2.D0)+INTDOS)
	WRITE(1,*)
	WRITE(1,*) 'IDEAL DOS INTEGRAL BY TRAPEZOID IS EQUAL TO = '
	WRITE(1,*)
	WRITE(1,*) INTDOS 
	CLOSE(1)
	WRITE(*,*) 'FINISHED'
	WRITE(*,*)
	WRITE(*,*)

C**********************************************************************************************************
C 	������ ������ � �����
C**********************************************************************************************************

   	WRITE(*,*)
    	WRITE(*,*) 'CREATING INFO FILES...'

C	����� �������� ������� �����
 
	OPEN(21, FILE = 'G0000.TXT', FORM = 'FORMATTED') 
	OPEN(22, FILE = 'G001-1.TXT', FORM = 'FORMATTED')
	OPEN(23, FILE = 'G0010.TXT', FORM = 'FORMATTED')
	OPEN(24, FILE = 'G0011.TXT', FORM = 'FORMATTED')
	OPEN(25, FILE = 'G1-100.TXT', FORM = 'FORMATTED')
	OPEN(26, FILE = 'G1000.TXT', FORM = 'FORMATTED')
	OPEN(27, FILE = 'G1100.TXT', FORM = 'FORMATTED')
	OPEN(28, FILE = 'G1-11-1.TXT', FORM = 'FORMATTED')
	OPEN(29, FILE = 'G1-110.TXT', FORM = 'FORMATTED')
	OPEN(30, FILE = 'G1-111.TXT', FORM = 'FORMATTED')
	OPEN(31, FILE = 'G101-1.TXT', FORM = 'FORMATTED')
	OPEN(32, FILE = 'G1010.TXT', FORM = 'FORMATTED')
	OPEN(33, FILE = 'G1011.TXT', FORM = 'FORMATTED')   
	OPEN(34, FILE = 'G111-1.TXT', FORM = 'FORMATTED')
	OPEN(35, FILE = 'G1110.TXT', FORM = 'FORMATTED')
	OPEN(36, FILE = 'G1111.TXT', FORM = 'FORMATTED')

	IF (LMAX==3) THEN
	OPEN(37, FILE = 'G2-22-2.TXT', FORM = 'FORMATTED')
	OPEN(38, FILE = 'G2-12-1.TXT', FORM = 'FORMATTED')
	OPEN(39, FILE = 'G2020.TXT', FORM = 'FORMATTED')
	OPEN(40, FILE = 'G2121.TXT', FORM = 'FORMATTED')
	OPEN(41, FILE = 'G2222.TXT', FORM = 'FORMATTED')

	OPEN(42, FILE = 'G1-122.TXT', FORM = 'FORMATTED')
	OPEN(43, FILE = 'G1022.TXT', FORM = 'FORMATTED') 
	END IF

	DO X = 1, EPNTS
		E = EMIN+(DBLE(X-1))*ESTEP
		WRITE(21,*) E*EVS, DBLE(GLML1M1(1,1,1,1,X)),
	+DIMAG(GLML1M1(1,1,1,1,X))
		WRITE(22,*) E*EVS, DBLE(GLML1M1(1,1,2,1,X)),
	+DIMAG(GLML1M1(1,1,2,1,X))
		WRITE(23,*) E*EVS, DBLE(GLML1M1(1,1,2,2,X)),
	+DIMAG(GLML1M1(1,1,2,2,X))
		WRITE(24,*) E*EVS, DBLE(GLML1M1(1,1,2,3,X)),
	+DIMAG(GLML1M1(1,1,2,3,X))
		WRITE(25,*) E*EVS, DBLE(GLML1M1(2,1,1,1,X)),
	+DIMAG(GLML1M1(2,1,1,1,X))
		WRITE(26,*) E*EVS, DBLE(GLML1M1(2,2,1,1,X)),
	+DIMAG(GLML1M1(2,2,1,1,X))
		WRITE(27,*) E*EVS, DBLE(GLML1M1(2,3,1,1,X)),
	+DIMAG(GLML1M1(2,3,1,1,X))
		WRITE(28,*) E*EVS, DBLE(GLML1M1(2,1,2,1,X)),
	+DIMAG(GLML1M1(2,1,2,1,X))
		WRITE(29,*) E*EVS, DBLE(GLML1M1(2,1,2,2,X)),
	+DIMAG(GLML1M1(2,1,2,2,X))
		WRITE(30,*) E*EVS, DBLE(GLML1M1(2,1,2,3,X)),
	+DIMAG(GLML1M1(2,1,2,3,X))
		WRITE(31,*) E*EVS, DBLE(GLML1M1(2,2,2,1,X)),
	+DIMAG(GLML1M1(2,2,2,1,X))
		WRITE(32,*) E*EVS, DBLE(GLML1M1(2,2,2,2,X)),
	+DIMAG(GLML1M1(2,2,2,2,X))
		WRITE(33,*) E*EVS, DBLE(GLML1M1(2,2,2,3,X)),
	+DIMAG(GLML1M1(2,2,2,3,X))
		WRITE(34,*) E*EVS, DBLE(GLML1M1(2,3,2,1,X)),
	+DIMAG(GLML1M1(2,3,2,1,X))
		WRITE(35,*) E*EVS, DBLE(GLML1M1(2,3,2,2,X)),
	+DIMAG(GLML1M1(2,3,2,2,X))
		WRITE(36,*) E*EVS, DBLE(GLML1M1(2,3,2,3,X)),
	+DIMAG(GLML1M1(2,3,2,3,X))

	IF (LMAX==3) THEN
		WRITE(37,*) E*EVS, DBLE(GLML1M1(3,1,3,1,X)),
	+DIMAG(GLML1M1(3,1,3,1,X))
		WRITE(38,*) E*EVS, DBLE(GLML1M1(3,2,3,2,X)),
	+DIMAG(GLML1M1(3,2,3,2,X))
		WRITE(39,*) E*EVS, DBLE(GLML1M1(3,3,3,3,X)),
	+DIMAG(GLML1M1(3,3,3,3,X))
		WRITE(40,*) E*EVS, DBLE(GLML1M1(3,4,3,4,X)),
	+DIMAG(GLML1M1(3,4,3,4,X))
		WRITE(41,*) E*EVS, DBLE(GLML1M1(3,5,3,5,X)),
	+DIMAG(GLML1M1(3,5,3,5,X))
		WRITE(42,*) E*EVS, DBLE(GLML1M1(3,5,3,5,X)),
	+DIMAG(GLML1M1(2,1,3,5,X))
		WRITE(43,*) E*EVS, DBLE(GLML1M1(3,5,3,5,X)),
	+DIMAG(GLML1M1(2,2,3,5,X))


	END IF

	END DO

C	������� �������� � �������� �������: 
C 	L = LSMALL + 1
C  	MC = MSMALL + LSMALL + 1 = MSMALL + L

	CLOSE(21)
	CLOSE(22)
	CLOSE(23)
	CLOSE(24)
	CLOSE(25)
	CLOSE(26)
	CLOSE(27)
	CLOSE(28)
	CLOSE(29)
	CLOSE(30)
	CLOSE(31)
	CLOSE(32)
	CLOSE(33)
	CLOSE(34)
	CLOSE(35)
	CLOSE(36)

	IF (LMAX==3) THEN
	CLOSE(37)
	CLOSE(38)
	CLOSE(39)
	CLOSE(40)
	CLOSE(41)
	CLOSE(42)
	CLOSE(43)
	END IF
	

C**********************************************************************************************************
C	����������� �����
C**********************************************************************************************************

	WRITE(*,*) '!!! FULL CALCULATION FINISHED !!!'
 	WRITE(*,*) 'RESULTS IN OUT_GREEN1 AND POUT_GREEN1'
 
      WRITE(*,*)	 
C	CALL BEEPQQ(FREQUENCY, DURATION)
	CALL TIMESTAMP()

C	���������� ������, �������� ��������
C	CLOSE(60)
C	CLOSE(61)
C	CLOSE(64)
	CLOSE(65)
	CLOSE(66)
	CLOSE(7)

C	������ G � ����
	
	WRITE(*,*) "ENTER NAMES OF FILES TO DELETE OR PRESS ENTER: "
	LEN = GETSTRQQ(FILE)
	IF (FILE(1:LEN) .EQ. '*.*') THEN
	WRITE(*,*) "ARE YOU SURE (Y/N)?"
	CH = GETCHARQQ()
	IF ((CH .NE. 'Y') .AND. (CH .NE. 'Y')) STOP
	END IF
	COUNT = DELFILESQQ(FILE)
	WRITE(*,*) "DELETED ", COUNT, " FILES."

	WRITE(*,*) 'PRESS ENTER TO EXIT' 
	WRITE(*,*)

C************************************************************************************************************
C	������� � ��������� 
C************************************************************************************************************

	CONTAINS

C************************************************************************************************************

	SUBROUTINE ALBL(L, MSMALL, J, I, ALNAL, BLNAL)
	INTEGER I, J, L, MSMALL
	DOUBLE COMPLEX ALNAL, BLNAL
C	������� ������� ����� ��� ������� ���� A(L,N,ALPHA), B(L,N,ALPHA) ��� ���� K, MNP
	 K=(DBLE((J-1))*PI)/(DBLE(NINTERVALS)*C)
	 KP=K+(2*PI/C)*DBLE(P(I))
	 I1 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL, L-1, 1)
	 I2 = SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL, L-1, 2)
       ALNAL = I2*DCMPLX(PSIE(L,IT))-I1*DCMPLX(PSIER(L,IT))
	 BLNAL = I1*DCMPLX(PSIR(L,IT))-I2*DCMPLX(PSI(L,IT))
	END SUBROUTINE
 
C************************************************************************************************************

	DOUBLE COMPLEX FUNCTION ANBE(L, AS, BS, E)
	INTEGER L
	DOUBLE PRECISION E
	DOUBLE COMPLEX AS, BS
	ANBE = AS+DCMPLX(NLALPHA(L,IT))
	+*BS*DCMPLX((E-EL(L,IT)))
C	�������� �� ������: ANBE
C	����� ��������� ����� ��������� ��� ����. ��� ��� ��� L', MSMALL', N', ALPHA'
	END FUNCTION ANBE

C************************************************************************************************************

	SUBROUTINE RESULT3(J, L, L1, RES13, E, I, I_)
C	EXTERNAL RESULT1
C	��������� RESULT3 ����� ��������� �������� �������������� ���������������� ��������� - ����� ��� ������
C	�� ������ ������� ���������� �� AMNP, AMNP* � �� ��� ��������� � RESULT1.
C	��������� ��������� � ���������� ���������� RES10
	DOUBLE COMPLEX RES10, RES13, RES6, RES
	DOUBLE PRECISION RES11, E
	INTEGER J, L, L1, I, I_
	RES10 = (0.D0,0.D0)
	DO LAMBDA = 1, COUNTX
	 RES11 = E - BIGD(J, LAMBDA)
	 IF (ABS(RES11) <= EDELTA) THEN
	  RES10 = RES10 + DCMPLX(DELTANUM(EPSD, RES11))*BIGH(J, I, LAMBDA)
	+*DCONJG(BIGH(J, I_, LAMBDA))
	 END IF 
	END DO										 
      RES=ANBE(L, ASMALL(I,J), BSMALL(I,J), E)*
	+DCONJG(ANBE(L1, ASMALL1(I_,J), BSMALL1(I_,J), E))
	RES6 = RES5(J)*RES
	RES13 = RES10*RES6
	END SUBROUTINE RESULT3

C************************************************************************************************************
 	
	SUBROUTINE RESULT7(J, L, L1, RES13, E, I, I_)
C	EXTERNAL RESULT1
C	��������� RESULT7 ����� ��������� �������� �������������� ���������������� ��������� - ����� ��� ������
C	�� ������ ������� ���������� �� AMNP, AMNP* C ������ PRINCIPAL PART(P)
C	��������� ��������� � ���������� ���������� RES
	DOUBLE COMPLEX RES10, RES13, RES6, RES
	DOUBLE PRECISION RES11, E
	INTEGER J, L, L1, I, I_
	RES10 = (0.D0,0.D0)
	CL:DO LAMBDA = 1, COUNTX
	 RES11 = E - BIGD(J, LAMBDA)
	IF (ABS(RES11)>EDELTA) THEN	
	RES10 = RES10 + DCMPLX(1.D0/RES11)*BIGH(J, I, LAMBDA)
	+*DCONJG(BIGH(J, I_, LAMBDA))
	END IF
	END DO CL
      RES=ANBE(L, ASMALL(I,J), BSMALL(I,J), E)*
	+DCONJG(ANBE(L1, ASMALL1(I_,J), BSMALL1(I_,J), E))
	RES6 = RES5(J)*RES
	RES13 = RES10*RES6
	END SUBROUTINE RESULT7

C************************************************************************************************************
 	     
	DOUBLE PRECISION FUNCTION ALPH(L, IT)
	INTEGER L, IT
	ALPH = 1 + ((E-EL(L,IT))**2)*NLALPHA(L,IT)
	END FUNCTION

C************************************************************************************************************

C	��� SUBROUTINE ��������� �������� CJ*J+CY*Y
	SUBROUTINE RESULT4(I, RES4, MSMALL, RNA)
	INTEGER I,MSMALL, NBES
	
	DOUBLE PRECISION BESJ, BESJ1, BESY, BESY1,RES4, RNA, XBES
	EXTERNAL BESS, BESSYPROC
	XBES = KAPPA(I)*RNA
	NBES = MSMALL-M(I)
	CALL BESS(NBES, XBES, BESJ, BESJ1)
	CALL BESSYPROC(NBES, XBES, EPSBES,BESY, BESY1)
	RES4 = CJ(I)*BESJ+CY(I)*BESY
	
      END SUBROUTINE RESULT4

C************************************************************************************************************ 	

C	��� SUBROUTINE ��������� �������� ������� ���������� ������ ����� � �������� ��������� ���������
    	SUBROUTINE RESULT5(DDDDE)
	EXTERNAL DELTA
	INTEGER DELTA
	DOUBLE PRECISION DDDDE
	DDDDE = DBLE(DELTA(NA,NA1)*DELTA(AL,AL1)*DELTA(L,L1)*
	+DELTA(MSMALL,MSMALL1))*DSQRT(E)*ALPH(L, IT)*ALPH(L1, IT)
	END SUBROUTINE

C************************************************************************************************************

    	DOUBLE COMPLEX FUNCTION WHAT(L, MSMALL, L1, MSMALL1)
 
	INTEGER NEWL, L, MSMALL, NEWL1, L1, MSMALL1
	NEWL = L-1
	NEWL1 = L1-1
	WHAT = ((-1)**(NEWL-NEWL1))*((-1)**(NEWL+MSMALL))*
	+((-1)**(NEWL1+MSMALL1))

	END FUNCTION WHAT

C************************************************************************************************************

	INTEGER(4) FUNCTION CNTR(L, M, L1, M1, X)
C	������� ���������� ��������� COUNTER(�����) ��������� ����� PT �� ������ L, M, L1, M1
C	�������� �������� �������� ���� �����
	INTEGER L, M, L1, M1, I, X
	CL:DO I = 1, TOTALNUMPOINTS

		IF ((PT(I).LS==L).AND.(PT(I).MS==M).AND.
	+	(PT(I).LS1==L1).AND.(PT(I).MS1==M1).AND.(PT(I).EX==X)) THEN
		
		CNTR = PT(I).PNUM
		EXIT CL
		END IF

	END DO CL

	END FUNCTION CNTR

C************************************************************************************************************

	INTEGER FUNCTION ISDIAG(L, M, L1, M1)
C	������� ���������� ��������� COUNTER(�����) ��������� ����� PT �� ������ L, M, L1, M1
C	�������� �������� �������� ���� �����
	INTEGER L, M, L1, M1
	IF ((L==L1).AND.(M==M1)) THEN
		ISDIAG = 1
	ELSE
		ISDIAG = 0
	END IF

	END FUNCTION ISDIAG

C************************************************************************************************************


	END PROGRAM GFIDEAL