 	DOUBLE COMPLEX FUNCTION ANBE(L, MSMALL, J, NA, AL, E, X)
	INTEGER J, MSMALL, L, NA, AL, X
	DOUBLE PRECISION E
C	NA - ����� ������������ ������
C	X - ����� ������ � ������� PEXT
C	PEXT - ������, ���������� ������ �� �������� P, ������� ���������� ���� �� ����� 
	K=(DBLE(J)*PI)/(DBLE(NINTERVALS)*C)
	KP=K+(2*PI/C)*DBLE(PEXT(X))
	I1=SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL, L, 1) 
	I2=SIMP_I12(EPSI, RMT, KP, KAPPA(I), MSMALL, L, 2)
      ALNAL = I2*DCMPLX(PSIE(L+1))-I1*DCMPLX(PSIER(L+1))
	BLNAL = I1*DCMPLX(PSIR(L+1))-I2*DCMPLX(PSI(L+1))
	ANBE = ALNAL+DCMPLX(NLALPHA(L+1))*BLNAL*DCMPLX((E-EL(L+1)))
C	�������� �� ������: ANBE
C	����� ��������� ����� ��������� ��� ����. ��� ��� ��� L', MSMALL', N', ALPHA'
	END FUNCTION ANBE