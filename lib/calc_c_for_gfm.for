	double precision function CalcCJforGFM(m, a, b, k)
	implicit double precision(a-h,o-z)
	double precision a, b, k, eps
	integer m, s_m
	

	eps=1.0d-12

      s_m = iabs(m)
	
      call Bess(s_m,k*a,bja,bjap)
	call Bess(s_m,k*b,bjb,bjbp)
	call BessYproc(s_m,k*a,eps,bya,byap)
	call BessYproc(s_m,k*b,eps,byb,bybp)

      CalcCJforGFM = (dabs(a**2/2.0d0*(bjap-bjb*byap/byb)**2-
	*b**2/2.0d0*(bjbp-bjb*bybp/byb)**2))**(-0.5d0)

	end


	double precision function CalcCYforGFM(m, cj, b, k)
	implicit double precision(a-h,o-z)
	double precision b, k, eps
	integer m, s_m

	eps=1.0d-12

      s_m = iabs(m)

	call Bess(s_m,k*b,bjb,tmp)
	call BessYproc(s_m,k*b,eps,byb,tmp)

      CalcCYforGFM = -cj*bjb/byb

	end

	
