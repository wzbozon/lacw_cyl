 
      PROGRAM GFDEFECT

C****************************
C	���������� ����������
C****************************

	IMPLICIT NONE
	EXTERNAL GAUSSJ
100	FORMAT(F23.20)
1	FORMAT(A1\)

	COMMON /INDEFECT1/ EMIN, EMAX, EPNTS, EPSD, TDELTA, LMAX,
	+ NEARFERMI, DELTAFERMI

	DOUBLE PRECISION RMT(1), A5, E, EPSD, EMIN, EMAX, ESTEP,
	+INTS, TIME_OF_ALL, RAD(300,2), V(300,2), VR(300,2),
     +INTDOS, TDELTA, TIME_OF_TELEMENTS, RES1, RES2, DELTAFERMI,
	+EFERMI, DE, ALPHLL1, IS

	DOUBLE PRECISION, ALLOCATABLE :: NLALPHA(:,:), 
     +EL(:,:), PSIALL(:,:,:), PSIEALL(:,:,:),
     +DL(:), DLID(:), TELEMENTS(:,:,:), DOSTOTAL(:),
     +PDTOTAL(:), PDSTATES(:,:,:), DOSSTATES(:,:,:),
     +TMATRIX(:,:), EDMATRIX(:,:), PSI(:,:), QE(:,:)

	DOUBLE COMPLEX INTS1, RESS1, RESS2

	DOUBLE COMPLEX, ALLOCATABLE::  
	+GLML1M1(:,:,:,:,:),	G1(:,:,:,:,:), G2(:,:,:,:,:),
	+DEFGLML1M1(:,:,:,:,:),
	+GMATRIX(:,:), MIDMATRIX(:,:), RMIDMATRIX(:,:),
     +DEFGMATRIX(:,:), TESTMATRIX(:,:) 

	INTEGER I, I_, J, NT1, RES, IREADINDEFECT1,  
	+MSMALL, L, AL, NA, L1, NA1, AL1, X, X1, 
     +MSMALL1, EPNTS,LMAX, ALPHA, MNUM, MNUM1, 
	+CCOUNTER, CNUM, MC, MC1, NEARFERMI, XFERMI, 
	+A1, A2, A3, A4, IT, IT1, IT2, JRIS, MYX

	DOUBLE PRECISION, PARAMETER :: 
	+PI=3.14159265358979323846264338328D0
	DOUBLE PRECISION, PARAMETER :: EPSCONST = 0.00001D0
	DOUBLE PRECISION, PARAMETER:: EPSBES = 1.0D-12
	DOUBLE PRECISION, PARAMETER:: EVS=13.6058D0
	DOUBLE COMPLEX, PARAMETER:: IMAGINE=(0.D0, 1.D0)
	INTEGER NT
	INTEGER, PARAMETER:: NGRID = 300
	INTEGER, PARAMETER:: NLMAX = 8

17	FORMAT(F5.3,1X,F9.3)
 
C	������������� �������� ������� ��������� 
	TIME_OF_ALL=0.D0
	CALL TTIME(TIME_OF_ALL,'CALCULATION TIME ',0)

	WRITE(*,*) '********************************************'
	WRITE(*,*) 'GREEN FUNCTION AND LACW METHOD PROGRAM'
	WRITE(*,*) '********************************************'
	WRITE(*,*)
	CALL TIMESTAMP()

C****************************************************************************
C	���������� ����� �������� ������� COUNTX � ������ ���������� �� ������
C****************************************************************************

	OPEN(111,FILE='CHECK MATRICES.TXT')
 
	NT = 2
	IT1 = 1
	IT2 = 2
	IT=1
C     NT - ����� ����� ������
C	IT1 - ����� ������� ���� �����
C	IT2 - ����� ������� ���� �����

	ALLOCATE(EL(NLMAX,NT), NLALPHA(NLMAX,NT))	
	
	WRITE(*,*) 'IT1 = ', IT1
	WRITE(*,*) 'IT2 = ', IT2
	WRITE(*,*) 'NT = ', NT

C*********************************************
C	���������� ����� � ����������� �������
C 	EPNTS - ����� ����� �� �������
C	EPSD - ������� � ������ ������� ������
C*********************************************

      WRITE(*,*)
	WRITE(*,*) 'READING INDEFECT1.DAT...'
	RES = IREADINDEFECT1('INDEFECT1.DAT')
	WRITE(*,*) 'RES = ', RES
	WRITE(*,*) 'EMIN IS READ AS ', EMIN
	WRITE(*,*) 'EMAX IS READ AS ', EMAX
	WRITE(*,*) 'EPNTS IS READ AS ', EPNTS
	WRITE(*,*) 'EPSD IS READ AS ', EPSD
	WRITE(*,*) 'TDELTA IS READ AS ', TDELTA
	WRITE(*,*) 'LMAX IS READ AS ', LMAX
	WRITE(*,*) 'CALCULATE NEAR FERMI ENERGY?(1=YES,0=NO)', NEARFERMI
	WRITE(*,*) 'DELTAFERMI IS READ AS ', DELTAFERMI
	WRITE(*,*) 'RECALCULATION TO RYDBERGS...'
	EMIN = EMIN/EVS
	EMAX = EMAX/EVS
	WRITE(*,*) 'EMIN = ', EMIN
	WRITE(*,*) 'EMAX = ', EMAX
	IF (NEARFERMI==1) THEN
		EMIN = EFERMI - DELTAFERMI/EVS
		EMAX = EFERMI + DELTAFERMI/EVS
		WRITE(*,*) 'NEW EMIN,EMAX = ', EMIN, EMAX
	END IF

C*******************************
C	���������� JRIS, RMT, RAD
C*******************************

	OPEN(1, FILE = 'indefect2.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		READ(1,*) JRIS, RMT(IT)
		READ(1,*) RAD(:,IT)
	END DO
	CLOSE(1)

	ALLOCATE(PSIALL(NT, NLMAX, JRIS),PSIEALL(NT, NLMAX, JRIS))

C*********************************************************
C	���������� ������� ����������� ��� ��������� ������
C*********************************************************

	OPEN(1, FILE = 'indefect3.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		READ(1,*) V(:,IT)
	END DO
	CLOSE(1)

C	������� � V �� VR
	DO IT = 1, NT
		DO X = 1, JRIS
			VR(X,IT) = V(X,IT)*RAD(X,IT)
		END DO
	END DO

	WRITE(*,*) 'VR IN ZERO = ', VR(1,1)
	WRITE(*,*) 'VR2 IN ZERO = ', VR(1,2)

	OPEN(1, FILE = 'V(r).TXT', FORM = 'FORMATTED')
	WRITE(*,*) 'POINT1'
	write(*,*) 'JRIS = ', JRIS
	DO I = 1, JRIS
		WRITE(1,*) RAD(I,1), V(I,1), V(I,2)
	END DO
	CLOSE(1)
	WRITE(*,*) 'POINT2'



*	OPEN(1, FILE = 'U(r).TXT', FORM = 'FORMATTED')
*	DO X = 1, JRIS
*		WRITE(1,*) RAD(X,1), PSIALL(1,1,X), PSIALL(2,1,X)
*	END DO
*	CLOSE(1)
	WRITE(*,*) 'POINT22'
	WRITE(*,*) JRIS
	WRITE(*,*) NT
	WRITE(*,*) NLMAX
	WRITE(*,*) RAD(JRIS,1)
	READ(*,*)

C********************************************************************
C	���������� PSIALL(NT, NLMAX, EPNTS), PSIEALL(NT, NLMAX, EPNTS)
C********************************************************************

  	OPEN(1, FILE = 'indefect4.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIALL(IT, L, X), A1, A2, A3
	WRITE(*,*) 'READ POINT # ', IT, L, X
	WRITE(*,*) PSIALL(IT,L,X)
	PSIALL(IT,L,X) = PSIALL(IT, L, X)/RAD(X,1)
	END DO
	END DO
	END DO
	CLOSE(1)
	WRITE(*,*) 'POINT3'

 	OPEN(1, FILE = 'indefect5.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
	DO L = 1, NLMAX
	DO X = 1, JRIS
	READ(1, *) PSIEALL(IT, L, X), A1, A2, A3
	PSIEALL(IT, L, X)=PSIEALL(IT, L, X)/RAD(X,1)
	END DO
	END DO
	END DO
	CLOSE(1)
	WRITE(*,*) 'POINT4'

C**********************************************************************
C     ������ ������ � ������-��� ����� ��������������� U^2*R^2*dR 
C**********************************************************************

	WRITE(*,*) 'Calculation of Charges in MT-Sphere...'
	WRITE(*,*) 'NT = ', NT
	WRITE(*,*) 'NLMAX = ', NLMAX
	WRITE(*,*) 'RAD(50,1) = ', RAD(50,1)
	ALLOCATE(QE(NT,NLMAX))
	OPEN(1, FILE = 'Charges in MT-Sphere.TXT',FORM = 'FORMATTED')
	WRITE(*,*) 'File successfully opened...'


	DO IT = 1, NT
	DO L = 1, NLMAX
	IS = 0.D0

C	IS - ������������ �����				  
C	QE - ������ ����������� �������
C	PSIALL - �������� ������� ��� ������� L, ������� ���� ����� � ������ ����� �� R
C	������� ������� ��������������� �� ��������(����������� ����� �� ����� �����)

	DO X = 1, JRIS-1
	IS = IS + (PSIALL(IT, L, X)*PSIALL(IT, L, X)*RAD(X,IT)*RAD(X,IT)*
	*(RAD(X+1,IT)-RAD(X,IT)))
	END DO

	QE(IT,L) = 4*PI*IS
	WRITE(1,*) IT, L-1, QE(IT, L)
	WRITE(*,*) 'Charges successfully calculated for: '
	WRITE(*,*) 'IT=',IT,'	l=',L-1,'	QE=',QE(IT,L), '	IS=', IS 


	END DO
	WRITE(*,*) 'Charges successfully calculated for IT=', IT
	END DO
	WRITE(*,*) 'All charges successfully calculated...'

	CLOSE(1)
	WRITE(*,*) 'File successfully closed...'
	READ(*,*)

C***********************************************************************
C	���������� NLALPHA(NLMAX,NT)
C***********************************************************************

	OPEN(1, FILE = 'indefect6.dat', FORM = 'FORMATTED')
	DO IT = 1, NT
		DO L = 1, NLMAX 
 			READ(1,*) NLALPHA(L, IT), A1, A2
		END DO
	END DO
	CLOSE(1)

C******************************************
C	���������� � ���������� EL(NLMAX,NT)
C******************************************

	OPEN(1, FILE = 'indefect7.dat', FORM = 'FORMATTED')
 	DO IT = 1, NT
	 DO I = 1, 5
		READ(1,*) EL(I,IT), A1, A2
	END DO
	DO I = 6, NLMAX
		EL(I, IT) = EL(5, IT)
	END DO
	END DO
	CLOSE(1)

	WRITE(*,*) 'JRIS AND JRIS ARE READ AS: ', JRIS, JRIS

	IT = 1

	ESTEP = (EMAX-EMIN)/DBLE((EPNTS-1))

C*************************************************
C	���������� ������ �� EF.DAT - ������� �����
C*************************************************

	OPEN(1, FILE = 'EF.DAT', FORM = 'FORMATTED')
	READ(1,17) A5, EFERMI
	CLOSE(1)
	WRITE(*,*) 'EFERMI IS READ AS = ', EFERMI
	EFERMI = EFERMI/EVS
	WRITE(*,*) 'EFERMI IN RIDBERGS = ', EFERMI

C***************************************
C     ����������� ����� � ������� �����
C***************************************

	DO X = 1, EPNTS
		E = EMIN+(DBLE(X-1))*ESTEP
		DE = E - EFERMI
		WRITE(*,*) E, EFERMI, DE
		IF (DE > 0) THEN
			XFERMI = X
			EXIT
		END IF
	END DO
	WRITE(*,*) 'XFERMI IS CALCULATED AS ', XFERMI

C***************************************
	
	MNUM = (LMAX-1)*2 + 1

	WRITE(*,*) 'MNUM IS EQUAL TO = ', MNUM
	CNUM = ((1+MNUM)/2)*((MNUM-1)/2+1)

	ALLOCATE(GLML1M1(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+G1(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+G2(LMAX,MNUM,LMAX,MNUM,EPNTS),
	+DEFGLML1M1(LMAX,MNUM,LMAX,MNUM,EPNTS))
     	
	CCOUNTER = 0

	ALLOCATE(DL(EPNTS), DLID(EPNTS), DOSTOTAL(EPNTS),
	+PDTOTAL(EPNTS), PDSTATES(LMAX,MNUM,EPNTS),
     +DOSSTATES(LMAX, MNUM, EPNTS))

	CCOUNTER = 0

     	I =1
  	I_=1
      X = 1
	X1 = 1
	J = 1
	NA = 1
	AL = 1
	NA1 = 1
	AL1 = 1
	ALPHA = 1


C************************************************************************************
C	 ������ �������������� ����� ������������ ��������� �� ������� ��������-�������
C************************************************************************************
c	Write(*,*) 'Calculation of diagonal elements by Kramers-Kronig...'
c	Write(*,*)
c	Write(*,*) 'l,m,l1,m1'
c	Write(*,*)
c	DO L = 1, LMAX
c c	 	MC = 0
c	 	DO MSMALL = -(L-1), (L-1)
c	 	 	MC = MC + 1
c	 		DO L1 = 1, LMAX
c	  			MC1 = 0
c	  			DO MSMALL1 = -(L1-1), (L1-1)
c	 				 MC1 = MC1 + 1
c					 IF ((L==L1).AND.(MSMALL==MSMALL1)) THEN
c					 WRITE(*,*) L-1, MSMALL1, L1-1, MSMALL1
c c	 				 DO X =	1, EPNTS
c		E = EMIN+(DBLE(X-1))*ESTEP
c 		INTS1 = (0.D0,0.D0)
c		DO X1 = 1, EPNTS-1
c			CALL RESULT8(X1, RESS1)
c			CALL RESULT8(X1+1, RESS2)
c			INTS1 = INTS1 + (RESS1+RESS2)*0.5D0*DCMPLX(ESTEP)
c		ENDDO	
c		CALL RESULT6(IT1, L, L1, E, ALPHLL1)
c		G2(L,MC,L1,MC1,X) = -(1.D0/ALPHLL1)*(1.D0/PI)*INTS1
c		
c c					 END DO
c					 END IF
c c				END DO
c 			END DO
c		END DO
c  	END DO
c	WRITE(*,*)
c

C******************************************
C	������ ��������� ��������� T-�������
C******************************************

C	������������� �������� ������� ������� ������� T
 	TIME_OF_TELEMENTS=0.D0
	CALL TTIME(TIME_OF_TELEMENTS,'TIME OF T MATRIX',0)

      WRITE(*,*)
	WRITE(*,*)
	WRITE(*,*) 'CALCULATION OF T - MATRIX... '
	OPEN(22, FILE = 'TELEMENTS.TXT', FORM = 'FORMATTED')
	NT1 = 2
	ALLOCATE(TELEMENTS(LMAX, EPNTS, NT1))
	
	DO IT = 1, NT
	DO L = 1, LMAX
	DO X = 1, EPNTS
		E = EMIN+(DBLE(X-1))*ESTEP
C		����� X - ����� ����� �� E
		INTS = 0.D0
		DO I = 1, (JRIS-1) 
		 	CALL RESULT2(IT, L, E, I ,RES1)
			CALL RESULT2(IT, L, E, I+1 ,RES2)
			INTS = INTS + ((RES2+RES1)/2.D0)*(RAD(I+1,IT)-RAD(I,IT))
		END DO

	TELEMENTS(L, X, IT) = INTS

	END DO
	WRITE(*,1) '.'

	END DO
	END DO
	
	WRITE(*,*)
	WRITE(*,*) 'FINISHED'
	WRITE(22,*) TELEMENTS
	CALL TTIME(TIME_OF_TELEMENTS,'TIME OF T MATRIX',0)
	 
C********************************
C	����� T(E,L) ��� ������ NT	
C********************************
	
      IF ((NT==2).AND.(LMAX>1)) THEN
      OPEN(23, FILE='TE_IT1_L0.TXT', FORM='FORMATTED')
	OPEN(24, FILE='TE_IT1_L1.TXT', FORM='FORMATTED')	
	OPEN(25, FILE='TE_IT2_L0.TXT', FORM='FORMATTED') 
	OPEN(26, FILE='TE_IT2_L1.TXT', FORM='FORMATTED')


	DO X = 1, EPNTS 
		E = EMIN+(DBLE(X-1))*ESTEP
		WRITE(23,*) E*EVS, TELEMENTS(1, X, 1)
		WRITE(24,*) E*EVS, TELEMENTS(2, X, 1)
		WRITE(25,*) E*EVS, TELEMENTS(1, X, 2)
		WRITE(26,*) E*EVS, TELEMENTS(2, X, 2)
	END DO
	
	CLOSE(23)
	CLOSE(24)
	CLOSE(25)
	CLOSE(26)
	END IF


C	����� �(E,L) ��� ������ ������ ��� L=3
	IF ((NT==2).AND.(LMAX>2)) THEN
	OPEN(23, FILE='TE_IT1_L2.TXT', FORM='FORMATTED')
	OPEN(24, FILE='TE_IT2_L2.TXT', FORM='FORMATTED')

	DO X = 1, EPNTS 
		 E = EMIN+(DBLE(X-1))*ESTEP
		 WRITE(23,*) E*EVS, TELEMENTS(3, X, 1)
		 WRITE(24,*) E*EVS, TELEMENTS(3, X, 2)	
	END DO

	CLOSE(23)
	CLOSE(24)
	
	END IF

C	����� �(E,L) ��� ������ ������ ��� L=4
 	IF ((NT==2).AND.(LMAX>3)) THEN
	OPEN(23, FILE='TE_IT1_L3.TXT', FORM='FORMATTED')
	OPEN(24, FILE='TE_IT2_L3.TXT', FORM='FORMATTED')

	DO X = 1, EPNTS 
		 E = EMIN+(DBLE(X-1))*ESTEP
		 WRITE(23,*) E*EVS, TELEMENTS(4, X, 1)
		 WRITE(24,*) E*EVS, TELEMENTS(4, X, 2)	
	END DO

	CLOSE(23)
	CLOSE(24)

	END IF  
	   
	    
		 
C	������� �������� � �������� �������: 
C	L = LSMALL + 1
C	MC = MSMALL + LSMALL + 1

C********************************************************************************************
C	���������� ��������� ������� GLML1M1(L,MC,L1,MC1,X) ������� ����� ��������� ����������
C********************************************************************************************

	WRITE(*,*)
	WRITE(*,*) 'READING INDEFECT8.DAT ... '
	WRITE(*,*)
	OPEN(1, FILE = 'indefect8.dat', FORM = 'FORMATTED')
	DO L = 1, LMAX
	 	MC = 0
		DO MSMALL = -(L-1), (L-1)
	 		MC = MC + 1
	 		DO L1 = 1, LMAX
	  			MC1 = 0
	  			DO MSMALL1 = -(L1-1), (L1-1)
	 				 CCOUNTER = CCOUNTER + 1
	 				 MC1 = MC1 + 1
	 				 DO X =	1, EPNTS
						READ(1,*)
	READ(1,*) GLML1M1(L,MC,L1,MC1,X), A1, A2, A3, A4, A5
c	IF ((L.NE.L1).OR.(MSMALL.NE.MSMALL1)) THEN
c	GLML1M1(L,MC,L1,MC1,X) = (0.d0,0.d0)
c	END IF
					 END DO
				END DO
			END DO
		END DO
	END DO
	CLOSE(1)

C	��������

	OPEN(1, FILE = 'GLML1M1READ.TXT', FORM = 'FORMATTED')
	DO L = 1, LMAX
 	 	MC = 0
		DO MSMALL = -(L-1), (L-1)
	 		MC = MC + 1
	 		DO L1 = 1, LMAX
	  			MC1 = 0
	  			DO MSMALL1 = -(L1-1), (L1-1)
	 				 CCOUNTER = CCOUNTER + 1
	 				 MC1 = MC1 + 1
 	 				 DO X =	1, EPNTS
 						WRITE(1,*)
 						WRITE(1,*) GLML1M1(L,MC,L1,MC1,X), 
 	+					L-1, MSMALL, L1-1, MSMALL1, X
 					 END DO
 				END DO
 			END DO
		END DO
  	END DO
      CLOSE(1)
	WRITE(*,*)
	WRITE(*,*) 'READING FINISHED'
	WRITE(*,*)


C*****************************************
C	������ G-������� ������� � ��������
C*****************************************

C 	WRITE(*,*) 'FOR WHICH X SHOULD CHECK? '
C 	READ(*,*) MYX
	MYX = 1
	WRITE(*,*) 'MATRIX CALCULATIONS BEGAN...'
C	WRITE(*,*) 'CNUM = ', CNUM

	ALLOCATE(GMATRIX(CNUM,CNUM), TMATRIX(CNUM,CNUM),
	+ EDMATRIX(CNUM,CNUM), DEFGMATRIX(CNUM, CNUM),
     + MIDMATRIX(CNUM,CNUM), RMIDMATRIX(CNUM,CNUM),
     + TESTMATRIX(CNUM,CNUM))
	OPEN(20,FILE = 'TMATRICES.TXT',
	+ FORM = 'FORMATTED', STATUS = 'UNKNOWN')
	OPEN(10,FILE = 'PDSTATES.TXT',FORM='FORMATTED')
	DO X = 1, EPNTS
C	WRITE(*,*) X
	E = EMIN+(DBLE(X-1))*ESTEP
	I = 0
	J = 0
	DO L = 1, LMAX
		MNUM=(L-1)*2+1
		MC = 0
		DO MSMALL = -(L-1), (L-1)
			MC = MC + 1
			J = 0
			I = I + 1
			DO L1 = 1, LMAX
				MNUM1=(L1-1)*2+1
				MC1 = 0
				DO MSMALL1 = -(L1-1),(L1-1)
				MC1 = MC1 + 1
				J = J + 1
				GMATRIX(I,J) = GLML1M1(L,MC,L1,MC1,X)
				 	IF (I==J) THEN
						TMATRIX(I,J) = TELEMENTS(L, X, IT2)
	+					 - TELEMENTS(L, X, IT1)
						EDMATRIX(I,J) = 1.D0
					ELSE
						TMATRIX(I,J) = 0.D0
						EDMATRIX(I,J) = 0.D0
					END IF
				END DO
			END DO
		END DO
	END DO

	IF (X==MYX) THEN
 	WRITE(111,*) 'E=',E
	WRITE(111,*) 'X=',X
	WRITE(111,*) '--------------- GMATRIX ---------------'
	WRITE(111,*) GMATRIX
	WRITE(111,*) '--------------- TMATRIX ---------------'
	WRITE(111,*) TMATRIX
	WRITE(111,*) '--------------- EDMATRIX ---------------'
	WRITE(111,*) EDMATRIX
	END IF

	WRITE(20,*) 'X=', X
	WRITE(20,*) 'TMATRIX'
	WRITE(20,*) TMATRIX
	WRITE(20,*)
	WRITE(20,*)
	  
	 
	MIDMATRIX = EDMATRIX - MATMUL(GMATRIX,TMATRIX)
	RMIDMATRIX = MIDMATRIX

C	�������� ������������ ������� MIDMATRIX
C	NLEAD = 4
C	CALL ADG2C(CMPLX(MIDMATRIX),CNUM,CNUM,NLEAD,DET1,
C	+DET2,RCOND,Z,IERR)
C	WRITE(*,*) DET1, DET2
	
	CALL GAUSSJ(MIDMATRIX, CNUM, CNUM)
	TESTMATRIX = MATMUL(MIDMATRIX, RMIDMATRIX)
	DEFGMATRIX = MATMUL(MIDMATRIX, GMATRIX)
 
 	IF(X==MYX) THEN
	WRITE(111,*) '--------------- RMIDMATRIX --------------'
	WRITE(111,*) RMIDMATRIX
	WRITE(111,*) '--------------- TESTMATRIX --------------'
	WRITE(111,*) TESTMATRIX
  	WRITE(111,*) '--------------- MIDMATRIX ---------------'
	WRITE(111,*) MIDMATRIX
	WRITE(111,*) '---------------DEFGMATRIX ---------------'
	WRITE(111,*) DEFGMATRIX 
	END IF

	I = 0
	J = 0
	I = 0
	IT = 1
	DO L = 1, LMAX
		MNUM=(L-1)*2+1
		MC = 0
		DO MSMALL = -(L-1),(L-1)
			MC = MC + 1	
			J = 0
			I = I + 1
			DO L1 = 1, LMAX		
				MNUM=(L1-1)*2+1
				MC1 = 0
				DO MSMALL1 = -(L1-1),(L1-1)
				MC1 = MC1 + 1
				J = J + 1
		DEFGLML1M1(L,MC,L1,MC1,X) = DEFGMATRIX(I,J)
				IF ((L==L1).AND.(MSMALL==MSMALL1)) THEN 										 
					PDSTATES(L, MC, X) = ALPH(L, IT2)*(DSQRT(E)-
	+		DIMAG(DEFGLML1M1(L,MC,L1,MC1,X)))/PI
					WRITE(10,*) PDSTATES(L,MC,X),L,MC,X
		 		END IF
				END DO
		 	END DO
	 	END DO
	END DO
 	 
	END DO

	WRITE(*,*) 'MATRIX CALCULATIONS FINISHED.'

C	����� ������������ ������� ����� ����������� �� �������� ������� (�������������� �����)
c
c	OPEN(21, FILE = 'KG0000.TXT', FORM = 'FORMATTED') 
c	OPEN(28, FILE = 'KG1-11-1.TXT', FORM = 'FORMATTED')
c	OPEN(32, FILE = 'KG1010.TXT', FORM = 'FORMATTED')
c	OPEN(36, FILE = 'KG1111.TXT', FORM = 'FORMATTED')
c	DO X = 1, EPNTS
c		 E = EMIN+(DBLE(X-1))*ESTEP
c		 WRITE(21,*) E*EVS, DBLE(G2(1,1,1,1,X)),
c	+DIMAG(GLML1M1(1,1,1,1,X))
c		 WRITE(28,*) E*EVS, DBLE(G2(2,1,2,1,X)),
c	+DIMAG(GLML1M1(2,1,2,1,X))
c		 WRITE(32,*) E*EVS, DBLE(G2(2,2,2,2,X)),
c	+DIMAG(GLML1M1(2,2,2,2,X))
c		 WRITE(36,*) E*EVS, DBLE(G2(2,3,2,3,X)),
c	+DIMAG(GLML1M1(2,3,2,3,X))
c	END DO
c	CLOSE(21)
c	CLOSE(28)
c	CLOSE(32)
c	CLOSE(36)

C	����� �������� ������� �����
 
	OPEN(21, FILE = 'DG0000.TXT', FORM = 'FORMATTED') 
	OPEN(22, FILE = 'DG001-1.TXT', FORM = 'FORMATTED')
	OPEN(23, FILE = 'DG0010.TXT', FORM = 'FORMATTED')
	OPEN(24, FILE = 'DG0011.TXT', FORM = 'FORMATTED')
	OPEN(25, FILE = 'DG1-100.TXT', FORM = 'FORMATTED')
	OPEN(26, FILE = 'DG1000.TXT', FORM = 'FORMATTED')
	OPEN(27, FILE = 'DG1100.TXT', FORM = 'FORMATTED')
	OPEN(28, FILE = 'DG1-11-1.TXT', FORM = 'FORMATTED')
	OPEN(29, FILE = 'DG1-110.TXT', FORM = 'FORMATTED')
	OPEN(30, FILE = 'DG1-111.TXT', FORM = 'FORMATTED')
	OPEN(31, FILE = 'DG101-1.TXT', FORM = 'FORMATTED')
	OPEN(32, FILE = 'DG1010.TXT', FORM = 'FORMATTED')
	OPEN(33, FILE = 'DG1011.TXT', FORM = 'FORMATTED')   
	OPEN(34, FILE = 'DG111-1.TXT', FORM = 'FORMATTED')
	OPEN(35, FILE = 'DG1110.TXT', FORM = 'FORMATTED')
	OPEN(36, FILE = 'DG1111.TXT', FORM = 'FORMATTED')

	IF (LMAX==3) THEN
	OPEN(37, FILE = 'DG2-22-2.TXT', FORM = 'FORMATTED')
	OPEN(38, FILE = 'DG2-12-1.TXT', FORM = 'FORMATTED')
	OPEN(39, FILE = 'DG2020.TXT', FORM = 'FORMATTED')
	OPEN(40, FILE = 'DG2121.TXT', FORM = 'FORMATTED')
	OPEN(41, FILE = 'DG2222.TXT', FORM = 'FORMATTED')

	OPEN(42, FILE = 'DG1-122.TXT', FORM = 'FORMATTED')
	OPEN(43, FILE = 'DG1022.TXT', FORM = 'FORMATTED') 
	END IF
	 	

 	DO X = 1, EPNTS
		E = EMIN+(DBLE(X-1))*ESTEP
		WRITE(21,*) E*EVS, DBLE(DEFGLML1M1(1,1,1,1,X)),
	+DIMAG(DEFGLML1M1(1,1,1,1,X))
		WRITE(22,*) E*EVS, DBLE(DEFGLML1M1(1,1,2,1,X)),
	+DIMAG(DEFGLML1M1(1,1,2,1,X))
		WRITE(23,*) E*EVS, DBLE(DEFGLML1M1(1,1,2,2,X)),
	+DIMAG(DEFGLML1M1(1,1,2,2,X))
		WRITE(24,*) E*EVS, DBLE(DEFGLML1M1(1,1,2,3,X)),
	+DIMAG(DEFGLML1M1(1,1,2,3,X))
		WRITE(25,*) E*EVS, DBLE(DEFGLML1M1(2,1,1,1,X)),
	+DIMAG(DEFGLML1M1(2,1,1,1,X))
		WRITE(26,*) E*EVS, DBLE(DEFGLML1M1(2,2,1,1,X)),
	+DIMAG(DEFGLML1M1(2,2,1,1,X))
		WRITE(27,*) E*EVS, DBLE(DEFGLML1M1(2,3,1,1,X)),
	+DIMAG(DEFGLML1M1(2,3,1,1,X))
		WRITE(28,*) E*EVS, DBLE(DEFGLML1M1(2,1,2,1,X)),
	+DIMAG(DEFGLML1M1(2,1,2,1,X))
		WRITE(29,*) E*EVS, DBLE(DEFGLML1M1(2,1,2,2,X)),
	+DIMAG(DEFGLML1M1(2,1,2,2,X))
		WRITE(30,*) E*EVS, DBLE(DEFGLML1M1(2,1,2,3,X)),
	+DIMAG(DEFGLML1M1(2,1,2,3,X))
		WRITE(31,*) E*EVS, DBLE(DEFGLML1M1(2,2,2,1,X)),
	+DIMAG(DEFGLML1M1(2,2,2,1,X))
		WRITE(32,*) E*EVS, DBLE(DEFGLML1M1(2,2,2,2,X)),
	+DIMAG(DEFGLML1M1(2,2,2,2,X))
		WRITE(33,*) E*EVS, DBLE(DEFGLML1M1(2,2,2,3,X)),
	+DIMAG(DEFGLML1M1(2,2,2,3,X))
		WRITE(34,*) E*EVS, DBLE(DEFGLML1M1(2,3,2,1,X)),
	+DIMAG(DEFGLML1M1(2,3,2,1,X))
		WRITE(35,*) E*EVS, DBLE(DEFGLML1M1(2,3,2,2,X)),
	+DIMAG(DEFGLML1M1(2,3,2,2,X))
		WRITE(36,*) E*EVS, DBLE(DEFGLML1M1(2,3,2,3,X)),
	+DIMAG(DEFGLML1M1(2,3,2,3,X))

	IF (LMAX==3) THEN
		WRITE(37,*) E*EVS, DBLE(DEFGLML1M1(3,1,3,1,X)),
	+DIMAG(DEFGLML1M1(3,1,3,1,X))
		WRITE(38,*) E*EVS, DBLE(DEFGLML1M1(3,2,3,2,X)),
	+DIMAG(DEFGLML1M1(3,2,3,2,X))
		WRITE(39,*) E*EVS, DBLE(DEFGLML1M1(3,3,3,3,X)),
	+DIMAG(DEFGLML1M1(3,3,3,3,X))
		WRITE(40,*) E*EVS, DBLE(DEFGLML1M1(3,4,3,4,X)),
	+DIMAG(DEFGLML1M1(3,4,3,4,X))
		WRITE(41,*) E*EVS, DBLE(DEFGLML1M1(3,5,3,5,X)),
	+DIMAG(DEFGLML1M1(3,5,3,5,X))
		WRITE(42,*) E*EVS, DBLE(DEFGLML1M1(3,5,3,5,X)),
	+DIMAG(DEFGLML1M1(2,1,3,5,X))
		WRITE(43,*) E*EVS, DBLE(DEFGLML1M1(3,5,3,5,X)),
	+DIMAG(DEFGLML1M1(2,2,3,5,X))


	END IF

	END DO

C	������� �������� � �������� �������: 
C 	L = LSMALL + 1
C  	MC = MSMALL + LSMALL + 1 = MSMALL + L

	CLOSE(21)
	CLOSE(22)
	CLOSE(23)
	CLOSE(24)
	CLOSE(25)
	CLOSE(26)
	CLOSE(27)
	CLOSE(28)
	CLOSE(29)
	CLOSE(30)
	CLOSE(31)
	CLOSE(32)
	CLOSE(33)
	CLOSE(34)
	CLOSE(35)
	CLOSE(36)

	IF (LMAX==3) THEN
	CLOSE(37)
	CLOSE(38)
	CLOSE(39)
	CLOSE(40)
	CLOSE(41)
	CLOSE(42)
	CLOSE(43)
	END IF

C	FORTRAN ������� �� �������� �������
	OPEN(1,FILE = 'MATRICES', FORM = 'FORMATTED',
	+ STATUS = 'UNKNOWN')

	WRITE(1,*) 'GMATRIX'
	WRITE(1,*) GMATRIX
	WRITE(1,*)
	WRITE(1,*) 'TMATRIX'
	WRITE(1,*) TMATRIX
	WRITE(1,*)
	WRITE(1,*) 'EDMATRIX'
	WRITE(1,*) EDMATRIX
	WRITE(1,*)
	WRITE(1,*) 'MIDMATRIX'
	WRITE(1,*) MIDMATRIX
	WRITE(1,*) 
	WRITE(1,*) 'RMIDMATRIX'
	WRITE(1,*) RMIDMATRIX
	WRITE(1,*)
	WRITE(1,*) 'TESTMATRIX'
	WRITE(1,*) TESTMATRIX
	CLOSE(1)
	WRITE(*,*) 'MATRICES WRITTEN TO FILES.'

C****************************************
C 	������ ������� DOSSTATES(L, MC, X)
C****************************************
	
      DO X = 1, EPNTS
	DO L = 1, LMAX
	 	MNUM=(L-1)*2+1
		MC = 0
		DO MSMALL = -(L-1),(L-1)
			MC = MC + 1	
			DO L1 = 1, LMAX		
				MNUM=(L1-1)*2+1
				MC1 = 0
				DO MSMALL1 = -(L1-1),(L1-1)
				MC1 = MC1 + 1
				IF ((L==L1).AND.(MSMALL==MSMALL1)) THEN
					E = EMIN+(DBLE(X-1))*ESTEP				 
	 	 			DOSSTATES(L, MC, X) = ALPH(L, IT1)*(DSQRT(E)-
	+				DIMAG(GLML1M1(L,MC,L1,MC1,X)))/PI
		 		END IF
				END DO
		 	END DO
		END DO
 	END DO
	END DO

C*****************************************************
C	������ ��������� ��������� ��������� ����������
C*****************************************************
 	  
 	WRITE(*,*) 'CALCULATION OF DOS OF IDEAL TUBE...'
	DO X = 1, EPNTS
	DOSTOTAL(X) = 0.D0
	DO L = 1, LMAX
	MNUM=(L-1)*2+1
	 DO MC = 1, MNUM
		DOSTOTAL(X) = DOSTOTAL(X) + DOSSTATES(L, MC, X)
	 END DO
	END DO
	E = EMIN+(DBLE(X-1))*ESTEP
	END DO


C******************************************************
C	������ ��������� ��������� ���������� C ��������
C******************************************************

	WRITE(*,*) 'CALCULATION OF DOS OF DEFECT TUBE...'

C	FULL DOS

	OPEN(51, FILE='DEF_GREEN1', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	PDTOTAL(X) = 0.D0
	DO L = 1, LMAX
	MNUM=(L-1)*2+1
	 DO MC = 1, MNUM
		PDTOTAL(X) = PDTOTAL(X) + PDSTATES(L, MC, X)	
	 END DO
	END DO
	E = EMIN+(DBLE(X-1))*ESTEP
	WRITE(51,*) E*EVS, PDTOTAL(X), DOSTOTAL(X)
	END DO
	CLOSE(51)

C	LSMALL = 0

	L = 1
	MNUM=(L-1)*2+1
      OPEN(50, FILE = 'DEF_GREEN1_S', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
	DLID(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + PDSTATES(1, MC, X)
			DLID(X) = DLID(X) + DOSSTATES(1, MC, X)
		END DO 
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(50,*) E*EVS, DL(X), DLID(X)
	END DO
	CLOSE(50)

C	LSMALL=1

	L = 2
	MNUM=(L-1)*2+1
	OPEN(50, FILE = 'DEF_GREEN1_P', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
	DLID(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + PDSTATES(2, MC, X)
			DLID(X) = DLID(X) + DOSSTATES(2, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(50,*) E*EVS, DL(X), DLID(X)
	END DO
	CLOSE(50)

C	LSMALL=2

	IF (LMAX == 3) THEN
	L = 3
	MNUM=(L-1)*2+1
	OPEN(50, FILE = 'DEF_GREEN1_D', FORM = 'FORMATTED')
	DO X = 1, EPNTS
	DL(X) = 0.D0
		DO MC = 1, MNUM
			DL(X) = DL(X) + PDSTATES(3, MC, X)
			DLID(X) = DLID(X) + DOSSTATES(3, MC, X)
		END DO
	E = EMIN+(DBLE(X-1))*ESTEP
 	WRITE(50,*) E*EVS, DL(X), DLID(X)
	END DO
	CLOSE(50)
	END IF

C*************************************************
C	���������� ��������� �� ��������� ���������
C*************************************************

 	OPEN(1, FILE = 'INT_OF_DEFECT_DOS.TXT', FORM = 'FORMATTED')
	WRITE(1,*) 'DEFECT INTEGRAL OF DOS = '
	INTDOS = 0.D0
	DO X = 1, XFERMI
	INTDOS = INTDOS + PDTOTAL(X) * ESTEP
	END DO
	WRITE(*,*) 'DEFECT DOS INTEGRAL IS EQUAL TO =', INTDOS
	WRITE(1,*) 
	WRITE(1,*) INTDOS

C	���������� ���� �� ������� ��������

	INTDOS = 0.D0
	DO X=1, (XFERMI-2)
		INTDOS = INTDOS + PDTOTAL(1 + X)
	END DO
	INTDOS = ESTEP*(((PDTOTAL(1) + PDTOTAL(XFERMI))/2.D0)+INTDOS)
	WRITE(1,*)
	WRITE(1,*) 'DEFECT DOS INTEGRAL BY TRAPEZOID IS EQUAL TO = '
	WRITE(1,*)
	WRITE(1,*) INTDOS 
	CLOSE(1)

C***********************
C	����������� �����
C***********************

	WRITE(*,*) '!!! FULL CALCULATION FINISHED !!!'
 	WRITE(*,*) 'RESULTS IN OUT_GREEN1 AND POUT_GREEN1'
      WRITE(*,*)	 
	CALL TIMESTAMP()
	CALL TTIME(TIME_OF_ALL,'CALCULATION TIME ',0)
	CLOSE(65)
	CLOSE(66)
	CLOSE(7)
	WRITE(*,*) 'PRESS ENTER TO EXIT' 
	WRITE(*,*)
      READ(*,*)

C*************************
C	������� � ��������� 
C*************************

	CONTAINS

C********************************************************************************************************
C	���������, ������� ��������� ��������������� ��������� ��� ��������� ���������� �������� T-�������
C********************************************************************************************************

	SUBROUTINE RESULT2(IT, L, E, R, RES)
	INTEGER L, R, N, IT
C	R - ����� ����� �� R
	DOUBLE PRECISION P, E, RES, X, RET_J, RET_Y
	X = DSQRT(E) * RAD(R,IT)
C	X - �������� ������� �������
C	RAD - R � ������ �����
	N = L-1
C	N - ������� ������� �������
C	CALL BESS(N, X, BESSLJ, BESSLJ1)
	CALL BESSSPH(X, N, RET_J, RET_Y)
C	WRITE(*,*) 'BESSSPH RESULT = ', X, N, RET_J
C	��������� ������� ������� ������� ���� ������� N � ����� X
C	V(R,IT) - ��������� V � ������ ����� �� R
C	PSIALL(L, R) - �������� PSI � LSMALL = (L-1) � ����� ����� R 
	P = PSIALL(IT, L, R) + (E - EL(L,IT))*PSIEALL(IT, L, R)
	RES = RET_J * (VR(R,IT)) * P * RAD(R,IT)
C	RES - ��������������� ���������
C	PE - �������� ��� � ������������ ����� �� E
	END SUBROUTINE RESULT2

C************************************************************************************************************
  
	DOUBLE PRECISION FUNCTION ALPH(L, IT)
	INTEGER L, IT
	ALPH = 1 + ((E-EL(L,IT))**2)*NLALPHA(L,IT)
	END FUNCTION


C************************************************************************************************************

      SUBROUTINE RESULT8(X1, RES)
C	���������, ������� ������� ��������������� ��������� � ������� �������� ������� ��� ���������� �������������� 
C	����� G ������ �������(��� ������������� ������� ����� ��������� ����)
	INTEGER X1
	DOUBLE PRECISION E1, ALPHLL12
	DOUBLE COMPLEX RES
	E1 = EMIN+(DBLE(X1-1))*ESTEP 
	CALL RESULT6(IT, L, L1, E1, ALPHLL12)

	IF (X.NE.X1) THEN
		RES = DCMPLX((1.D0/(E-E1))*ALPHLL12)*
	+	DIMAG(GLML1M1(L, MC, L1, MC1, X))
	ELSE
	RES = 0.D0
	END IF

	END SUBROUTINE RESULT8

C************************************************************************************************************

	SUBROUTINE RESULT6(IT,L, L1, E, ALPHLL1)
	EXTERNAL DELTA
	INTEGER IT, L, L1, DELTA, X
	DOUBLE PRECISION E, ALPHLL1, IN1, IN2, IN3
C	��������� ��� ���������� ALPH(L,L1,E)
C	��������� 3 ��������� ������� ��������������� �� ��������	

      IN1 = 0.D0
	DO X = 1, JRIS-1
	IN1 = IN1 + (PSIEALL(IT,L,X)*PSIALL(IT,L1,X)*(RAD(X,IT)**2)+ 
	+PSIEALL(IT,L,X+1)*PSIALL(IT,L1,X+1)*(RAD(X+1,IT)**2))*0.5d0*
	+(RAD((X+1),IT)-RAD(X,IT))
	END DO

	IN2 = 0.D0
	DO X = 1, JRIS-1
	IN2 = IN2 + (PSIEALL(IT,L1,X)*PSIALL(IT,L,X)*(RAD(X,IT)**2)+
	+PSIEALL(IT,L1,X+1)*PSIALL(IT,L,X+1)*(RAD(X+1,IT)**2))*0.5d0*
	+(RAD((X+1),IT)-RAD(X,IT))
	END DO

 	IN3 = 0.D0
 	DO X = 1, JRIS
	IN3 = IN3 + (PSIEALL(IT,L,X)*PSIEALL(IT,L1,X)*(RAD(X,IT)**2)+
	+PSIEALL(IT,L,X)*PSIEALL(IT,L1,X)*(RAD(X,IT)**2))*0.5d0*
	+(RAD((X+1),IT)-RAD(X,IT))
	END DO

	ALPHLL1 = DBLE(DELTA(L,L1)) + (E-EL(L,IT))*IN1
	+ + (E-EL(L1,IT))*IN2 +
	+ (E-EL(L,IT))*(E-EL(L1,IT))*IN3 
	
	END SUBROUTINE RESULT6
	

C************************************************************************************************************

	END PROGRAM GFDEFECT




