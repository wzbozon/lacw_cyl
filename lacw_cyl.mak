# Microsoft Developer Studio Generated NMAKE File, Format Version 4.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

!IF "$(CFG)" == ""
CFG=SpinOrbital - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to SpinOrbital - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "lacw_cyl - Win32 Release" && "$(CFG)" !=\
 "lacw_cyl - Win32 Debug" && "$(CFG)" != "atom - Win32 Release" && "$(CFG)" !=\
 "atom - Win32 Debug" && "$(CFG)" != "strcy - Win32 Release" && "$(CFG)" !=\
 "strcy - Win32 Debug" && "$(CFG)" != "over55 - Win32 Release" && "$(CFG)" !=\
 "over55 - Win32 Debug" && "$(CFG)" != "Defect - Win32 Release" && "$(CFG)" !=\
 "Defect - Win32 Debug" && "$(CFG)" != "vacancy - Win32 Release" && "$(CFG)" !=\
 "vacancy - Win32 Debug" && "$(CFG)" != "SpinOrbital - Win32 Release" &&\
 "$(CFG)" != "SpinOrbital - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "lacw_cyl.mak" CFG="SpinOrbital - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "lacw_cyl - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "lacw_cyl - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "atom - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "atom - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "strcy - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "strcy - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "over55 - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "over55 - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "Defect - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "Defect - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "vacancy - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "vacancy - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "SpinOrbital - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "SpinOrbital - Win32 Debug" (based on\
 "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "SpinOrbital - Win32 Debug"
F90=fl32.exe
RSC=rc.exe

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "SpinOrbital - Win32 Release" "vacancy - Win32 Release"\
 "Defect - Win32 Release" "over55 - Win32 Release" "strcy - Win32 Release"\
 "atom - Win32 Release" 

CLEAN : 
	-@erase 

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "Release/" /c /nologo
# ADD F90 /Ox /I "Release/" /c /nologo
F90_PROJ=/Ox /I "Release/" /c /nologo /Fo"Release/" 
F90_OBJS=.\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/lacw_cyl.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/lacw_cyl.pdb" /machine:I386 /out:"$(OUTDIR)/lacw_cyl.exe" 
LINK32_OBJS=

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "SpinOrbital - Win32 Debug" "vacancy - Win32 Debug"\
 "Defect - Win32 Debug" "over55 - Win32 Debug" "strcy - Win32 Debug"\
 "atom - Win32 Debug" 

CLEAN : 
	-@erase 

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "Debug/" /c /nologo
# ADD F90 /Zi /I "Debug/" /c /nologo
F90_PROJ=/Zi /I "Debug/" /c /nologo /Fo"Debug/" /Fd"Debug/lacw_cyl.pdb" 
F90_OBJS=.\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/lacw_cyl.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/lacw_cyl.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/lacw_cyl.exe" 
LINK32_OBJS=

!ELSEIF  "$(CFG)" == "atom - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "atom\Release"
# PROP BASE Intermediate_Dir "atom\Release"
# PROP BASE Target_Dir "atom"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "atom\Release"
# PROP Intermediate_Dir "atom\Release"
# PROP Target_Dir "atom"
OUTDIR=.\atom\Release
INTDIR=.\atom\Release

ALL : "$(OUTDIR)\atomD.exe"

CLEAN : 
	-@erase ".\atom\Release\atomD.exe"
	-@erase ".\atom\Release\atom1.obj"
	-@erase ".\atom\Release\proatom.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "atom\Release/" /c /nologo
# ADD F90 /Ox /I "atom\Release/" /c /nologo
F90_PROJ=/Ox /I "atom\Release/" /c /nologo /Fo"atom\Release/" 
F90_OBJS=.\atom\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/atom.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /out:"atom\Release/atomD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/atomD.pdb" /machine:I386 /out:"$(OUTDIR)/atomD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/atom1.obj" \
	"$(INTDIR)/proatom.obj"

"$(OUTDIR)\atomD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "atom - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "atom\Debug"
# PROP BASE Intermediate_Dir "atom\Debug"
# PROP BASE Target_Dir "atom"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "atom\Debug"
# PROP Intermediate_Dir "atom\Debug"
# PROP Target_Dir "atom"
OUTDIR=.\atom\Debug
INTDIR=.\atom\Debug

ALL : "$(OUTDIR)\atomD.exe"

CLEAN : 
	-@erase ".\atom\Debug\atomD.exe"
	-@erase ".\atom\Debug\proatom.obj"
	-@erase ".\atom\Debug\atom1.obj"
	-@erase ".\atom\Debug\atomD.ilk"
	-@erase ".\atom\Debug\atomD.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "atom\Debug/" /c /nologo
# ADD F90 /Zi /I "atom\Debug/" /c /nologo
F90_PROJ=/Zi /I "atom\Debug/" /c /nologo /Fo"atom\Debug/"\
 /Fd"atom\Debug/lacw_cyl.pdb" 
F90_OBJS=.\atom\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/atom.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /out:"atom\Debug/atomD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/atomD.pdb" /debug /machine:I386 /out:"$(OUTDIR)/atomD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/proatom.obj" \
	"$(INTDIR)/atom1.obj"

"$(OUTDIR)\atomD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "strcy - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "strcy\Release"
# PROP BASE Intermediate_Dir "strcy\Release"
# PROP BASE Target_Dir "strcy"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "strcy\Release"
# PROP Intermediate_Dir "strcy\Release"
# PROP Target_Dir "strcy"
OUTDIR=.\strcy\Release
INTDIR=.\strcy\Release

ALL : "$(OUTDIR)\strcyD.exe"

CLEAN : 
	-@erase ".\strcy\Release\strcyD.exe"
	-@erase ".\strcy\Release\nagf.obj"
	-@erase ".\strcy\Release\atomdata.obj"
	-@erase ".\strcy\Release\lentrim.obj"
	-@erase ".\strcy\Release\d01daft.obj"
	-@erase ".\strcy\Release\fpmap.obj"
	-@erase ".\strcy\Release\f02haft.obj"
	-@erase ".\strcy\Release\plgndr.obj"
	-@erase ".\strcy\Release\indexx2.obj"
	-@erase ".\strcy\Release\atomfunc.obj"
	-@erase ".\strcy\Release\coordsys.obj"
	-@erase ".\strcy\Release\Strcy.obj"
	-@erase ".\strcy\Release\bess_nul.obj"
	-@erase ".\strcy\Release\int_i12.obj"
	-@erase ".\strcy\Release\findzero.obj"
	-@erase ".\strcy\Release\nagd.obj"
	-@erase ".\strcy\Release\fstring.obj"
	-@erase ".\strcy\Release\projstrc.obj"
	-@erase ".\strcy\Release\extrcalc.obj"
	-@erase ".\strcy\Release\bessl2.obj"
	-@erase ".\strcy\Release\cholesky.obj"
	-@erase ".\strcy\Release\strresult.obj"
	-@erase ".\strcy\Release\ctime.obj"
	-@erase ".\strcy\Release\indexx.obj"
	-@erase ".\strcy\Release\fkappa.obj"
	-@erase ".\strcy\Release\oldp.obj"
	-@erase ".\strcy\Release\mikna.obj"
	-@erase ".\strcy\Release\bessl1.obj"
	-@erase ".\strcy\Release\trap_int.obj"
	-@erase ".\strcy\Release\selectb.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "strcy\Release/" /c /nologo
# ADD F90 /Ox /I "strcy\Release/" /c /nologo
F90_PROJ=/Ox /I "strcy\Release/" /c /nologo /Fo"strcy\Release/" 
F90_OBJS=.\strcy\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/strcy.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /out:"strcy\Release/strcyD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/strcyD.pdb" /machine:I386 /out:"$(OUTDIR)/strcyD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/nagf.obj" \
	"$(INTDIR)/atomdata.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/d01daft.obj" \
	"$(INTDIR)/fpmap.obj" \
	"$(INTDIR)/f02haft.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/indexx2.obj" \
	"$(INTDIR)/atomfunc.obj" \
	"$(INTDIR)/coordsys.obj" \
	"$(INTDIR)/Strcy.obj" \
	"$(INTDIR)/bess_nul.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/findzero.obj" \
	"$(INTDIR)/nagd.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/projstrc.obj" \
	"$(INTDIR)/extrcalc.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/cholesky.obj" \
	"$(INTDIR)/strresult.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/indexx.obj" \
	"$(INTDIR)/fkappa.obj" \
	"$(INTDIR)/oldp.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/selectb.obj"

"$(OUTDIR)\strcyD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "strcy\Debug"
# PROP BASE Intermediate_Dir "strcy\Debug"
# PROP BASE Target_Dir "strcy"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "strcy\Debug"
# PROP Intermediate_Dir "strcy\Debug"
# PROP Target_Dir "strcy"
OUTDIR=.\strcy\Debug
INTDIR=.\strcy\Debug

ALL : "$(OUTDIR)\strcyD.exe"

CLEAN : 
	-@erase ".\strcy\Debug\strcyD.exe"
	-@erase ".\strcy\Debug\atomfunc.obj"
	-@erase ".\strcy\Debug\indexx2.obj"
	-@erase ".\strcy\Debug\plgndr.obj"
	-@erase ".\strcy\Debug\cholesky.obj"
	-@erase ".\strcy\Debug\indexx.obj"
	-@erase ".\strcy\Debug\findzero.obj"
	-@erase ".\strcy\Debug\fkappa.obj"
	-@erase ".\strcy\Debug\selectb.obj"
	-@erase ".\strcy\Debug\trap_int.obj"
	-@erase ".\strcy\Debug\nagf.obj"
	-@erase ".\strcy\Debug\d01daft.obj"
	-@erase ".\strcy\Debug\ctime.obj"
	-@erase ".\strcy\Debug\atomdata.obj"
	-@erase ".\strcy\Debug\mikna.obj"
	-@erase ".\strcy\Debug\bessl2.obj"
	-@erase ".\strcy\Debug\coordsys.obj"
	-@erase ".\strcy\Debug\int_i12.obj"
	-@erase ".\strcy\Debug\bess_nul.obj"
	-@erase ".\strcy\Debug\fpmap.obj"
	-@erase ".\strcy\Debug\oldp.obj"
	-@erase ".\strcy\Debug\nagd.obj"
	-@erase ".\strcy\Debug\bessl1.obj"
	-@erase ".\strcy\Debug\fstring.obj"
	-@erase ".\strcy\Debug\lentrim.obj"
	-@erase ".\strcy\Debug\strresult.obj"
	-@erase ".\strcy\Debug\projstrc.obj"
	-@erase ".\strcy\Debug\Strcy.obj"
	-@erase ".\strcy\Debug\f02haft.obj"
	-@erase ".\strcy\Debug\extrcalc.obj"
	-@erase ".\strcy\Debug\strcyD.ilk"
	-@erase ".\strcy\Debug\strcyD.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "strcy\Debug/" /c /nologo
# ADD F90 /Zi /I "strcy\Debug/" /c /nologo
F90_PROJ=/Zi /I "strcy\Debug/" /c /nologo /Fo"strcy\Debug/"\
 /Fd"strcy\Debug/lacw_cyl.pdb" 
F90_OBJS=.\strcy\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/strcy.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /out:"strcy\Debug/strcyD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/strcyD.pdb" /debug /machine:I386 /out:"$(OUTDIR)/strcyD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/atomfunc.obj" \
	"$(INTDIR)/indexx2.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/cholesky.obj" \
	"$(INTDIR)/indexx.obj" \
	"$(INTDIR)/findzero.obj" \
	"$(INTDIR)/fkappa.obj" \
	"$(INTDIR)/selectb.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/nagf.obj" \
	"$(INTDIR)/d01daft.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/atomdata.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/coordsys.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/bess_nul.obj" \
	"$(INTDIR)/fpmap.obj" \
	"$(INTDIR)/oldp.obj" \
	"$(INTDIR)/nagd.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/strresult.obj" \
	"$(INTDIR)/projstrc.obj" \
	"$(INTDIR)/Strcy.obj" \
	"$(INTDIR)/f02haft.obj" \
	"$(INTDIR)/extrcalc.obj"

"$(OUTDIR)\strcyD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "over55 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "over55\Release"
# PROP BASE Intermediate_Dir "over55\Release"
# PROP BASE Target_Dir "over55"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "over55\Release"
# PROP Intermediate_Dir "over55\Release"
# PROP Target_Dir "over55"
OUTDIR=.\over55\Release
INTDIR=.\over55\Release

ALL : "$(OUTDIR)\over55D.exe"

CLEAN : 
	-@erase ".\over55\Release\over55D.exe"
	-@erase ".\over55\Release\trap_int.obj"
	-@erase ".\over55\Release\fpmap.obj"
	-@erase ".\over55\Release\atomdata.obj"
	-@erase ".\over55\Release\int_i12.obj"
	-@erase ".\over55\Release\plgndr.obj"
	-@erase ".\over55\Release\nagd.obj"
	-@erase ".\over55\Release\Strcy.obj"
	-@erase ".\over55\Release\cholesky.obj"
	-@erase ".\over55\Release\fstring.obj"
	-@erase ".\over55\Release\indexx.obj"
	-@erase ".\over55\Release\fkappa.obj"
	-@erase ".\over55\Release\lentrim.obj"
	-@erase ".\over55\Release\coordsys.obj"
	-@erase ".\over55\Release\bess_nul.obj"
	-@erase ".\over55\Release\indexx2.obj"
	-@erase ".\over55\Release\oldp.obj"
	-@erase ".\over55\Release\ctime.obj"
	-@erase ".\over55\Release\Over55.obj"
	-@erase ".\over55\Release\projstrc.obj"
	-@erase ".\over55\Release\bessl2.obj"
	-@erase ".\over55\Release\selectb.obj"
	-@erase ".\over55\Release\calc_c.obj"
	-@erase ".\over55\Release\extrcalc.obj"
	-@erase ".\over55\Release\strresult.obj"
	-@erase ".\over55\Release\atomfunc.obj"
	-@erase ".\over55\Release\nagf.obj"
	-@erase ".\over55\Release\d01daft.obj"
	-@erase ".\over55\Release\mikna.obj"
	-@erase ".\over55\Release\bessl1.obj"
	-@erase ".\over55\Release\findzero.obj"
	-@erase ".\over55\Release\f02haft.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "over55\Release/" /c /nologo
# ADD F90 /Ox /I "over55\Release/" /c /nologo
F90_PROJ=/Ox /I "over55\Release/" /c /nologo /Fo"over55\Release/" 
F90_OBJS=.\over55\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/over55.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /out:"over55\Release/over55D.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/over55D.pdb" /machine:I386 /out:"$(OUTDIR)/over55D.exe" 
LINK32_OBJS= \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/fpmap.obj" \
	"$(INTDIR)/atomdata.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/nagd.obj" \
	"$(INTDIR)/Strcy.obj" \
	"$(INTDIR)/cholesky.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/indexx.obj" \
	"$(INTDIR)/fkappa.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/coordsys.obj" \
	"$(INTDIR)/bess_nul.obj" \
	"$(INTDIR)/indexx2.obj" \
	"$(INTDIR)/oldp.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/Over55.obj" \
	"$(INTDIR)/projstrc.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/selectb.obj" \
	"$(INTDIR)/calc_c.obj" \
	"$(INTDIR)/extrcalc.obj" \
	"$(INTDIR)/strresult.obj" \
	"$(INTDIR)/atomfunc.obj" \
	"$(INTDIR)/nagf.obj" \
	"$(INTDIR)/d01daft.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/findzero.obj" \
	"$(INTDIR)/f02haft.obj"

"$(OUTDIR)\over55D.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "over55\Debug"
# PROP BASE Intermediate_Dir "over55\Debug"
# PROP BASE Target_Dir "over55"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "over55\Debug"
# PROP Intermediate_Dir "over55\Debug"
# PROP Target_Dir "over55"
OUTDIR=.\over55\Debug
INTDIR=.\over55\Debug

ALL : "$(OUTDIR)\over55D.exe"

CLEAN : 
	-@erase ".\over55\Debug\over55D.exe"
	-@erase ".\over55\Debug\cholesky.obj"
	-@erase ".\over55\Debug\nagd.obj"
	-@erase ".\over55\Debug\selectb.obj"
	-@erase ".\over55\Debug\bessl1.obj"
	-@erase ".\over55\Debug\mikna.obj"
	-@erase ".\over55\Debug\coordsys.obj"
	-@erase ".\over55\Debug\d01daft.obj"
	-@erase ".\over55\Debug\trap_int.obj"
	-@erase ".\over55\Debug\Over55.obj"
	-@erase ".\over55\Debug\fpmap.obj"
	-@erase ".\over55\Debug\atomdata.obj"
	-@erase ".\over55\Debug\f02haft.obj"
	-@erase ".\over55\Debug\calc_c.obj"
	-@erase ".\over55\Debug\oldp.obj"
	-@erase ".\over55\Debug\Strcy.obj"
	-@erase ".\over55\Debug\int_i12.obj"
	-@erase ".\over55\Debug\atomfunc.obj"
	-@erase ".\over55\Debug\strresult.obj"
	-@erase ".\over55\Debug\nagf.obj"
	-@erase ".\over55\Debug\bess_nul.obj"
	-@erase ".\over55\Debug\fstring.obj"
	-@erase ".\over55\Debug\findzero.obj"
	-@erase ".\over55\Debug\lentrim.obj"
	-@erase ".\over55\Debug\plgndr.obj"
	-@erase ".\over55\Debug\bessl2.obj"
	-@erase ".\over55\Debug\projstrc.obj"
	-@erase ".\over55\Debug\ctime.obj"
	-@erase ".\over55\Debug\indexx2.obj"
	-@erase ".\over55\Debug\indexx.obj"
	-@erase ".\over55\Debug\fkappa.obj"
	-@erase ".\over55\Debug\extrcalc.obj"
	-@erase ".\over55\Debug\over55D.ilk"
	-@erase ".\over55\Debug\over55D.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "over55\Debug/" /c /nologo
# ADD F90 /Zi /I "over55\Debug/" /c /nologo
F90_PROJ=/Zi /I "over55\Debug/" /c /nologo /Fo"over55\Debug/"\
 /Fd"over55\Debug/lacw_cyl.pdb" 
F90_OBJS=.\over55\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/over55.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /force /out:"over55\Debug/over55D.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/over55D.pdb" /debug /machine:I386 /force\
 /out:"$(OUTDIR)/over55D.exe" 
LINK32_OBJS= \
	"$(INTDIR)/cholesky.obj" \
	"$(INTDIR)/nagd.obj" \
	"$(INTDIR)/selectb.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/coordsys.obj" \
	"$(INTDIR)/d01daft.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/Over55.obj" \
	"$(INTDIR)/fpmap.obj" \
	"$(INTDIR)/atomdata.obj" \
	"$(INTDIR)/f02haft.obj" \
	"$(INTDIR)/calc_c.obj" \
	"$(INTDIR)/oldp.obj" \
	"$(INTDIR)/Strcy.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/atomfunc.obj" \
	"$(INTDIR)/strresult.obj" \
	"$(INTDIR)/nagf.obj" \
	"$(INTDIR)/bess_nul.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/findzero.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/projstrc.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/indexx2.obj" \
	"$(INTDIR)/indexx.obj" \
	"$(INTDIR)/fkappa.obj" \
	"$(INTDIR)/extrcalc.obj"

"$(OUTDIR)\over55D.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Defect - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Defect\Release"
# PROP BASE Intermediate_Dir "Defect\Release"
# PROP BASE Target_Dir "Defect"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Defect\Release"
# PROP Intermediate_Dir "Defect\Release"
# PROP Target_Dir "Defect"
OUTDIR=.\Defect\Release
INTDIR=.\Defect\Release

ALL : "$(OUTDIR)\DefectD.exe"

CLEAN : 
	-@erase ".\Defect\Release\DefectD.exe"
	-@erase ".\Defect\Release\Defect.obj"
	-@erase ".\Defect\Release\calc_c_for_gfm.obj"
	-@erase ".\Defect\Release\plgndr.obj"
	-@erase ".\Defect\Release\int_i12.obj"
	-@erase ".\Defect\Release\readindefect1.obj"
	-@erase ".\Defect\Release\intlib.obj"
	-@erase ".\Defect\Release\fstring.obj"
	-@erase ".\Defect\Release\ctime.obj"
	-@erase ".\Defect\Release\lentrim.obj"
	-@erase ".\Defect\Release\mikna.obj"
	-@erase ".\Defect\Release\bessl2.obj"
	-@erase ".\Defect\Release\sorting.obj"
	-@erase ".\Defect\Release\trap_int.obj"
	-@erase ".\Defect\Release\gfunctions.obj"
	-@erase ".\Defect\Release\bessl1.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "Defect\Release/" /c /nologo
# ADD F90 /Ox /I "Defect\Release/" /c /nologo
F90_PROJ=/Ox /I "Defect\Release/" /c /nologo /Fo"Defect\Release/" 
F90_OBJS=.\Defect\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/Defect.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /out:"Defect\Release/DefectD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/DefectD.pdb" /machine:I386 /out:"$(OUTDIR)/DefectD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/Defect.obj" \
	"$(INTDIR)/calc_c_for_gfm.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/readindefect1.obj" \
	"$(INTDIR)/intlib.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/sorting.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/gfunctions.obj" \
	"$(INTDIR)/bessl1.obj"

"$(OUTDIR)\DefectD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Defect - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Defect\Debug"
# PROP BASE Intermediate_Dir "Defect\Debug"
# PROP BASE Target_Dir "Defect"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Defect\Debug"
# PROP Intermediate_Dir "Defect\Debug"
# PROP Target_Dir "Defect"
OUTDIR=.\Defect\Debug
INTDIR=.\Defect\Debug

ALL : "$(OUTDIR)\DefectD.exe"

CLEAN : 
	-@erase ".\Defect\Debug\DefectD.exe"
	-@erase ".\Defect\Debug\gfunctions.obj"
	-@erase ".\Defect\Debug\Defect.obj"
	-@erase ".\Defect\Debug\calc_c_for_gfm.obj"
	-@erase ".\Defect\Debug\mikna.obj"
	-@erase ".\Defect\Debug\sorting.obj"
	-@erase ".\Defect\Debug\trap_int.obj"
	-@erase ".\Defect\Debug\plgndr.obj"
	-@erase ".\Defect\Debug\int_i12.obj"
	-@erase ".\Defect\Debug\readindefect1.obj"
	-@erase ".\Defect\Debug\intlib.obj"
	-@erase ".\Defect\Debug\fstring.obj"
	-@erase ".\Defect\Debug\bessl2.obj"
	-@erase ".\Defect\Debug\lentrim.obj"
	-@erase ".\Defect\Debug\bessl1.obj"
	-@erase ".\Defect\Debug\ctime.obj"
	-@erase ".\Defect\Debug\DefectD.ilk"
	-@erase ".\Defect\Debug\DefectD.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "Defect\Debug/" /c /nologo
# ADD F90 /Zi /I "Defect\Debug/" /c /nologo
F90_PROJ=/Zi /I "Defect\Debug/" /c /nologo /Fo"Defect\Debug/"\
 /Fd"Defect\Debug/lacw_cyl.pdb" 
F90_OBJS=.\Defect\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/Defect.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /out:"Defect\Debug/DefectD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/DefectD.pdb" /debug /machine:I386 /out:"$(OUTDIR)/DefectD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/gfunctions.obj" \
	"$(INTDIR)/Defect.obj" \
	"$(INTDIR)/calc_c_for_gfm.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/sorting.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/readindefect1.obj" \
	"$(INTDIR)/intlib.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/ctime.obj"

"$(OUTDIR)\DefectD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "vacancy - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "vacancy\Release"
# PROP BASE Intermediate_Dir "vacancy\Release"
# PROP BASE Target_Dir "vacancy"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "vacancy\Release"
# PROP Intermediate_Dir "vacancy\Release"
# PROP Target_Dir "vacancy"
OUTDIR=.\vacancy\Release
INTDIR=.\vacancy\Release

ALL : "$(OUTDIR)\vacancyD.exe"

CLEAN : 
	-@erase ".\vacancy\Release\vacancyD.exe"
	-@erase ".\vacancy\Release\lentrim.obj"
	-@erase ".\vacancy\Release\ctime.obj"
	-@erase ".\vacancy\Release\polint.obj"
	-@erase ".\vacancy\Release\gaussj.obj"
	-@erase ".\vacancy\Release\intlib.obj"
	-@erase ".\vacancy\Release\fstring.obj"
	-@erase ".\vacancy\Release\Bess_sph.obj"
	-@erase ".\vacancy\Release\bessl2.obj"
	-@erase ".\vacancy\Release\readindefect1.obj"
	-@erase ".\vacancy\Release\vacancy.obj"
	-@erase ".\vacancy\Release\bessl1.obj"
	-@erase ".\vacancy\Release\gfunctions.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "vacancy\Release/" /c /nologo
# ADD F90 /Ox /I "vacancy\Release/" /c /nologo
F90_PROJ=/Ox /I "vacancy\Release/" /c /nologo /Fo"vacancy\Release/" 
F90_OBJS=.\vacancy\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/vacancy.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /out:"vacancy\Release/vacancyD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/vacancyD.pdb" /machine:I386 /out:"$(OUTDIR)/vacancyD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/polint.obj" \
	"$(INTDIR)/gaussj.obj" \
	"$(INTDIR)/intlib.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/Bess_sph.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/readindefect1.obj" \
	"$(INTDIR)/vacancy.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/gfunctions.obj"

"$(OUTDIR)\vacancyD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "vacancy - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "vacancy\Debug"
# PROP BASE Intermediate_Dir "vacancy\Debug"
# PROP BASE Target_Dir "vacancy"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "vacancy\Debug"
# PROP Intermediate_Dir "vacancy\Debug"
# PROP Target_Dir "vacancy"
OUTDIR=.\vacancy\Debug
INTDIR=.\vacancy\Debug

ALL : "$(OUTDIR)\vacancyD.exe"

CLEAN : 
	-@erase ".\vacancy\Debug\vacancyD.exe"
	-@erase ".\vacancy\Debug\Bess_sph.obj"
	-@erase ".\vacancy\Debug\fstring.obj"
	-@erase ".\vacancy\Debug\lentrim.obj"
	-@erase ".\vacancy\Debug\ctime.obj"
	-@erase ".\vacancy\Debug\intlib.obj"
	-@erase ".\vacancy\Debug\gfunctions.obj"
	-@erase ".\vacancy\Debug\polint.obj"
	-@erase ".\vacancy\Debug\bessl2.obj"
	-@erase ".\vacancy\Debug\gaussj.obj"
	-@erase ".\vacancy\Debug\readindefect1.obj"
	-@erase ".\vacancy\Debug\bessl1.obj"
	-@erase ".\vacancy\Debug\vacancy.obj"
	-@erase ".\vacancy\Debug\vacancyD.ilk"
	-@erase ".\vacancy\Debug\vacancyD.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "vacancy\Debug/" /c /nologo
# ADD F90 /Zi /I "vacancy\Debug/" /c /nologo
F90_PROJ=/Zi /I "vacancy\Debug/" /c /nologo /Fo"vacancy\Debug/"\
 /Fd"vacancy\Debug/lacw_cyl.pdb" 
F90_OBJS=.\vacancy\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/vacancy.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /out:"vacancy\Debug/vacancyD.exe"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/vacancyD.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/vacancyD.exe" 
LINK32_OBJS= \
	"$(INTDIR)/Bess_sph.obj" \
	"$(INTDIR)/fstring.obj" \
	"$(INTDIR)/lentrim.obj" \
	"$(INTDIR)/ctime.obj" \
	"$(INTDIR)/intlib.obj" \
	"$(INTDIR)/gfunctions.obj" \
	"$(INTDIR)/polint.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/gaussj.obj" \
	"$(INTDIR)/readindefect1.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/vacancy.obj"

"$(OUTDIR)\vacancyD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "SpinOrbital - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "SpinOrbital\Release"
# PROP BASE Intermediate_Dir "SpinOrbital\Release"
# PROP BASE Target_Dir "SpinOrbital"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "SpinOrbital\Release"
# PROP Intermediate_Dir "SpinOrbital\Release"
# PROP Target_Dir "SpinOrbital"
OUTDIR=.\SpinOrbital\Release
INTDIR=.\SpinOrbital\Release

ALL : "$(OUTDIR)\SpinOrbital.exe"

CLEAN : 
	-@erase ".\SpinOrbital\Release\SpinOrbital.exe"
	-@erase ".\SpinOrbital\Release\plgndr.obj"
	-@erase ".\SpinOrbital\Release\SpinOrbital.obj"
	-@erase ".\SpinOrbital\Release\bessl2.obj"
	-@erase ".\SpinOrbital\Release\nagd.obj"
	-@erase ".\SpinOrbital\Release\calc_c_for_gfm.obj"
	-@erase ".\SpinOrbital\Release\bessl1.obj"
	-@erase ".\SpinOrbital\Release\f02haft.obj"
	-@erase ".\SpinOrbital\Release\mikna.obj"
	-@erase ".\SpinOrbital\Release\nagf.obj"
	-@erase ".\SpinOrbital\Release\int_i12.obj"
	-@erase ".\SpinOrbital\Release\trap_int.obj"
	-@erase ".\SpinOrbital\Release\intlib.obj"
	-@erase ".\SpinOrbital\Release\ctime.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "SpinOrbital\Release/" /c /nologo
# ADD F90 /Ox /I "SpinOrbital\Release/" /c /nologo
F90_PROJ=/Ox /I "SpinOrbital\Release/" /c /nologo /Fo"SpinOrbital\Release/" 
F90_OBJS=.\SpinOrbital\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/SpinOrbital.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/SpinOrbital.pdb" /machine:I386 /out:"$(OUTDIR)/SpinOrbital.exe"\
 
LINK32_OBJS= \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/SpinOrbital.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/nagd.obj" \
	"$(INTDIR)/calc_c_for_gfm.obj" \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/f02haft.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/nagf.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/intlib.obj" \
	"$(INTDIR)/ctime.obj"

"$(OUTDIR)\SpinOrbital.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "SpinOrbital - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "SpinOrbital\Debug"
# PROP BASE Intermediate_Dir "SpinOrbital\Debug"
# PROP BASE Target_Dir "SpinOrbital"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "SpinOrbital\Debug"
# PROP Intermediate_Dir "SpinOrbital\Debug"
# PROP Target_Dir "SpinOrbital"
OUTDIR=.\SpinOrbital\Debug
INTDIR=.\SpinOrbital\Debug

ALL : "$(OUTDIR)\SpinOrbital.exe"

CLEAN : 
	-@erase ".\SpinOrbital\Debug\SpinOrbital.exe"
	-@erase ".\SpinOrbital\Debug\bessl1.obj"
	-@erase ".\SpinOrbital\Debug\trap_int.obj"
	-@erase ".\SpinOrbital\Debug\f02haft.obj"
	-@erase ".\SpinOrbital\Debug\int_i12.obj"
	-@erase ".\SpinOrbital\Debug\mikna.obj"
	-@erase ".\SpinOrbital\Debug\SpinOrbital.obj"
	-@erase ".\SpinOrbital\Debug\calc_c_for_gfm.obj"
	-@erase ".\SpinOrbital\Debug\intlib.obj"
	-@erase ".\SpinOrbital\Debug\nagd.obj"
	-@erase ".\SpinOrbital\Debug\plgndr.obj"
	-@erase ".\SpinOrbital\Debug\bessl2.obj"
	-@erase ".\SpinOrbital\Debug\nagf.obj"
	-@erase ".\SpinOrbital\Debug\ctime.obj"
	-@erase ".\SpinOrbital\Debug\SpinOrbital.ilk"
	-@erase ".\SpinOrbital\Debug\SpinOrbital.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "SpinOrbital\Debug/" /c /nologo
# ADD F90 /Zi /I "SpinOrbital\Debug/" /c /nologo
F90_PROJ=/Zi /I "SpinOrbital\Debug/" /c /nologo /Fo"SpinOrbital\Debug/"\
 /Fd"SpinOrbital\Debug/lacw_cyl.pdb" 
F90_OBJS=.\SpinOrbital\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/SpinOrbital.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/SpinOrbital.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/SpinOrbital.exe" 
LINK32_OBJS= \
	"$(INTDIR)/bessl1.obj" \
	"$(INTDIR)/trap_int.obj" \
	"$(INTDIR)/f02haft.obj" \
	"$(INTDIR)/int_i12.obj" \
	"$(INTDIR)/mikna.obj" \
	"$(INTDIR)/SpinOrbital.obj" \
	"$(INTDIR)/calc_c_for_gfm.obj" \
	"$(INTDIR)/intlib.obj" \
	"$(INTDIR)/nagd.obj" \
	"$(INTDIR)/plgndr.obj" \
	"$(INTDIR)/bessl2.obj" \
	"$(INTDIR)/nagf.obj" \
	"$(INTDIR)/ctime.obj"

"$(OUTDIR)\SpinOrbital.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

################################################################################
# Begin Target

# Name "lacw_cyl - Win32 Release"
# Name "lacw_cyl - Win32 Debug"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

!ENDIF 

################################################################################
# Begin Project Dependency

# Project_Dep_Name "atom"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

"atom - Win32 Release" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="atom - Win32 Release" 

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

"atom - Win32 Debug" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="atom - Win32 Debug" 

!ENDIF 

# End Project Dependency
################################################################################
# Begin Project Dependency

# Project_Dep_Name "strcy"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

"strcy - Win32 Release" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="strcy - Win32 Release" 

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

"strcy - Win32 Debug" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="strcy - Win32 Debug" 

!ENDIF 

# End Project Dependency
################################################################################
# Begin Project Dependency

# Project_Dep_Name "over55"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

"over55 - Win32 Release" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="over55 - Win32 Release" 

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

"over55 - Win32 Debug" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="over55 - Win32 Debug" 

!ENDIF 

# End Project Dependency
################################################################################
# Begin Project Dependency

# Project_Dep_Name "Defect"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

"Defect - Win32 Release" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="Defect - Win32 Release" 

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

"Defect - Win32 Debug" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="Defect - Win32 Debug" 

!ENDIF 

# End Project Dependency
################################################################################
# Begin Project Dependency

# Project_Dep_Name "vacancy"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

"vacancy - Win32 Release" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="vacancy - Win32 Release" 

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

"vacancy - Win32 Debug" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="vacancy - Win32 Debug" 

!ENDIF 

# End Project Dependency
################################################################################
# Begin Project Dependency

# Project_Dep_Name "SpinOrbital"

!IF  "$(CFG)" == "lacw_cyl - Win32 Release"

"SpinOrbital - Win32 Release" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="SpinOrbital - Win32 Release" 

!ELSEIF  "$(CFG)" == "lacw_cyl - Win32 Debug"

"SpinOrbital - Win32 Debug" : 
   $(MAKE) /$(MAKEFLAGS) /F .\lacw_cyl.mak CFG="SpinOrbital - Win32 Debug" 

!ENDIF 

# End Project Dependency
# End Target
################################################################################
# Begin Target

# Name "atom - Win32 Release"
# Name "atom - Win32 Debug"

!IF  "$(CFG)" == "atom - Win32 Release"

!ELSEIF  "$(CFG)" == "atom - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\lib\atom1.for

"$(INTDIR)\atom1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\proatom.for

"$(INTDIR)\proatom.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
# End Target
################################################################################
# Begin Target

# Name "strcy - Win32 Release"
# Name "strcy - Win32 Debug"

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\lib\xcpot.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\varsizes.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\strresult.for
DEP_F90_STRRE=\
	".\lib\atomdata.fh"\
	".\lib\strresult.fh"\
	".\lib\stdio.fh"\
	

"$(INTDIR)\strresult.obj" : $(SOURCE) $(DEP_F90_STRRE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\strresult.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\Strcy.for
DEP_F90_STRCY=\
	".\lib\atomdata.fh"\
	".\lib\coordsys.fh"\
	".\lib\projstrc.fh"\
	".\lib\strresult.fh"\
	".\lib\fkappa.fh"\
	".\lib\stdio.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\Strcy.obj" : $(SOURCE) $(DEP_F90_STRCY) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\stdio.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\selectb.for

"$(INTDIR)\selectb.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\projstrc.for
DEP_F90_PROJS=\
	".\lib\projstrc.fh"\
	".\lib\coordsys.fh"\
	".\lib\atomdata.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\projstrc.obj" : $(SOURCE) $(DEP_F90_PROJS) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\projstrc.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\plgndr.for

"$(INTDIR)\plgndr.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\oldp.for
DEP_F90_OLDP_=\
	".\lib\atomdata.fh"\
	

"$(INTDIR)\oldp.obj" : $(SOURCE) $(DEP_F90_OLDP_) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\nagf.for

"$(INTDIR)\nagf.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\nagd.for

"$(INTDIR)\nagd.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\lentrim.for

"$(INTDIR)\lentrim.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\int_i12.for

"$(INTDIR)\int_i12.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\indexx2.for

"$(INTDIR)\indexx2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\indexx.for

"$(INTDIR)\indexx.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fstring.for

"$(INTDIR)\fstring.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fpmap.for
DEP_F90_FPMAP=\
	".\lib\coordsys.fh"\
	".\lib\varsizes.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\fpmap.obj" : $(SOURCE) $(DEP_F90_FPMAP) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fkappa.for
DEP_F90_FKAPP=\
	".\lib\fkappa.fh"\
	".\lib\projstrc.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\fkappa.obj" : $(SOURCE) $(DEP_F90_FKAPP) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fkappa.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\findzero.for

"$(INTDIR)\findzero.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\f02haft.for

"$(INTDIR)\f02haft.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\extrcalc.for

"$(INTDIR)\extrcalc.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\d01daft.for

"$(INTDIR)\d01daft.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\ctime.for

"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\coordsys.for
DEP_F90_COORD=\
	".\lib\coordsys.fh"\
	

"$(INTDIR)\coordsys.obj" : $(SOURCE) $(DEP_F90_COORD) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\coordsys.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\cholesky.for

"$(INTDIR)\cholesky.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl2.for

"$(INTDIR)\bessl2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl1.for

"$(INTDIR)\bessl1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bess_nul.for

"$(INTDIR)\bess_nul.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\atomfunc.for

"$(INTDIR)\atomfunc.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\atomdata.for
DEP_F90_ATOMD=\
	".\lib\atomdata.fh"\
	

"$(INTDIR)\atomdata.obj" : $(SOURCE) $(DEP_F90_ATOMD) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\atomdata.fh

!IF  "$(CFG)" == "strcy - Win32 Release"

!ELSEIF  "$(CFG)" == "strcy - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\trap_int.for

"$(INTDIR)\trap_int.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\mikna.for

"$(INTDIR)\mikna.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
# End Target
################################################################################
# Begin Target

# Name "over55 - Win32 Release"
# Name "over55 - Win32 Debug"

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\lib\xcpot.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\varsizes.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\strresult.for
DEP_F90_STRRE=\
	".\lib\atomdata.fh"\
	".\lib\strresult.fh"\
	".\lib\stdio.fh"\
	

"$(INTDIR)\strresult.obj" : $(SOURCE) $(DEP_F90_STRRE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\strresult.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\stdio.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\selectb.for

"$(INTDIR)\selectb.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\projstrc.for
DEP_F90_PROJS=\
	".\lib\projstrc.fh"\
	".\lib\coordsys.fh"\
	".\lib\atomdata.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\projstrc.obj" : $(SOURCE) $(DEP_F90_PROJS) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\projstrc.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\plgndr.for

"$(INTDIR)\plgndr.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\oldp.for
DEP_F90_OLDP_=\
	".\lib\atomdata.fh"\
	

"$(INTDIR)\oldp.obj" : $(SOURCE) $(DEP_F90_OLDP_) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\nagf.for

"$(INTDIR)\nagf.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\nagd.for

"$(INTDIR)\nagd.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\lentrim.for

"$(INTDIR)\lentrim.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\int_i12.for

"$(INTDIR)\int_i12.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\indexx2.for

"$(INTDIR)\indexx2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\indexx.for

"$(INTDIR)\indexx.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fstring.for

"$(INTDIR)\fstring.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fpmap.for
DEP_F90_FPMAP=\
	".\lib\coordsys.fh"\
	".\lib\varsizes.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\fpmap.obj" : $(SOURCE) $(DEP_F90_FPMAP) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fkappa.for
DEP_F90_FKAPP=\
	".\lib\fkappa.fh"\
	".\lib\projstrc.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\fkappa.obj" : $(SOURCE) $(DEP_F90_FKAPP) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fkappa.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\findzero.for

"$(INTDIR)\findzero.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\extrcalc.for

"$(INTDIR)\extrcalc.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\d01daft.for

"$(INTDIR)\d01daft.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\ctime.for

"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\coordsys.for
DEP_F90_COORD=\
	".\lib\coordsys.fh"\
	

"$(INTDIR)\coordsys.obj" : $(SOURCE) $(DEP_F90_COORD) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\coordsys.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\cholesky.for

"$(INTDIR)\cholesky.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl2.for

"$(INTDIR)\bessl2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl1.for

"$(INTDIR)\bessl1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bess_nul.for

"$(INTDIR)\bess_nul.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\atomfunc.for

"$(INTDIR)\atomfunc.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\atomdata.for
DEP_F90_ATOMD=\
	".\lib\atomdata.fh"\
	

"$(INTDIR)\atomdata.obj" : $(SOURCE) $(DEP_F90_ATOMD) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\atomdata.fh

!IF  "$(CFG)" == "over55 - Win32 Release"

!ELSEIF  "$(CFG)" == "over55 - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\Over55.for
DEP_F90_OVER5=\
	".\lib\atomdata.fh"\
	".\lib\coordsys.fh"\
	".\lib\projstrc.fh"\
	".\lib\varsizes.fh"\
	".\lib\stdio.fh"\
	".\lib\strresult.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\Over55.obj" : $(SOURCE) $(DEP_F90_OVER5) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\trap_int.for

"$(INTDIR)\trap_int.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\mikna.for

"$(INTDIR)\mikna.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\calc_c.for

"$(INTDIR)\calc_c.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\Strcy.for
DEP_F90_STRCY=\
	".\lib\atomdata.fh"\
	".\lib\coordsys.fh"\
	".\lib\projstrc.fh"\
	".\lib\strresult.fh"\
	".\lib\fkappa.fh"\
	".\lib\stdio.fh"\
	".\lib\xcpot.fh"\
	

"$(INTDIR)\Strcy.obj" : $(SOURCE) $(DEP_F90_STRCY) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\f02haft.for

"$(INTDIR)\f02haft.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
# End Target
################################################################################
# Begin Target

# Name "Defect - Win32 Release"
# Name "Defect - Win32 Debug"

!IF  "$(CFG)" == "Defect - Win32 Release"

!ELSEIF  "$(CFG)" == "Defect - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\lib\bessl1.for

"$(INTDIR)\bessl1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\plgndr.for

"$(INTDIR)\plgndr.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\intlib.f90

"$(INTDIR)\intlib.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\gfunctions.for

"$(INTDIR)\gfunctions.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\ctime.for

!IF  "$(CFG)" == "Defect - Win32 Release"


"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "Defect - Win32 Debug"


"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\calc_c_for_gfm.for

"$(INTDIR)\calc_c_for_gfm.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl2.for

"$(INTDIR)\bessl2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\Defect.for

!IF  "$(CFG)" == "Defect - Win32 Release"


"$(INTDIR)\Defect.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "Defect - Win32 Debug"


"$(INTDIR)\Defect.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\readindefect1.for

"$(INTDIR)\readindefect1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fstring.for

"$(INTDIR)\fstring.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\lentrim.for

"$(INTDIR)\lentrim.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\sorting.for

"$(INTDIR)\sorting.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\int_i12.for

"$(INTDIR)\int_i12.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\trap_int.for

"$(INTDIR)\trap_int.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\mikna.for

"$(INTDIR)\mikna.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
# End Target
################################################################################
# Begin Target

# Name "vacancy - Win32 Release"
# Name "vacancy - Win32 Debug"

!IF  "$(CFG)" == "vacancy - Win32 Release"

!ELSEIF  "$(CFG)" == "vacancy - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\lib\vacancy.for

"$(INTDIR)\vacancy.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\gaussj.for

"$(INTDIR)\gaussj.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\ctime.for

"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl1.for

"$(INTDIR)\bessl1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl2.for

"$(INTDIR)\bessl2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\polint.for

"$(INTDIR)\polint.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\Bess_sph.for

"$(INTDIR)\Bess_sph.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\lentrim.for

"$(INTDIR)\lentrim.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\fstring.for

"$(INTDIR)\fstring.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\readindefect1.for

"$(INTDIR)\readindefect1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\gfunctions.for

"$(INTDIR)\gfunctions.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\intlib.f90

"$(INTDIR)\intlib.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
# End Target
################################################################################
# Begin Target

# Name "SpinOrbital - Win32 Release"
# Name "SpinOrbital - Win32 Debug"

!IF  "$(CFG)" == "SpinOrbital - Win32 Release"

!ELSEIF  "$(CFG)" == "SpinOrbital - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\lib\SpinOrbital.for

"$(INTDIR)\SpinOrbital.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\intlib.f90

"$(INTDIR)\intlib.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\int_i12.for

"$(INTDIR)\int_i12.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl1.for

"$(INTDIR)\bessl1.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\bessl2.for

"$(INTDIR)\bessl2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\trap_int.for

"$(INTDIR)\trap_int.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\plgndr.for

"$(INTDIR)\plgndr.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\mikna.for

"$(INTDIR)\mikna.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\calc_c_for_gfm.for

"$(INTDIR)\calc_c_for_gfm.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\f02haft.for

"$(INTDIR)\f02haft.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\nagf.for

"$(INTDIR)\nagf.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\nagd.for

"$(INTDIR)\nagd.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lib\ctime.for

!IF  "$(CFG)" == "SpinOrbital - Win32 Release"


"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "SpinOrbital - Win32 Debug"


"$(INTDIR)\ctime.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
