## Intel Fortran Compiler:
#FFLAGS=-G6 -Ox -QxK -w90 -4Yportlib
#FC=ifl
#LINK=xilink

## Microsoft Powerstation Fortran:
FFLAGS=-G5 -Ox -Op -nologo
FC=fl32
LINK=link

NAME=Over55D
OBJS=../lib/Over55.obj ../lib/oldp.obj \
	../lib/int_i12.obj ../lib/bessl1.obj ../lib/bessl2.obj ../lib/plgndr.obj \
	../lib/bess_nul.obj ../lib/ctime.obj ../lib/d01daft.obj ../lib/cholesky.obj \
	../lib/f02haft.obj ../lib/nagd.obj ../lib/nagf.obj ../lib/indexx2.obj \
	../lib/fkappa.obj ../lib/findzero.obj ../lib/extrcalc.obj ../lib/atomfunc.obj \
	../lib/projstrc.obj ../lib/atomdata.obj ../lib/strresult.obj ../lib/coordsys.obj \
	../lib/fstring.obj ../lib/lentrim.obj ../lib/calc_c.obj ../lib/mikna.obj \
	../lib/trap_int.obj
LIBS=

NAME_EXE=$(NAME).exe

$(NAME_EXE):	$(OBJS)
	@$(LINK) -out:$(NAME_EXE) $(OBJS:../lib/=) $(LIBS)

clean:;	@for %i in ($(OBJS)) do @del %i

.for.obj:;	$(FC) -c $(FFLAGS) $< >$(<:.for=.err)
