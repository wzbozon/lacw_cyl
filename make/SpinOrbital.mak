## Intel Fortran Compiler:
#FFLAGS=-G6 -Ox -QxK -w90 -4Yportlib
#FC=ifl
#LINK=xilink

## Microsoft Powerstation Fortran:
FFLAGS=-G5 -Ox -Op -nologo
FC=fl32
LINK=link

NAME=SpinOrbital

OBJS=../lib/bessl1.obj ../lib/bessl2.obj ../lib/calc_c_for_gfm.obj ../lib/ctime.obj \
../lib/f02haft.obj ../lib/int_i12.obj ../lib/intlib.obj ../lib/mikna.obj \
../lib/nagd.obj ../lib/nagf.obj ../lib/plgndr.obj ../lib/SpinOrbital.obj \
../lib/trap_int.obj
LIBS=

NAME_EXE=$(NAME).exe

$(NAME_EXE):	$(OBJS)
	@$(LINK) -out:$(NAME_EXE) $(OBJS:../lib/=) $(LIBS)

clean:;	@for %i in ($(OBJS)) do @del %i

.for.obj:
        $(FC) -c $(FFLAGS) $< >$(<:.for=.err)
