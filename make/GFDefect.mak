## Intel Fortran Compiler:
#FFLAGS=-G6 -Ox -QxK -w90 -4Yportlib
#FC=ifl
#LINK=xilink

## Microsoft Powerstation Fortran:
FFLAGS=-G5 -Ox -Op -nologo
FC=fl32
LINK=link

NAME=GFDefect

OBJS=../lib/vacancy.obj ../lib/intlib.obj ../lib/ctime.obj ../lib/gaussj.obj \
	../lib/bessl1.obj ../lib/bessl2.obj ../lib/polint.obj ../lib/Bess_sph.obj \
	../lib/determinant1.obj ../lib/determinant2.obj ../lib/determinant3.obj \
	../lib/fstring.obj ../lib/readindefect1.obj ../lib/lentrim.obj \
	../lib/gfunctions.obj
	

LIBS=

NAME_EXE=$(NAME).exe

$(NAME_EXE):	$(OBJS)
	@$(LINK) -out:$(NAME_EXE) $(OBJS:../lib/=) $(LIBS)

clean:;	@for %i in ($(OBJS)) do @del %i

.for.obj:
        $(FC) -c $(FFLAGS) $< >$(<:.for=.err)
