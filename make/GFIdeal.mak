## Intel Fortran Compiler:
#FFLAGS=-G6 -Ox -QxK -w90 -4Yportlib
#FC=ifl
#LINK=xilink

## Microsoft Powerstation Fortran:
FFLAGS=-G5 -Ox -Op -nologo
FC=fl32
LINK=link

NAME=GFIdeal

OBJS=../lib/Defect.obj ../lib/bessl1.obj ../lib/calc_c_for_gfm.obj ../lib/bessl2.obj \
	../lib/plgndr.obj ../lib/gfunctions.obj \
	../lib/intlib.obj ../lib/ctime.obj ../lib/sorting.obj ../lib/int_i12.obj \
	../lib/fstring.obj ../lib/readindefect1.obj ../lib/lentrim.obj \
        ../lib/mikna.obj ../lib/trap_int.obj

LIBS=

NAME_EXE=$(NAME).exe

$(NAME_EXE):	$(OBJS)
	@$(LINK) -out:$(NAME_EXE) $(OBJS:../lib/=) $(LIBS)

clean:;	@for %i in ($(OBJS)) do @del %i

.for.obj:
        $(FC) -c $(FFLAGS) $< >$(<:.for=.err)
