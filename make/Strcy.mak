## Intel Fortran Compiler:
#FFLAGS=-G6 -Ox -QxK -w90 -4Yportlib
#FC=ifl
#LINK=xilink

## Microsoft Powerstation Fortran:
FFLAGS=-G5 -Ox -Op -nologo
FC=fl32
LINK=link

NAME=StrcyD

OBJS=../lib/Strcy.obj ../lib/projstrc.obj ../lib/atomdata.obj ../lib/strresult.obj \
	../lib/int_i12.obj ../lib/indexx.obj ../lib/atomfunc.obj \
	../lib/fkappa.obj ../lib/findzero.obj ../lib/bess_nul.obj ../lib/bessl1.obj \
	../lib/bessl2.obj ../lib/plgndr.obj ../lib/selectb.obj ../lib/lentrim.obj \
	../lib/coordsys.obj ../lib/fpmap.obj ../lib/fstring.obj ../lib/calc_c.obj \
	../lib/mikna.obj ../lib/trap_int.obj

LIBS=

NAME_EXE=$(NAME).exe

$(NAME_EXE):	$(OBJS)
	@$(LINK) -out:$(NAME_EXE) $(OBJS:../lib/=) $(LIBS)

clean:;	@for %i in ($(OBJS)) do @del %i

.for.obj:
        $(FC) -c $(FFLAGS) $< >$(<:.for=.err)
